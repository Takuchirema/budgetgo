package com.mygoals.budgetgo;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Takunda Chirema on 2017-08-25.
 */

public class SMS {

    private String sms;
    private Long date;
    private Date actualDate;
    private String phoneNumber;

    public SMS(String sms,String phoneNumber){
        this.sms=sms;
        this.phoneNumber=phoneNumber;
    }

    public String getSms() {
        return sms;
    }

    public void setSms(String sms) {
        this.sms = sms;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(String date) {

        Long timestamp = Long.parseLong(date);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        actualDate = calendar.getTime();

        this.date = timestamp;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String number) {
        this.phoneNumber = number;
    }

    public Date getActualDate() {
        return actualDate;
    }

    public void setActualDate(Date actualDate) {
        this.actualDate = actualDate;
    }
}
