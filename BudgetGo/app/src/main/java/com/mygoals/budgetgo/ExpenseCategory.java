package com.mygoals.budgetgo;

import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Takunda Chirema on 2017-07-22.
 */

public class ExpenseCategory implements Comparable<ExpenseCategory>{

    private int id;
    private ArrayList<Expense> expenses = new ArrayList<Expense>();
    private AccountsMap<Integer,Account> accounts = new AccountsMap<>();
    private float value;
    private float aim;
    private String period;
    private String processMonth;

    private int drawable;
    private String name;
    public static int notificationPrefix = 2;
    public static String notificationType = "Expenses";

    public ExpenseCategory(int id,String name){
        this.id = id;
        this.name = name;
        setExpenseCategory(id);
    }

    public void createExpenses(){

        Expense e1 = new Expense(5000,id);
        e1.setExpenseAccountName("Checkers");
        e1.setExpenseDay("22-07-2017");
        e1.setExpenseDescription("Bought goods from Checkers");

        Expense e2 = new Expense(250,id);
        e2.setExpenseAccountName("Pick n Pay");
        e2.setExpenseDay("22-07-2017");
        e2.setExpenseDescription("Bought goods from Pick n Pay");

        expenses.add(e1);
        expenses.add(e2);
    }

    public float calculateExpensesValue(){
        float expensesValue = 0;
        //System.out.println("expense value calc: "+name+" accounts: "+accounts.size());
        for (Map.Entry<Integer,Account> e:accounts.entrySet()){
            Account account = e.getValue();
            if ( processMonth != null && !processMonth.isEmpty()){
                if (account.getProcessMonth()!= null && account.getProcessMonth().equalsIgnoreCase(processMonth)){
                    //System.out.println("expense value calc account: " + e.getValue().getExpensesValue());
                    expensesValue = expensesValue + e.getValue().getExpensesValue();
                }
            }else {
                //System.out.println("expense value calc account: " + e.getValue().getExpensesValue());
                expensesValue = expensesValue + e.getValue().getExpensesValue();
            }
        }

        value = expensesValue;
        return expensesValue;
    }

    public ContentValues getContentValues(){
        ContentValues data = new ContentValues();

        data.put("Name",name);
        data.put("Aim",aim);
        data.put("Drawable",drawable);

        return data;
    }

    public void save(){
        ExpenseCategory newCategory = SQLite.storeExpenseCategory(this);
        ExpenseCategories.expenseCategoriesMap.put(newCategory.getId(),newCategory);
        ExpenseCategories.refreshSpinnerItems();
    }

    public void delete(){
        boolean deleted = SQLite.deleteExpenseCategory(this.id);

        if (deleted){
            ExpenseCategories.remove(this.id);
            ExpenseCategories.refreshSpinnerItems();
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getExpenseCategoryName() {
        return name;
    }

    public void setExpenseCategory(int expenseCategoryId) {
        //Find the accounts with this expensecategory and put them in
        Iterator<Account> iterator = Accounts.getIterator();
        while(iterator.hasNext()){
            Account account = iterator.next();
            if (Accounts.isExpense(account.getAccountType()) && account.getExpenseCategoryId() == expenseCategoryId){
                accounts.put(account.getId(),account);
            }
        }
    }

    public ArrayList<Expense> getExpenses() {
        return expenses;
    }

    public void setExpenses(ArrayList<Expense> expenses) {
        this.expenses = expenses;
    }

    public float getValue() {
        return value;
    }

    public float getValue(String month){
        float expensesValue = 0;

        for (Map.Entry<Integer,Account> e:accounts.entrySet()){
            Account account = e.getValue();

            ArrayList<Transaction> monthTransactions = account.getTransactions().getTransactionsForMonth(month);
            float valueForMonth = account.getTransactionsValue(monthTransactions);

            expensesValue = expensesValue + valueForMonth;
        }

        return expensesValue;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPeriod() {
        DateTime dateTime = new DateTime(new Date());

        if (processMonth!=null && !processMonth.isEmpty()){
            period = fullMonthName(processMonth) + " "+dateTime.toString("yyyy");
        }else{
            if (!accounts.isEmpty()) {
                period = getFirstEntry().getPeriod();
            }else {
                period = dateTime.toString("MMMM") + " " + dateTime.toString("yyyy");
            }
        }
        return period;
    }

    public Account getFirstEntry(){
        return (Account) accounts.values().toArray()[0];
    }

    private String fullMonthName(String mmm){

        DateTime dateTime = new DateTime(new Date());
        String fullMonth = dateTime.toString("MMMM");

        if (mmm.equalsIgnoreCase("jan")){
            fullMonth = "January";
        }else if (mmm.equalsIgnoreCase("feb")){
            fullMonth = "February";
        }else if (mmm.equalsIgnoreCase("mar")){
            fullMonth = "March";
        }else if (mmm.equalsIgnoreCase("apr")){
            fullMonth = "April";
        }else if (mmm.equalsIgnoreCase("may")){
            fullMonth = "May";
        }else if (mmm.equalsIgnoreCase("jun")){
            fullMonth = "June";
        }else if (mmm.equalsIgnoreCase("jul")){
            fullMonth = "July";
        }else if (mmm.equalsIgnoreCase("aug")){
            fullMonth = "August";
        }else if (mmm.equalsIgnoreCase("sep")){
            fullMonth = "September";
        }else if (mmm.equalsIgnoreCase("oct")){
            fullMonth = "October";
        }else if (mmm.equalsIgnoreCase("nov")){
            fullMonth = "November";
        }else if (mmm.equalsIgnoreCase("dec")){
            fullMonth = "December";
        }
        return fullMonth;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public float getAim() {
        return aim;
    }

    public AccountsMap<Integer, Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(AccountsMap<Integer, Account> accounts) {
        this.accounts = accounts;
    }

    public void setAim(float aim) {
        this.aim = aim;
    }

    public void createValue(){

        for (Expense expense:expenses){
            value += expense.getValue();
        }
    }

    public int getExpensesProgress() {
        float expensesSpentFraction = value / aim;

        int progress = (int)(expensesSpentFraction*1000);
        evaluateProgress(progress);

        return progress;
    }

    public void evaluateProgress(int progress){

        String currentMonth = new java.text.SimpleDateFormat("MMM").format(new Date());

        if (processMonth == null || processMonth.isEmpty()){
            return;
        }

        if (!processMonth.equals(currentMonth)){
            return;
        }

        if (progress > 500 && progress <= 800) {
            sendNotification("You have spent half of your budget for "+getName());
        }else if (progress > 800 && progress < 1000){
            sendNotification("Your budget for  "+getName()+" is almost up!");
        }else if (progress >= 1000){
            sendNotification("Your have exhausted you budget for  "+getName()+"!");
        }

    }

    public void sendNotification(String message){

        if (processMonth == null || processMonth.isEmpty()){
            return;
        }

        int notificationId = Integer.valueOf(String.valueOf(notificationPrefix) + String.valueOf(id));

        Intent i = new Intent(GoalApp.context,GoalApp.class);
        i.putExtra("menuFragment","Expenses");
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        Notifications notifications = new Notifications(GoalApp.context,notificationId,notificationType,i);
        notifications.setDrawableResource(drawable);
        notifications.notify("Expenses",message);
    }

    public String getProcessMonth() {
        return processMonth;
    }

    public boolean hasProcessMonth(){

        if (processMonth == null){
            return false;
        }

        if (processMonth.isEmpty()){
            return false;
        }

        return true;
    }

    public void setProcessMonth(String processMonth) {
        this.processMonth = processMonth;
    }

    public int getColor(){
        int colorIndex = Math.abs(getName().hashCode()%ColourCodes.colourCodes.length);
        return Color.parseColor(ColourCodes.colourCodes[colorIndex]);
    }

    public int getDrawable() {
        return drawable;
    }

    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }


    @Override
    public int compareTo(@NonNull ExpenseCategory another) {

        float v1;
        float v2;

        v1 = this.value;
        v2 = another.value;

        if (v2 > v1){
            return 1;
        }else{
            return -1;
        }
    }
}
