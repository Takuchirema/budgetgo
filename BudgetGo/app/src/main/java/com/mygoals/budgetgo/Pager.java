package com.mygoals.budgetgo;

/**
 * Created by Takunda Chirema on 2017-07-23.
 */
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Takunda
 */

//Extending FragmentStatePagerAdapter
public class Pager extends FragmentStatePagerAdapter {

    //integer to count number of tabs
    int tabCount;
    Context context;

    //Constructor to the class
    public Pager(Context context, FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.tabCount= tabCount;
        this.context=context;
    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 0:
                ViewAccounts tab1 = new ViewAccounts();
                tab1.setContext(context);
                return tab1;
            case 1:
                ViewExpenses tab2 = new ViewExpenses();
                tab2.setContext(context);
                return tab2;
            case 2:
                ViewGoals tab3 = new ViewGoals();
                tab3.setContext(context);
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        super.getPageTitle(position);

        switch (position){
            case 0:
                return "Accounts";
            case 1:
                return "Expenses";
            case 2:
                return "Goals";

            default:
                return null;
        }
    }

    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount() {
        return tabCount;
    }
}
