package com.mygoals.budgetgo;

/**
 * Created by Takunda Chirema on 2017-07-22.
 */

public class Duration {

    public Enum<Period> period;
    public int length;

    public Duration(Enum<Period> period,int length){
        this.period = period;
        this.length=length;
    }

    public Enum<Period> getPeriod() {
        return period;
    }

    public void setPeriod(Enum<Period> period) {
        this.period = period;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
