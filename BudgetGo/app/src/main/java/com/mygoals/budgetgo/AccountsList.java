package com.mygoals.budgetgo;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by Takunda Chirema on 2017-07-22.
 */

public class AccountsList extends ArrayAdapter<Integer>{

    private Activity context;
    private ArrayList<Integer> web;
    private boolean getAverages = false;
    private boolean editable = true;

    public AccountsList(Activity context, ArrayList<Integer>  web) {
        super(context, R.layout.account_listview, web);

        this.context = context;
        this.web = web;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        final Account account = Accounts.get(web.get(position));

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.account_listview, null, true);

        if (account == null){
            return rowView;
        }

        TextView details = (TextView) rowView.findViewById(R.id.accountMonth);

        String accountName = "";

        if (account.getDisplayName().trim().isEmpty()) {
            accountName = account.getNames().get(0).toString();
        }else{
            accountName = account.getDisplayName();
        }

        details.setText(accountName + " for " + account.getPeriod());

        ImageView imageView = (ImageView) rowView.findViewById(R.id.img);

        TextView accountValue = (TextView)  rowView.findViewById(R.id.accountValue);

        if (getAverages && !Accounts.isCurrent(account)){
            details.setText("Average for "+accountName+" "+account.getPeriod());
            accountValue.setText(""+account.calculateAverageValue());
        }else if (!Accounts.isExpense(account.getAccountType())){
            accountValue.setText(""+account.getAccountValue());
        }else{
            accountValue.setText(""+account.getExpensesValue());
            if (account.getExpensesValue() >= account.getBudget() && account.hasProcessMonth()){
                //System.out.println(account.getName()+" "+account.getExpensesValue()+" "+account.getBudget());
                accountValue.setBackgroundResource(R.drawable.red_rectangle);
            }else {
                accountValue.setBackgroundResource(R.drawable.orange_rectangle);
            }
        }

        if (editable){
            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context,ViewAccount.class);
                    i.putExtra("accountId", account.getId());
                    i.putExtra("accountType", account.getAccountType().toString());
                    i.putExtra("expenseCategoryId", account.getExpenseCategoryId());
                    i.putExtra("accountName", account.getName());
                    i.putExtra("accountNumber", account.getAccountNumber());
                    context.startActivity(i);
                }
            });
        }

        int budgetProgress = account.getBudgetProgress();
        SeekBar expensesBar= (SeekBar) rowView.findViewById(R.id.budgetProgress);
        expensesBar.setProgress(0);
        expensesBar.setProgress(budgetProgress);
        expensesBar.setEnabled(false);

        if(Accounts.isExpense(account.getAccountType())) {
            imageView.setImageResource(account.getExpenseCategory().getDrawable());
        }else{
            imageView.setImageResource(Accounts.getAccountIcon(account.getAccountType()));
        }

        return rowView;
    }

    public void setGetAverages(boolean getAverages) {
        this.getAverages = getAverages;
    }

    public void setEditable(boolean editable){
        this.editable = editable;
    }
}
