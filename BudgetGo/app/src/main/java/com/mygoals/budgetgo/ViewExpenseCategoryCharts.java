package com.mygoals.budgetgo;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import im.dacer.androidcharts.LineView;
import im.dacer.androidcharts.PieHelper;
import im.dacer.androidcharts.PieView;

public class ViewExpenseCategoryCharts extends Fragment {

    private ExpenseCategory expenseCategory;
    private static View myFragmentView;
    private TextView currentMonthTV;

    private ImageView leftBtn;
    private ImageView rightBtn;

    private String currentMonth;
    private ArrayList<String> months = new ArrayList<>();
    private ArrayList<ArrayList<Integer>> dataset = new ArrayList<>();
    private ArrayList<PieHelper> pieDataSet = new ArrayList<>();

    private LayoutInflater inflater;
    private LinearLayout expenditureRows;
    private float totalExpenditure;

    public static final ArrayList<String> staticMonths = new ArrayList<String>()
    {{add("Jan");add("Feb");add("Mar");add("Apr");add("May");add("Jun");add("Jul");add("Aug");add("Sep");add("Oct");add("Nov");add("Dec");}};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.fragment_view_expense_category_charts, container, false);
        this.inflater = inflater;

        ((TextView)myFragmentView.findViewById(R.id.line_view_title)).setText(expenseCategory.getName()+" Timeline");

        currentMonthTV = (TextView) myFragmentView.findViewById(R.id.monthTxt);
        rightBtn = (ImageView)myFragmentView.findViewById(R.id.rightBtn);
        leftBtn = (ImageView)myFragmentView.findViewById(R.id.leftBtn);

        expenditureRows = (LinearLayout) myFragmentView.findViewById(R.id.expenditureLayout);

        setListeners();
        prepareData();
        createCharts();

        return myFragmentView;
    }

    private void setListeners(){
        rightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int index = staticMonths.indexOf(currentMonth);
                index ++;

                if (index > staticMonths.size() - 1) {
                    currentMonth = staticMonths.get(0);
                }else{
                    currentMonth = staticMonths.get(index);
                }

                prepareData();
            }
        });

        leftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int index = staticMonths.indexOf(currentMonth);
                index --;

                if (index < 0) {
                    currentMonth = staticMonths.get(staticMonths.size() - 1);
                }else{
                    currentMonth = staticMonths.get(index);
                }

                prepareData();
            }
        });
    }

    public void createCharts(){
        createLineChart();
    }

    public void prepareData(){
        dataset.clear();
        totalExpenditure = 0;
        expenditureRows.removeAllViews();

        String processMonth = new SimpleDateFormat("MMM").format(new Date());
        if (currentMonth == null){
            currentMonth = processMonth;
        }

        currentMonthTV.setText(currentMonth);

        months = getPastMonths(processMonth,6);
        ArrayList<Integer> values = getMonthValues(months);
        dataset.add(values);

        // For Expenditure table
        populateExpenditureGrid();
    }

    public void populateExpenditureGrid(){

        float totalBudget = 0;

        addRow("Account","Expense","Proposed","#26A69A","#FFFFFF",expenditureRows);

        ArrayList<Account> accounts= new ArrayList<>(expenseCategory.getAccounts().values());

        Collections.sort(accounts, new Comparator<Account>(){
            @Override
            public int compare(Account a1, Account a2) {
                return a1.compareTo(a2);
            }
        });

        for (Account account:accounts){

            if (!Accounts.isExpense(account.getAccountType())){
                continue;
            }

            float value = account.getTransactionsValue(account.getTransactions().getTransactionsForMonth(currentMonth));

            View row = addRow(account.getName(),""+value,""+account.getBudget(),null,null,expenditureRows);

            if (account.getBudget() > 0 && value > account.getBudget()){
                row.findViewById(R.id.text2).setBackgroundColor(Color.parseColor("#7F0000"));
                ((TextView)row.findViewById(R.id.text2)).setTextColor(Color.parseColor("#FFFFFF"));
            }

            totalExpenditure = totalExpenditure + value;
            totalBudget = totalBudget + account.getBudget();
            //System.out.println("expenses total: "+totalExpenditure+" value: "+value);
        }

        View row = addRow("Total",""+totalExpenditure,""+totalBudget,"#EE7600",null,expenditureRows);

        if (totalBudget > 0 && totalExpenditure > totalBudget){
            row.findViewById(R.id.text2).setBackgroundColor(Color.parseColor("#7F0000"));
            ((TextView)row.findViewById(R.id.text2)).setTextColor(Color.parseColor("#FFFFFF"));
        }

    }

    public View addRow(String text0, String text1, String text2, String color, String textColor,LinearLayout rowsLayout){

        View accountCell = inflater.inflate(R.layout.grid_row, null, true);

        TextView accountCellText = (TextView)accountCell.findViewById(R.id.text0);
        TextView currentCellText = (TextView)accountCell.findViewById(R.id.text1);
        TextView proposedCellText = (TextView)accountCell.findViewById(R.id.text2);

        if (color != null) {
            accountCellText.setBackgroundColor(Color.parseColor(color));
            currentCellText.setBackgroundColor(Color.parseColor(color));
            proposedCellText.setBackgroundColor(Color.parseColor(color));
        }

        if (textColor != null) {
            accountCellText.setTextColor(Color.parseColor(textColor));
            currentCellText.setTextColor(Color.parseColor(textColor));
            proposedCellText.setTextColor(Color.parseColor(textColor));
        }

        accountCellText.setText(text0);
        currentCellText.setText(text1);
        proposedCellText.setText(text2);

        rowsLayout.addView(accountCell);

        return accountCell;
    }

    public ArrayList<PieHelper> getPieData(){
        ArrayList<PieHelper> values = new ArrayList<>();

        AccountsMap<Integer,Account> accounts = expenseCategory.getAccounts();
        float monthExpense = accounts.getMonthValue(currentMonth);

        for (Map.Entry<Integer,Account> e:accounts.entrySet()){
            Account account = e.getValue();
            float percent =  (account.getExpensesValue()/monthExpense)*100;
            PieHelper helper = new PieHelper(percent,Accounts.getAccountColor(account.getName()));
            //System.out.println("pie data: "+percent+" "+account.getName());
            values.add(helper);
        }

        return values;
    }

    public ArrayList<Integer> getMonthValues(ArrayList<String> months){
        ArrayList<Integer> values = new ArrayList<>();
        AccountsMap<Integer,Account> accounts = expenseCategory.getAccounts();

        for (int i = 0;i<months.size();i++){
            String month = months.get(i);

            values.add(Math.round(accounts.getMonthValue(month)));
        }

        return values;
    }

    public void createLineChart() {
        LineView lineView = (LineView)myFragmentView.findViewById(R.id.line_view);
        lineView.setDrawDotLine(true);
        lineView.setBackgroundColor(Color.WHITE);
        lineView.setShowPopup(LineView.SHOW_POPUPS_MAXMIN_ONLY);
        lineView.setBottomTextList(months);
        lineView.setColorArray(new int[]{Color.BLUE});
        lineView.setDataList(dataset);
    }

    public void createPieChart(){
        PieView pieView = (PieView)myFragmentView.findViewById(R.id.pie_view);
        pieView.setDate(pieDataSet);
        pieView.selectedPie(PieView.NO_SELECTED_INDEX);
        pieView.setOnPieClickListener(listener);
        pieView.showPercentLabel(true);

        //create the accounts info
        createPieInfo();
    }

    public void createPieInfo(){
        LayoutInflater inflater = getActivity().getLayoutInflater();

        for (Account account:expenseCategory.getAccounts().values()) {

            if (account.getExpensesValue() <= 0){
                continue;
            }

            View pieInfoView = inflater.inflate(R.layout.pie_info, null, true);

            GradientDrawable background = (GradientDrawable) pieInfoView.findViewById(R.id.color).getBackground();
            background.setColor(account.getColor());

            TextView accountName = (TextView)pieInfoView.findViewById(R.id.accountName);
            accountName.setText(account.getName());

            TextView accountValue = (TextView)pieInfoView.findViewById(R.id.value);
            accountValue.setText("R"+account.getExpensesValue());

            ((LinearLayout)myFragmentView.findViewById(R.id.pie_info_container)).addView(pieInfoView);
        }
    }

    PieView.OnPieClickListener listener = new PieView.OnPieClickListener() {
        @Override public void onPieClick(int index) {
            if (index != PieView.NO_SELECTED_INDEX) {
                Account account = getAccountAt(index);
                GradientDrawable background = (GradientDrawable) myFragmentView.findViewById(R.id.color).getBackground();
                background.setColor(account.getColor());

                TextView accountName = (TextView)myFragmentView.findViewById(R.id.accountName);
                accountName.setText(account.getName());

                TextView accountValue = (TextView)myFragmentView.findViewById(R.id.value);
                accountValue.setText("R"+account.getExpensesValue());
            }
        }
    };

    public Account getAccountAt(int index){
        List<Account> l = new ArrayList<>(expenseCategory.getAccounts().values());
        return l.get(index);
    }

    public void setExpenseCategory(ExpenseCategory expenseCategory) {
        this.expenseCategory = expenseCategory;
    }

    public ArrayList<String> getPastMonths(String startingMonth, int range){
        ArrayList<String> months = new ArrayList<>();

        // Get the position of starting month
        int index = staticMonths.indexOf(startingMonth);

        if (range >= 12){
            months = staticMonths;
        }else{

            for (int i = range - 1;i>=0;i--){
                int position = index - i;
                if (position < 0){
                    position = 12 - Math.abs(position);
                }

                months.add(staticMonths.get(position));
            }
        }

        return months;
    }
}
