package com.mygoals.budgetgo;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.Iterator;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Takunda on 2018/01/12.
 */

public class SyncLinkedHashMap<K,V> extends LinkedHashMap<K,V> implements Iterable<V>{

    private Semaphore lock = new Semaphore(1);
    private Date lastAccessTime;

    @Override
    public synchronized V put(K key, V value){
        /*if (lock.isHeldByCurrentThread()) {
            try {
                //System.out.println("put lock wait");
                lock.wait();
                put(key,value);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/
        return super.put(key,value);
    }

    @Override
    public synchronized Set<Entry<K, V>> entrySet(){
        return super.entrySet();
    }

    @Override
    public synchronized V get(Object var1){
        return super.get(var1);
    }

    @Override
    public V remove(Object key) {
        ////System.out.println("remove lock wait");
        /*try {
            lock.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        return super.remove(key);
    }

    @Override
    public Iterator<V> iterator() {
        /*try {
            lock.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        final Iterator<V> iterator = super.values().iterator();

        Iterator<V> it = new Iterator<V>() {

            @Override
            public boolean hasNext() {
                /*if (!iterator.hasNext()) {
                    //System.out.println("has next lock unlocking");
                    lock.release();
                }*/
                return iterator.hasNext();
            }

            @Override
            public V next() {
                return iterator.next();
            }

            @Override
            public void remove() {
                iterator.remove();
            }
        };
        return it;
    }

}
