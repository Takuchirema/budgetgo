package com.mygoals.budgetgo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;


public class ViewIncomeAccounts extends Fragment {
    private static ArrayList<Integer> accountsList = new ArrayList<>();
    private static AccountsList adapter;
    private static ListView listView;

    private Context context;

    private static View myFragmentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.fragment_view_income_accounts, container, false);

        createAccountCategories();
        refreshAccountBalances(null);
        return myFragmentView;
    }

    public void addAccount(){
        Intent i = new Intent(context,CreateAccount.class);
        startActivity(i);
    }

    public void createAccountCategories(){
        accountsList.clear();

        if (Accounts.isEmpty()) {
            Accounts.createAccounts();
        }

        createListAdapter();
    }

    /**
     * This method takes all accounts in the map and calculates the balances
     * */
    public static void refreshAccountBalances(String processMonth){
        //System.out.println("refreshing account balances: for - "+processMonth+" "+Accounts.size());
        accountsList.clear();

        Iterator<Account> iterator = Accounts.getIterator();
        while(iterator.hasNext()){
            Account account = iterator.next();

            if (!Accounts.isCurrent(account) && !Accounts.isIncome(account.getAccountType())) {
                continue;
            }

            if (Accounts.isIncome(account.getAccountType()) && !Accounts.isTotalIncomeAccount(account)){
                continue;
            }

            accountsList.add(account.getId());

            account.setProcessMonth(processMonth);
            account.calculateAccountValue();
            //System.out.println("Account: "+account.getName()+" balance: "+account.getAccountValue()+" trans: "+account.getTransactions().size());
        }

        adapter = new AccountsList((Activity)myFragmentView.getContext(), accountsList);
        adapter.setGetAverages(true);
        adapter.setEditable(false);

        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    /**
     * Creates the adapter to be put in the list view
     * */
    public void createListAdapter(){

        LinkedHashMap<Integer, Account> accounts = new LinkedHashMap<>();

        Iterator<Account> iterator = Accounts.getIterator();
        while(iterator.hasNext()){
            Account account = iterator.next();

            if (!Accounts.isCurrent(account) && !Accounts.isIncome(account.getAccountType())) {
                continue;
            }

            if (Accounts.isIncome(account.getAccountType()) && !Accounts.isTotalIncomeAccount(account)){
                continue;
            }

            accountsList.add(account.getId());
            accounts.put(account.getId(),account);
        }

        adapter = new AccountsList((Activity)myFragmentView.getContext(), accountsList);
        adapter.setGetAverages(true);
        adapter.setEditable(false);

        listView = (ListView) myFragmentView.findViewById(R.id.accounts_list);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

            }
        });

        listView.setAdapter(adapter);
    }
}
