package com.mygoals.budgetgo;

import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by Takunda Chirema on 2017-07-22.
 */

public interface IGetData {

    public void postGetGoals(LinkedHashMap<String,Goal> goals);

    public void postGetExpenses(HashMap<String,Expense> expense);

    public void postGetShops(HashMap<String,Shop> shops);

    public void postGetAccounts(HashMap<String,Account> accounts);
}
