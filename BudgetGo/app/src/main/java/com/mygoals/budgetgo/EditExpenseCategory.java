package com.mygoals.budgetgo;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class EditExpenseCategory extends Fragment {

    private EditText expenseCategoryNameET;
    private EditText expenseBudgetET;
    private TextView saveExpenseTV;
    private TextView deleteCategoryTV;
    private ImageView selectIconIV;

    private int drawable;
    private ExpenseCategory expenseCategory;
    private static View myFragmentView;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.activity_create_expense_category, container, false);

        ((TextView)myFragmentView.findViewById(R.id.title)).setVisibility(View.GONE);

        String expenseType = expenseCategory.getName();

        expenseCategoryNameET = (EditText)myFragmentView.findViewById(R.id.expenseCategoryName);
        expenseCategoryNameET.setText(expenseType);

        expenseBudgetET = (EditText)myFragmentView.findViewById(R.id.expenseBudget);
        expenseBudgetET.setText(""+expenseCategory.getAim());

        saveExpenseTV = (TextView)myFragmentView.findViewById(R.id.saveExpense);
        saveExpenseTV.setText("Edit Expense");

        deleteCategoryTV = (TextView)myFragmentView.findViewById(R.id.deleteCategory);
        deleteCategoryTV.setVisibility(View.VISIBLE);

        selectIconIV = (ImageView)myFragmentView.findViewById(R.id.expenseIcon);
        selectIconIV.setImageResource(expenseCategory.getDrawable());
        drawable = expenseCategory.getDrawable();

        setListeners();

        return myFragmentView;
    }

    public void setListeners(){
        saveExpenseTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveExpense();
            }
        });

        deleteCategoryTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteCategory();
            }
        });

        selectIconIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createImageSelector();
            }
        });
    }

    private void createImageSelector() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View layout = inflater.inflate(R.layout.select_icon, null, true);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(layout);
        final AlertDialog alert = builder.create();

        GridLayout imagesLayout = (GridLayout)layout.findViewById(R.id.selectIcon);

        ArrayList<Integer> imageResources = GoalApp.imageResources;


        for (final Integer imageResource:imageResources){

            View imageLayout = inflater.inflate(R.layout.icon, null, true);
            ImageView image = (ImageView) imageLayout.findViewById(R.id.icon);

            try {
                Drawable myIcon = getResources().getDrawable(imageResource);
                image.setImageDrawable(myIcon);
            }catch (Exception ex)
            {
                continue;
            }

            image.setBackgroundResource(R.drawable.bordered_circle);

            imageLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    drawable = imageResource;
                    selectIconIV.setImageResource(imageResource);
                    alert.hide();
                }
            });
            imagesLayout.addView(imageLayout);
        }

        alert.show();

        Button cancel = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        LinearLayout.LayoutParams noButtonLL = (LinearLayout.LayoutParams) cancel.getLayoutParams();
        noButtonLL.gravity = Gravity.CENTER;
        noButtonLL.width = 500;
        cancel.setLayoutParams(noButtonLL);
    }

    public void saveExpense(){
        String budget = expenseBudgetET.getText().toString();
        String name = expenseCategoryNameET.getText().toString();

        if(TextUtils.isEmpty(budget)) {
            expenseBudgetET.setError("Please set your expense budget");
            return;
        }

        expenseCategory.setName(name);
        expenseCategory.createExpenses();
        expenseCategory.createValue();
        expenseCategory.setAim(Float.parseFloat(budget));
        expenseCategory.setDrawable(drawable);

        //Store into database
        expenseCategory.save();

        GoalApp.reprocessExpenses();

        getActivity().finish();
    }

    private void deleteCategory(){
        expenseCategory.delete();

        GoalApp.reprocessExpenses();
        getActivity().finish();
    }

    public void setExpenseCategory(ExpenseCategory expenseCategory) {
        this.expenseCategory = expenseCategory;
    }
}
