package com.mygoals.budgetgo;

import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormatSymbols;
import java.util.ArrayList;

public class CreateExpenseCategory extends AppCompatActivity {

    private EditText expenseCategoryET;
    private EditText expenseBudgetET;
    private TextView saveExpenseTV;
    private ImageView selectIconIV;
    private int drawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_expense_category);

        expenseCategoryET = (EditText) findViewById(R.id.expenseCategoryName);
        expenseBudgetET = (EditText)findViewById(R.id.expenseBudget);
        saveExpenseTV = (TextView)findViewById(R.id.saveExpense);
        selectIconIV = (ImageView)findViewById(R.id.expenseIcon);

        setListeners();
    }

    public void setListeners(){
        saveExpenseTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveExpense();
            }
        });

        selectIconIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createImageSelector();
            }
        });
    }

    private void createImageSelector() {
        LayoutInflater inflater = this.getLayoutInflater();
        View layout = inflater.inflate(R.layout.select_icon, null, true);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        final AlertDialog alert = builder.create();

        GridLayout imagesLayout = (GridLayout)layout.findViewById(R.id.selectIcon);

        ArrayList<Integer> imageResources = GoalApp.imageResources;


        for (final Integer imageResource:imageResources){

            View imageLayout = inflater.inflate(R.layout.icon, null, true);
            ImageView image = (ImageView) imageLayout.findViewById(R.id.icon);

            try {
                Drawable myIcon = getResources().getDrawable(imageResource);
                image.setImageDrawable(myIcon);
            }catch (Exception ex)
            {
                continue;
            }

            image.setBackgroundResource(R.drawable.bordered_circle);

            imageLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    drawable = imageResource;
                    selectIconIV.setImageResource(imageResource);
                    alert.hide();
                }
            });
            imagesLayout.addView(imageLayout);
        }

        alert.show();

        Button cancel = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        LinearLayout.LayoutParams noButtonLL = (LinearLayout.LayoutParams) cancel.getLayoutParams();
        noButtonLL.gravity = Gravity.CENTER;
        noButtonLL.width = 500;
        cancel.setLayoutParams(noButtonLL);
    }

    public void saveExpense(){
        String budget = expenseBudgetET.getText().toString();

        if(TextUtils.isEmpty(budget)) {
            expenseBudgetET.setError("Please set your expense budget");
            return;
        }

        String categoryName = expenseCategoryET.getText().toString();
        ExpenseCategory e1 = new ExpenseCategory(ViewExpenses.getExpenseCategoryList().size()+1,categoryName);
        e1.createExpenses();
        e1.setAim(Float.parseFloat(budget));
        e1.createValue();
        e1.setDrawable(drawable);

        //Store into database
        e1.save();

        GoalApp.reprocessExpenses();

        finish();
    }

}
