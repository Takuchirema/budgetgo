package com.mygoals.budgetgo;

/**
 * Created by Takunda Chirema on 2017-07-22.
 */

public enum ETransactionType {
    WITHDRAWAL,
    DEPOSIT,
    BALANCE
}
