package com.mygoals.budgetgo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Takunda Chirema on 2017-07-22.
 */

public class ViewAccountTransactions extends Fragment implements IGetData {

    private int accountId;
    private Account account;
    private LayoutInflater inflater;
    private LinearLayout transactionList;

    private static View myFragmentView;
    private static FloatingActionButton deleteBtn;
    private HashMap<Long,View> deleteTransactions = new HashMap<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.fragment_view_account_transactions, container, false);
        deleteBtn = (FloatingActionButton)myFragmentView.findViewById(R.id.delete);

        setDeleteBtnListener();

        viewTransactions();
        return myFragmentView;
    }

    private void setDeleteBtnListener(){
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (Map.Entry<Long,View>  e:deleteTransactions.entrySet()){
                    Long transactionId = e.getKey();
                    View transactionView = e.getValue();

                    Transactions.deleteTransaction(transactionId);
                    transactionList.removeView(transactionView);
                }
                GoalApp.refreshBalances();
            }
        });
    }

    public void viewTransactions(){
        account = Accounts.get(accountId);

        inflater = getActivity().getLayoutInflater();
        transactionList = (LinearLayout)myFragmentView.findViewById(R.id.transactionList);
        transactionList.removeAllViews();

        if (account!=null){
            PopulateTransactions transactions = new PopulateTransactions();
            transactions.execute();
        }else{
            //System.out.println("Account is null");
        }
    }

    class PopulateTransactions extends AsyncTask<Void, Integer, String>
    {
        protected void onPreExecute (){
            super.onPreExecute();
        }

        protected String doInBackground(Void...arg0) {
            HashMap<Long,Transaction> transactions = account.getTransactions();

            for (Map.Entry<Long,Transaction> e: transactions.entrySet()){
                final Transaction transaction = e.getValue();

                if (account.getProcessMonth()!=null && !account.getProcessMonth().isEmpty()){
                    if (!transaction.getMonth().equalsIgnoreCase(account.getProcessMonth())){
                        continue;
                    }
                }

                //System.out.println("Transaction: "+transaction.getTransactionMessage()+" account: "+account.getName());

                if(getActivity() == null)
                    return "You are at PostExecute";

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        View transactionView = inflater.inflate(R.layout.transaction, null, true);

                        setListener(transaction, transactionView);

                        TextView month = (TextView) transactionView.findViewById(R.id.transactionMonth);
                        TextView day = (TextView) transactionView.findViewById(R.id.transactionDay);
                        TextView year = (TextView) transactionView.findViewById(R.id.transactionYear);

                        TextView message = (TextView) transactionView.findViewById(R.id.transactionMessage);
                        TextView value = (TextView) transactionView.findViewById(R.id.transactionValue);

                        month.setText(transaction.getMonth());
                        day.setText(transaction.getDay());
                        year.setText(transaction.getYear());

                        String text = transaction.getTransactionMessage();
                        if (transaction.getDebitAccountId()!=0){
                            text = text +" Debit: "+ Accounts.get(transaction.getDebitAccountId()).getName();
                        }

                        if (transaction.getCreditAccountId()!=0){
                            text = text +" Credit: "+ Accounts.get(transaction.getCreditAccountId()).getName();
                        }

                        message.setText(text);
                        value.setText(""+getValueSign(transaction)*transaction.getValue());

                        transactionList.addView(transactionView);
                    }
                });

            }
            return "You are at PostExecute";
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }

    public void setListener(final Transaction transaction, final View transactionView){

        transactionView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (!deleteTransactions.containsKey(transaction.getId())) {
                    view.setBackgroundColor(Color.LTGRAY);
                    deleteTransactions.put(transaction.getId(),transactionView);
                }else{
                    view.setBackgroundColor(Color.WHITE);
                    deleteTransactions.remove(transaction.getId());
                }

                if (deleteTransactions.size() > 0){
                    deleteBtn.setVisibility(View.VISIBLE);
                }else{
                    deleteBtn.setVisibility(View.GONE);
                }

                return false;
            }
        });

    }

    public int getValueSign(Transaction transaction){
        if (transaction.getCreditAccount()!=null && transaction.getCreditAccount().getId() == this.accountId){
            return -1;
        }else{
            return 1;
        }
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    @Override
    public void postGetGoals(LinkedHashMap<String, Goal> goals) {

    }

    @Override
    public void postGetExpenses(HashMap<String, Expense> expense) {

    }

    @Override
    public void postGetShops(HashMap<String, Shop> shops) {

    }

    @Override
    public void postGetAccounts(HashMap<String, Account> accounts) {

    }
}
