package com.mygoals.budgetgo;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.provider.Telephony;
import android.text.format.DateUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Simmetrics.StringMetric;
import Simmetrics.metrics.StringMetrics;

/**
 * Created by Takunda Chirema on 2017-08-25.
 */

public class Transactions extends AsyncTask<Void, Void, ArrayList<Transaction>> {

    public static ArrayList<Transaction> sortedTransactionList = new ArrayList<>();
    public static LinkedHashMap<Long,Transaction > transactionsMap = new LinkedHashMap<>();

    private ArrayList<Account> accounts = new ArrayList<>();
    private IProcess processCaller;
    private static Context context;
    public static Date latestTransactionDate;
    public static Date earliestTransactionDate;

    public Transactions(Context context){
        this.context=context;
    }
    public boolean reprocessAccounts = false;

    public static final ArrayList<String> staticMonths = new ArrayList<String>()
    {{add("Jan");add("Feb");add("Mar");add("Apr");add("May");add("Jun");add("Jul");add("Aug");add("Sep");add("Oct");add("Nov");add("Dec");}};

    @Override
    protected ArrayList<Transaction> doInBackground(Void... params) {

        //Make sure we have accounts before processing transactions
        Accounts.getDBAccounts();

        //Get transactions from the database first
        getDBTransactions();

        if (reprocessAccounts){
            reprocessTransactions(sortedTransactionList);
            return null;
        }

        //System.out.println("reprocessing accounts: "+latestTransactionDate);

        List<SMS> smss = new ArrayList<>();
        Cursor cur = context.getContentResolver().query(Telephony.Sms.Inbox.CONTENT_URI, null, null, null, null);
        Date processDate = null;

        // If no transactions i.e. first time app usage, use the one month processing window.
        // If we processed but user changed window period to be bigger, process unprocessed transactions.
        if (latestTransactionDate == null){
            processDate = getEarliestProcessingDate();
        }else if (getEarliestProcessingDate().before(earliestTransactionDate)){
            processDate = getEarliestProcessingDate();
        }else {
            processDate = new Date(latestTransactionDate.getTime() - (1000 * 60 *60 * 24));
        }

        while (cur.moveToNext()) {
            String address = cur.getString(cur.getColumnIndex(Telephony.Sms.Inbox.ADDRESS));
            String date = cur.getString(cur.getColumnIndex(Telephony.Sms.Inbox.DATE));
            String body = cur.getString(cur.getColumnIndexOrThrow(Telephony.Sms.Inbox.BODY));

            SMS sms = new SMS(body,address);
            sms.setDate(date);

            if ( processDate == null || (processDate != null && processDate.before(sms.getActualDate()))) {
                printSMS(sms);
                smss.add(sms);
            }
        }

        ArrayList<Transaction> transactions = createTransactions(smss);

        //Store the new transactions in the database
        SQLite.storeTransactions(transactions);

        sortTransactions(transactions);

        //Transactions must be in their financial month
        adjustTransactionsCycle();

        //Put Accounts on transactions if they are not there already
        populateTransactionAccounts();

        reconcileTransactions();

        //Remove all accounts with 1 transactions or less. They are insignificant at the moment and probably erronous.
        //cleanTransactionAccounts();

        return transactions;
    }

    protected void onPostExecute(ArrayList<Transaction> transactions) {

        if (processCaller != null){
            processCaller.postGetTransactionSMS(sortedTransactionList);
        }
        ////System.out.println("Latest Balance Is: "+sortedTransactionList.get(0).getBalanceAfterTransaction());
    }

    public static void reprocessTransactions(ArrayList<Transaction> transactions){
        //System.out.println("reprocessing accounts");
        for (Transaction transaction:transactions){
            transaction.setupHints();
            transaction.searchForAccounts();
            transaction.save();
        }
    }

    public void adjustTransactionsCycle(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        boolean cycleSetting = prefs.getBoolean("salary_cycle",false);

        if (!cycleSetting){
            for (int i=0;i<sortedTransactionList.size();i++){
                Transaction transaction = sortedTransactionList.get(i);
                transaction.resetDate();
            }
            return;
        }

        for (int i=0;i<sortedTransactionList.size();i++){
            Transaction transaction = sortedTransactionList.get(i);
            adjustTransactionCycle(transaction,i);
        }

        sortTransactions(sortedTransactionList);
    }

    /**
     * Will adjust only if this transaction is from the set Salary Account.
     * Forwards adjustment will take the salary transaction and all succeeding transactions, and put them into the next month
     * Backwards adjustment will take all preceding transactions, and put them into the previous month
     * @param transaction
     */
    public void adjustTransactionCycle(Transaction transaction, int position){

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        int accountId = Integer.parseInt(prefs.getString("select_account","0"));
        int cycleType = 1;
        //int cycleType = Integer.parseInt(prefs.getString("cycle_type","1"));

        if (transaction.getDebitAccountId() != accountId && transaction.getCreditAccountId() != accountId){
            return;
        }

        int currentMonth = (new Date()).getMonth();

        if (currentMonth == transaction.getActualDate().getMonth()){
            //return;
        }

        //Reset the position
        /*//System.out.println("date: start pos - "+position);
        //System.out.println(position-4+" before date: "+sortedTransactionList.get(position-4).getActualDate()+" "+sortedTransactionList.get(position-4).getTransactionMessage());
        //System.out.println(position-3+" before date: "+sortedTransactionList.get(position-3).getActualDate()+" "+sortedTransactionList.get(position-3).getTransactionMessage());

        //System.out.println(position-2+" before date: "+sortedTransactionList.get(position-2).getActualDate()+" "+sortedTransactionList.get(position-2).getTransactionMessage());
        //System.out.println(position-1+" before date: "+sortedTransactionList.get(position-1).getActualDate()+" "+sortedTransactionList.get(position-1).getTransactionMessage());
        */
        //Get actual time of transaction from the transaction message
        //transaction.setActualDate(getTransactionRealTime(transaction));

        int dayOfMonth = transaction.getActualDate().getDate();

        if (dayOfMonth < 15){
            cycleType=2;
        }

        //Reset the position
        for (int i=position+1;i<sortedTransactionList.size();i++){
            Transaction beforeTransaction = sortedTransactionList.get(i);
            if (beforeTransaction.getActualDate().before(transaction.getActualDate())){
                //System.out.println(i+" before date: "+beforeTransaction.getActualDate()+" "+beforeTransaction.getTransactionMessage());
                break;
            }
            //System.out.println(i+" after date:");
            position++;
        }
        //System.out.println("date: end pos - "+position);

        // Forwards goes back to 0
        // Backwards goes towards end of the list
        if (cycleType == 1){
            int min = 1;

            if (transaction.getActualDate().getDate() == 1){
                return;
            }

            Date forwardDate = getCycleDate(1,transaction.getActualDate(),min);
            transaction.setActualDate(forwardDate);

            loop:
            for(int i=position-1;i>0;i--){
                Transaction adjustTransaction = sortedTransactionList.get(i);
                System.out.println(i+" adj before date: "+adjustTransaction.getActualDate()+" "+adjustTransaction.getTransactionMessage());
                if (adjustTransaction.getActualDate().getMonth() != forwardDate.getMonth()){
                    adjustTransaction.setActualDate(getCycleDate(1,adjustTransaction.getActualDate(),min));
                    min = (min + 1)%60;
                }else if (adjustTransaction.getId() == transaction.getId()) {
                    continue;
                }
                else{
                    System.out.println(i+" break before date: "+adjustTransaction.getActualDate()+" "+adjustTransaction.getTransactionMessage());
                    break loop;
                }
            }
        }else{
            Date backwardDate = getCycleDate(2,transaction.getActualDate(),0);
            int min = 59;

            loop:
            for(int i=position+1;i<sortedTransactionList.size();i++){
                Transaction adjustTransaction = sortedTransactionList.get(i);
                if (adjustTransaction.getActualDate().getMonth() != backwardDate.getMonth()){
                    adjustTransaction.setActualDate(getCycleDate(2,adjustTransaction.getActualDate(),min));
                    min = min--;
                }else{
                    break loop;
                }
            }
        }
    }

    public Date getCycleDate(int cycleType, Date date, int min){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        if (cycleType == 1) {
            cal.add(Calendar.MONTH, 1);
            cal.set(Calendar.DAY_OF_MONTH,1);
        }else{
            cal.add(Calendar.MONTH, -1);
            cal.set(Calendar.DAY_OF_MONTH,Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));
        }

        cal.set(Calendar.HOUR_OF_DAY,0);
        cal.set(Calendar.MINUTE, min);

        Date cycleDate = cal.getTime();

        //System.out.println(cycleType+" cycle date: "+cycleDate+" "+date+" min: "+min);

        return cycleDate;
    }



    /**
     * This method will fill in blanks in the current accounts.
     * It calls each individual current account and reconciles.
     */
    public void reconcileTransactions(){
        //Make sure the accounts has the expense account first
        Accounts.getUnknownExpenseAccount();

        Iterator<Account> iterator = Accounts.getIterator();
        while(iterator.hasNext()) {
            Account account = iterator.next();
            if (Accounts.isCurrent(account)) {
                account.reconcileTransactions();
            }
        }
    }

    public void populateTransactionAccounts(){
        //System.out.println("populate transaction accounts");
        if (Accounts.isEmpty()){
            //System.out.println("accounts empty");
            Accounts.getDBAccounts();
        }else{
            //System.out.println("accounts not empty");
        }

        // Clear the transactions in the accounts.
        // This is so that order is restored with new incoming transactions.
        Iterator<Account> iterator = Accounts.getIterator();
        while(iterator.hasNext()){
            Account account = iterator.next();
            account.getTransactions().clear();
        }

        for (int i=0;i<sortedTransactionList.size();i++){
            Transaction transaction = sortedTransactionList.get(i);
            //System.out.println("populate accounts: "+transaction.getActualDate()+" "+transaction.getCreditAccountId()+" "+transaction.getDebitAccountId());
            if (transaction.getCreditAccountId() != 0){
                Account creditAccount = Accounts.get(transaction.getCreditAccountId());
                if (creditAccount != null) {
                    //System.out.println("credit account id: " + transaction.getCreditAccountId());
                    transaction.setCreditAccount(creditAccount);
                    Accounts.get(transaction.getCreditAccountId()).getTransactions().put(transaction.getDate(), transaction);
                }
            }

            if (transaction.getDebitAccountId() != 0){
                Account debitAccount = Accounts.get(transaction.getDebitAccountId());

                if (debitAccount != null) {
                    //System.out.println("debit account id: " + transaction.getDebitAccountId());
                    transaction.setDebitAccount(debitAccount);
                    Accounts.get(transaction.getDebitAccountId()).getTransactions().put(transaction.getDate(), transaction);
                }
            }

            if (transaction.isIncome()){
                Account incomeAccount = Accounts.getTotalIncomeAccount();
                //System.out.println("Transaction is income: "+transaction.getTransactionMessage()+" "+incomeAccount.getId());
                incomeAccount.getTransactions().put(transaction.getDate(),transaction);
            }
        }
    }

    public void cleanTransactionAccounts(){
        //System.out.println("clean transaction accounts");

        ArrayList<Account> deletedAccounts = new ArrayList<>();

        Iterator<Account> iterator = Accounts.getIterator();
        while(iterator.hasNext()){
            Account account = iterator.next();

            if (account.getTransactions().size() <= 1){
                deletedAccounts.add(account);
            }
        }

        for (Account account:deletedAccounts){
            //System.out.println("deleted: "+account.getName());
            TransactionsMap<Long,Transaction> transactions = account.getTransactions();

            account.delete();

            for (Map.Entry<Long,Transaction> t:transactions.entrySet()){
                Transaction transaction = t.getValue();
                transaction.setupHints();
                transaction.searchForAccounts();
            }
        }
    }

    public static void getDBTransactions(){
        Date earliestProcessingDate = getEarliestProcessingDate();

        transactionsMap = SQLite.getTransactions(earliestProcessingDate);
        if (!transactionsMap.isEmpty()){
            sortedTransactionList.clear();
            ArrayList<Transaction> transactions = new ArrayList(transactionsMap.values());
            sortTransactions(transactions);
        }
    }

    public static Date getEarliestProcessingDate(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        int processingWindow = Math.abs(Integer.parseInt(prefs.getString("processing_window","1")));

        Calendar cal = Calendar.getInstance();

        cal.add(Calendar.MONTH, -1*processingWindow);
        cal.set(Calendar.DAY_OF_MONTH,Calendar.getInstance().getActualMinimum(Calendar.DAY_OF_MONTH));

        cal.set(Calendar.HOUR_OF_DAY,0);
        cal.set(Calendar.MINUTE, 1);

        Date date = cal.getTime();

        System.out.println("earliest processing date: "+date.toString()+" added "+(-1*processingWindow));
        return date;
    }

    public static void storeDBTransaction(Transaction transaction){
        ArrayList<Transaction> transactions = new ArrayList<>();
        transactions.add(transaction);
        SQLite.storeTransactions(transactions);
    }

    public static void storeDBTransactions(ArrayList<Transaction> transactions){
        SQLite.storeTransactions(transactions);
    }

    public static void deleteTransaction(Long transactionId){
        Transaction transaction = transactionsMap.get(transactionId);
        boolean deleted = transaction.delete();

        if (deleted){
            int creditAccountId = transaction.getCreditAccountId();
            int debitAccountId = transaction.getDebitAccountId();

            if (creditAccountId > 0){
                Accounts.get(creditAccountId).getTransactions().remove(transactionId);
            }

            if (debitAccountId > 0){
                Accounts.get(debitAccountId).getTransactions().remove(transactionId);
            }

            sortedTransactionList.clear();
            getDBTransactions();
        }

    }

    private static void sortTransactions(ArrayList<Transaction> transactions){
        sortedTransactionList.addAll(transactions);
        Collections.sort(sortedTransactionList);
        if (!sortedTransactionList.isEmpty()) {
            latestTransactionDate = sortedTransactionList.get(0).getActualDate();
            earliestTransactionDate = sortedTransactionList.get(sortedTransactionList.size()-1).getActualDate();
            //System.out.println("latest transaction date: " + latestTransactionDate.toString());
        }
    }

    public ArrayList<Transaction> createTransactions(List<SMS> smss){
        ArrayList<Transaction> transactions = new ArrayList<>();

        for (SMS sms:smss){
            Transaction transaction = new Transaction(sms.getDate(),sms.getSms());
            transaction.setDate(sms.getDate());
            transaction.setActualDate(sms.getActualDate());
            if (transaction.getValue() > 0 && transaction.getBalanceAfterTransaction() > 0) {
                transactions.add(transaction);
            }
            printTransaction(transaction);
        }
        return transactions;
    }

    private void printSMS(SMS sms){
        //System.out.println("********* SMS *************");
        //System.out.println("Transaction Id/Number: "+sms.getPhoneNumber());
        //System.out.println("Date: "+sms.getDate());
        //System.out.println("Sms: "+sms.getSms());
        //System.out.println("********** SMS *************");
    }

    public static void printTransaction(Transaction transaction){
        Account debitAccount = transaction.getDebitAccount();
        Account creditAccount = transaction.getCreditAccount();

        //System.out.println("********* Transaction *************");
        //System.out.println("Transaction Id/Number: "+transaction.getId());
        //System.out.println("Date: "+transaction.getDate());
        //System.out.println("TR Sms: "+transaction.getTransactionMessage());
        //System.out.println("Value: "+transaction.getValue());
        //System.out.println("Balance after: "+transaction.getBalanceAfterTransaction());
        if (debitAccount!=null)
            //System.out.println("Debit Account: "+debitAccount.getName()+" "+debitAccount.getAccountNumber());

        if (creditAccount!=null) {
            //System.out.println("Credit Account: "+creditAccount.getName()+" "+creditAccount.getAccountNumber());
        }

        //System.out.println("********** Transaction *************");
    }

    public static ArrayList<Transaction> getTransactions() {
        return sortedTransactionList;
    }

    public static void setTransactions(ArrayList<Transaction> transactions) {
        Transactions.sortedTransactionList = transactions;
    }

    public ArrayList<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(ArrayList<Account> accounts) {
        this.accounts = accounts;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public IProcess getProcessCaller() {
        return processCaller;
    }

    public void setProcessCaller(IProcess processCaller) {
        this.processCaller = processCaller;
    }

    public boolean reprocessAccounts() {
        return reprocessAccounts;
    }

    public void setReprocessAccounts(boolean reprocessAccounts) {
        this.reprocessAccounts = reprocessAccounts;
    }
}
