package com.mygoals.budgetgo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.util.Pair;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.woxthebox.draglistview.DragItemAdapter;
import com.woxthebox.draglistview.DragListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Takunda Chirema on 2017-07-22.
 */

public class GoalList extends DragItemAdapter<Integer, GoalList.ViewHolder>{

    private final Activity context;
    private final ArrayList<Integer> web;
    private final LinkedHashMap<Integer, Goal> goals;
    private int mLayoutId;
    private int mGrabHandleId;
    private boolean mDragOnLongPress;

    public GoalList(int layoutId, int grabHandleId, boolean dragOnLongPress,
                    Activity context, ArrayList<Integer> web, LinkedHashMap<Integer, Goal> goals) {
        mLayoutId = layoutId;
        mGrabHandleId = grabHandleId;
        mDragOnLongPress = dragOnLongPress;
        setHasStableIds(true);
        setItemList(web);

        this.context = context;
        this.web = web;
        this.goals = goals;

        //System.out.println("GoalList onCreateViewHolder");
    }

    public Activity getContext() {
        return context;
    }

    public ArrayList<Integer> getWeb() {
        return web;
    }

    public LinkedHashMap<Integer, Goal> getGoals() {
        return goals;
    }

    public List<Integer> getList(){
        return mItemList;
    }

    public void printList()
    {
        //System.out.println("****************");
        String listItems="";

        for (int id:mItemList){
            listItems = listItems+" "+id;
        }

        //System.out.println(listItems);
        //System.out.println("****************");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate(mLayoutId, parent, false);
        return new ViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        final Goal goal = goals.get(web.get(position));
        View rowView = holder.getMainView();

        rowView.setLongClickable(true);
        rowView.setClickable(true);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);
        txtTitle.setText(goal.getDescription());

        TextView goalInfo = (TextView) rowView.findViewById(R.id.infoText);
        goalInfo.setText(goal.getValue()+" saved "+(goal.getAim() - goal.getValue())+" to go!");

        SeekBar goalProgress = (SeekBar) rowView.findViewById(R.id.goalProgress);

        goalProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                int percent = progress/10;
                if (goal.getProgress() < goal.getRequiredProgress()){
                    seekBar.setThumb(getThumb(percent,R.drawable.red_circle));
                }else{
                    seekBar.setThumb(getThumb(percent,R.drawable.green_circle));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                seekBar.setThumb(getThumb(0,R.drawable.green_circle));

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        goalProgress.setProgress(0);
        goalProgress.setProgress(goal.getProgress());
        goalProgress.setEnabled(false);

        TextView goalOnTrackInfo = (TextView) rowView.findViewById(R.id.onTrackInfo);
        if (goal.getProgress() < goal.getRequiredProgress()){
            goalOnTrackInfo.setText(goal.getRequiredValue()+" is expected!");
            goalOnTrackInfo.setTextColor(Color.RED);
        }else{
            if (goal.isComplete()) {
                goalOnTrackInfo.setText("Goal is complete!!");
            }else{
                goalOnTrackInfo.setText("You are on Track!");
            }
            goalOnTrackInfo.setTextColor(R.color.darkGreen);
        }

        //don't want to see thumb
        goalProgress.getThumb().mutate().setAlpha(255);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.img);

        holder.mGrabView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //System.out.println("Goal Clicked");
                Intent i = new Intent(context,EditGoal.class);
                i.putExtra("id", goal.getId());
                context.startActivity(i);
            }
        });

        if (goal.isComplete()){
            imageView.setImageResource(R.drawable.complete);
            rowView.setBackgroundResource(R.color.tintGreen);
        }else{
            imageView.setImageResource(goal.getDrawable());
        }
    }

    public Drawable getThumb(int progress,int circle) {
        View thumbView = LayoutInflater.from(context).inflate(R.layout.layout_seekbar_thumb, null, false);
        thumbView.setBackgroundResource(circle);

        ((TextView) thumbView.findViewById(R.id.tvProgress)).setText(progress + "%");

        thumbView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        Bitmap bitmap = Bitmap.createBitmap(thumbView.getMeasuredWidth(), thumbView.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        thumbView.layout(0, 0, thumbView.getMeasuredWidth(), thumbView.getMeasuredHeight());
        thumbView.draw(canvas);

        return new BitmapDrawable(context.getResources(), bitmap);
    }

    @Override
    public long getItemId(int position) {
        return web.get(position);
    }

    class ViewHolder extends DragItemAdapter.ViewHolder {
        View mainView;

        ViewHolder(final View itemView) {
            super(itemView, mGrabHandleId, mDragOnLongPress);
            mainView = itemView;
        }

        @Override
        public void onItemClicked(View view) {
            Toast.makeText(getContext(), "Item clicked", Toast.LENGTH_SHORT).show();
        }

        @Override
        public boolean onItemLongClicked(View view) {
            Toast.makeText(view.getContext(), "Item long clicked", Toast.LENGTH_SHORT).show();
            return true;
        }

        public View getMainView(){
            return mainView;
        }
    }
}
