package com.mygoals.budgetgo;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import im.dacer.androidcharts.LineView;
import im.dacer.androidcharts.PieHelper;
import im.dacer.androidcharts.PieView;

public class ViewSavingsCharts extends Fragment {

    private ArrayList<String> months = new ArrayList<>();
    private ArrayList<ArrayList<Integer>> dataset = new ArrayList<>();
    private ArrayList<PieHelper> pieDataSet = new ArrayList<>();
    private ArrayList<Account> savingsAccounts = new ArrayList<>();
    private static View myFragmentView;

    private TextView currentMonthTV;

    private ImageView leftBtn;
    private ImageView rightBtn;

    private LayoutInflater inflater;
    private LinearLayout savingsRows;
    private float totalSavings;

    private String currentMonth;

    public static final ArrayList<String> staticMonths = new ArrayList<String>()
    {{add("Jan");add("Feb");add("Mar");add("Apr");add("May");add("Jun");add("Jul");add("Aug");add("Sep");add("Oct");add("Nov");add("Dec");}};
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.fragment_view_expense_category_charts, container, false);
        super.onCreate(savedInstanceState);
        this.inflater = inflater;

        ((TextView)myFragmentView.findViewById(R.id.line_view_title)).setText("Savings Timeline");

        currentMonthTV = (TextView) myFragmentView.findViewById(R.id.monthTxt);
        rightBtn = (ImageView)myFragmentView.findViewById(R.id.rightBtn);
        leftBtn = (ImageView)myFragmentView.findViewById(R.id.leftBtn);

        savingsRows = (LinearLayout) myFragmentView.findViewById(R.id.expenditureLayout);

        setListeners();
        prepareData();
        createCharts();
        
        return myFragmentView;
    }

    private void setListeners(){
        rightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int index = staticMonths.indexOf(currentMonth);
                index ++;

                if (index > staticMonths.size() - 1) {
                    currentMonth = staticMonths.get(0);
                }else{
                    currentMonth = staticMonths.get(index);
                }

                prepareData();
            }
        });

        leftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int index = staticMonths.indexOf(currentMonth);
                index --;

                if (index < 0) {
                    currentMonth = staticMonths.get(staticMonths.size() - 1);
                }else{
                    currentMonth = staticMonths.get(index);
                }

                prepareData();
            }
        });
    }

    public void createCharts(){
        createLineChart();
    }

    public void prepareData(){
        dataset.clear();
        totalSavings = 0;
        savingsRows.removeAllViews();

        String processMonth = GoalApp.processMonth;
        if (processMonth == null || processMonth.isEmpty()) {
            processMonth = new SimpleDateFormat("MMM").format(new Date());
        }

        if (currentMonth == null){
            currentMonth = processMonth;
        }
        currentMonthTV.setText(currentMonth);

        months = getPastMonths(processMonth,6);
        ArrayList<Integer> values = getMonthValues(months);
        dataset.add(values);

        populateSavingsGrid();
    }

    public void populateSavingsGrid(){

        float totalBudget = 0;

        addRow("Account","Savings","Proposed","#26A69A","#FFFFFF",savingsRows);

        ArrayList<Account> accounts= new ArrayList<>(Accounts.values());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            accounts.sort(new Comparator<Account>() {
                @Override
                public int compare(Account a1, Account a2) {
                    return a1.compareTo(a2);
                }
            });
        }

        for (Account account:accounts){

            if (account.getAccountType() != EAccountType.SAVINGS){
                continue;
            }

            float value = account.getTransactionsValue(account.getTransactions().getTransactionsForMonth(currentMonth));

            View row = addRow(account.getName(),""+value,""+account.getBudget(),null,null,savingsRows);

            if (account.getBudget() > 0 && value < account.getBudget()){
                row.findViewById(R.id.text2).setBackgroundColor(Color.parseColor("#7F0000"));
                ((TextView)row.findViewById(R.id.text2)).setTextColor(Color.parseColor("#FFFFFF"));
            }

            totalSavings = totalSavings + value;
            totalBudget = totalBudget + account.getBudget();
            //System.out.println("expenses total: "+totalSavings+" value: "+value);
        }

        View row = addRow("Total",""+totalSavings,""+totalBudget,"#EE7600",null,savingsRows);

        if (totalBudget > 0 && totalSavings < totalBudget){
            row.findViewById(R.id.text2).setBackgroundColor(Color.parseColor("#7F0000"));
            ((TextView)row.findViewById(R.id.text2)).setTextColor(Color.parseColor("#FFFFFF"));
        }

    }

    public View addRow(String text0, String text1, String text2, String color, String textColor,LinearLayout rowsLayout){

        View accountCell = inflater.inflate(R.layout.grid_row, null, true);

        TextView accountCellText = (TextView)accountCell.findViewById(R.id.text0);
        TextView currentCellText = (TextView)accountCell.findViewById(R.id.text1);
        TextView proposedCellText = (TextView)accountCell.findViewById(R.id.text2);

        if (color != null) {
            accountCellText.setBackgroundColor(Color.parseColor(color));
            currentCellText.setBackgroundColor(Color.parseColor(color));
            proposedCellText.setBackgroundColor(Color.parseColor(color));
        }

        if (textColor != null) {
            accountCellText.setTextColor(Color.parseColor(textColor));
            currentCellText.setTextColor(Color.parseColor(textColor));
            proposedCellText.setTextColor(Color.parseColor(textColor));
        }

        accountCellText.setText(text0);
        currentCellText.setText(text1);
        proposedCellText.setText(text2);

        rowsLayout.addView(accountCell);

        return accountCell;
    }

    public ArrayList<PieHelper> getPieData(){
        ArrayList<PieHelper> values = new ArrayList<>();

        // Put in savings
        Iterator<Account> iterator = Accounts.getIterator();
        while(iterator.hasNext()){
            Account account = iterator.next();

            if (account.getAccountType() != EAccountType.SAVINGS){
                continue;
            }

            savingsAccounts.add(account);
            float percent =  (account.getAccountValue()/totalSavings)*100;
            PieHelper helper = new PieHelper(percent,Accounts.getAccountColor(account.getName()));
            //System.out.println("savings pie data: "+percent+" "+account.getName()+" "+account.getAccountValue());
            values.add(helper);
        }

        return values;
    }

    public ArrayList<Integer> getMonthValues(ArrayList<String> months){
        ArrayList<Integer> values = new ArrayList<>();
        totalSavings = 0;

        for (int i = 0;i<months.size();i++){
            String month = months.get(i);

            Iterator<Account> iterator = Accounts.getIterator();
            while(iterator.hasNext()){
                Account account = iterator.next();

                if (account.getAccountType() != EAccountType.SAVINGS) {
                    continue;
                }

                ArrayList<Transaction> monthTransactions = account.getTransactions().getTransactionsForMonth(month);

                float amount = Math.max(0,account.getTransactionsValue(monthTransactions));
                values.add(Math.round(amount));
                totalSavings += amount;
            }
        }
        return values;
    }

    public void createLineChart() {
        LineView lineView = (LineView)myFragmentView.findViewById(R.id.line_view);
        lineView.setDrawDotLine(true);
        lineView.setBackgroundColor(Color.WHITE);
        lineView.setShowPopup(LineView.SHOW_POPUPS_MAXMIN_ONLY);
        lineView.setBottomTextList(months);
        lineView.setColorArray(new int[]{Color.BLUE});
        lineView.setDataList(dataset);
    }

    public void createPieChart(){
        PieView pieView = (PieView)myFragmentView.findViewById(R.id.pie_view);
        pieView.setDate(pieDataSet);
        pieView.selectedPie(PieView.NO_SELECTED_INDEX);
        pieView.setOnPieClickListener(listener);
        pieView.showPercentLabel(true);

        //create the accounts info
        createPieInfo();
    }

    public void createPieInfo(){
        LayoutInflater inflater = getActivity().getLayoutInflater();

        for (Account account:savingsAccounts) {

            if (account.getAccountValue() <= 0){
                continue;
            }

            View pieInfoView = inflater.inflate(R.layout.pie_info, null, true);

            GradientDrawable background = (GradientDrawable) pieInfoView.findViewById(R.id.color).getBackground();
            background.setColor(account.getColor());

            TextView accountName = (TextView)pieInfoView.findViewById(R.id.accountName);
            accountName.setText(account.getName());

            TextView accountValue = (TextView)pieInfoView.findViewById(R.id.value);
            accountValue.setText("R"+account.getAccountValue());
            accountValue.setVisibility(View.VISIBLE);

            ((LinearLayout)myFragmentView.findViewById(R.id.pie_info_container)).addView(pieInfoView);
        }
    }

    PieView.OnPieClickListener listener = new PieView.OnPieClickListener() {
        @Override public void onPieClick(int index) {
            if (index != PieView.NO_SELECTED_INDEX) {
                GradientDrawable background = (GradientDrawable) myFragmentView.findViewById(R.id.color).getBackground();
                TextView accountName = (TextView) myFragmentView.findViewById(R.id.accountName);
                TextView accountValue = (TextView)myFragmentView.findViewById(R.id.value);

                Account account = getAccountAt(index);
                background.setColor(account.getColor());
                accountName.setText(account.getName());

                accountValue.setText("R"+account.getAccountValue());
            }
        }
    };

    public ExpenseCategory getCategoryAt(int index){
        List<ExpenseCategory> l = new ArrayList<>(ExpenseCategories.expenseCategoriesMap.values());
        if (index >= l.size()){
            return null;
        }
        return l.get(index);
    }

    public Account getAccountAt(int index){
        return savingsAccounts.get(index);
    }

    public ArrayList<String> getPastMonths(String startingMonth, int range){
        ArrayList<String> months = new ArrayList<>();

        // Get the position of starting month
        int index = staticMonths.indexOf(startingMonth);

        if (range >= 12){
            months = staticMonths;
        }else{

            for (int i = range - 1;i>=0;i--){
                int position = index - i;
                if (position < 0){
                    position = 12 - Math.abs(position);
                }

                months.add(staticMonths.get(position));
            }
        }

        return months;
    }
}
