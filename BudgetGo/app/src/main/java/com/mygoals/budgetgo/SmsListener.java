package com.mygoals.budgetgo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

/**
 * Created by Takunda Chirema  on 2017-11-04.
 */

public class SmsListener extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        //System.out.println("Incomed message");
        GoalApp.processNewMessages();
    }
}
