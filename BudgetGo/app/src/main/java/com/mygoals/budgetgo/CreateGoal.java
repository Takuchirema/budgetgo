package com.mygoals.budgetgo;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.text.DateFormatSymbols;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

public class CreateGoal extends AppCompatActivity implements DatePickerDialog.OnDateSetListener  {

    private TextView startDayTV;
    private TextView endDayTV;
    private EditText descriptionET;
    private EditText valueET;
    private Context context;
    private EditText goalNameET;
    private TextView saveGoalTV;
    private TextView savingsAccountTV;
    private int savingsAccountId;
    private DatePickerDialog datePickerDialog;
    private ImageView selectIconIV;

    private String startDate;
    private String endDate;
    private int drawable;

    private boolean setStartDate = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_goal);

        context = CreateGoal.this;
        startDayTV = (TextView)findViewById(R.id.startDate);
        endDayTV = (TextView)findViewById(R.id.endDate);
        descriptionET = (EditText)findViewById(R.id.goalDescription);
        valueET = (EditText)findViewById(R.id.goalValue);
        goalNameET = (EditText)findViewById(R.id.goalName);
        saveGoalTV = (TextView)findViewById(R.id.saveGoal);
        savingsAccountTV = (TextView)findViewById(R.id.selectSavingsAccount);
        selectIconIV = (ImageView)findViewById(R.id.goalIcon);

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new DatePickerDialog(context, CreateGoal.this, year, month, day);
        setListeners();
    }

    private void saveGoal(){

        String value = valueET.getText().toString();
        String name = goalNameET.getText().toString();
        String description = descriptionET.getText().toString();

        if (startDate == null || endDate == null){
            Toast.makeText(CreateGoal.this, "End date must be after start date", Toast.LENGTH_LONG).show();
            return;
        }

        if(TextUtils.isEmpty(value)) {
            valueET.setError("Please set goal value");
            return;
        }

        if(TextUtils.isEmpty(name)) {
            goalNameET.setError("Please set the goal name");
            return;
        }

        if(TextUtils.isEmpty(description)) {
            valueET.setError("Please set goal description");
            return;
        }

        if (savingsAccountId == 0){
            Toast.makeText(CreateGoal.this, "Please select as savings account", Toast.LENGTH_LONG).show();
            return;
        }

        //System.out.println("period: "+startDate+" "+endDate);

        Goal g1 = new Goal(0);
        g1.setDescription(description);
        g1.setStartDay(startDate);
        g1.setEndDay(endDate);
        g1.setAim(Float.parseFloat(value));
        g1.setName(name);
        g1.setSavingsAccountId(savingsAccountId);
        g1.setDrawable(drawable);

        // Assuming GoalsMap is sorted
        g1.setRelativePriority(Goals.leastPriority);

        //Store into database
        g1.save();

        GoalApp.reloadGoalProgress();

        finish();
    }

    private void setListeners(){
        startDayTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStartDate = true;
                datePickerDialog.show();
            }
        });

        endDayTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStartDate = false;
                datePickerDialog.show();
            }
        });

        saveGoalTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveGoal();
            }
        });

        savingsAccountTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createAccountSelector();
            }
        });

        selectIconIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createImageSelector();
            }
        });
    }

    private void createImageSelector() {
        LayoutInflater inflater = this.getLayoutInflater();
        View layout = inflater.inflate(R.layout.select_icon, null, true);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        final AlertDialog alert = builder.create();

        GridLayout imagesLayout = (GridLayout)layout.findViewById(R.id.selectIcon);

        ArrayList<Integer> imageResources = GoalApp.imageResources;


        for (final Integer imageResource:imageResources){

            View imageLayout = inflater.inflate(R.layout.icon, null, true);
            ImageView image = (ImageView) imageLayout.findViewById(R.id.icon);

            try {
                Drawable myIcon = getResources().getDrawable(imageResource);
                image.setImageDrawable(myIcon);
            }catch (Exception ex)
            {
                continue;
            }

            image.setBackgroundResource(R.drawable.bordered_circle);

            imageLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    drawable = imageResource;
                    selectIconIV.setImageResource(imageResource);
                    alert.hide();
                }
            });
            imagesLayout.addView(imageLayout);
        }

        alert.show();

        Button cancel = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        LinearLayout.LayoutParams noButtonLL = (LinearLayout.LayoutParams) cancel.getLayoutParams();
        noButtonLL.gravity = Gravity.CENTER;
        noButtonLL.width = 500;
        cancel.setLayoutParams(noButtonLL);
    }

    private void createAccountSelector() {
        LayoutInflater inflater = this.getLayoutInflater();
        View layout = inflater.inflate(R.layout.select_account, null, true);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        final AlertDialog alert = builder.create();

        LinearLayout accountsLayout = (LinearLayout)layout.findViewById(R.id.selectAccount);

        Iterator<Account> iterator = Accounts.getIterator();
        while(iterator.hasNext()){
            final Account account = iterator.next();

            if (account.getAccountType() != EAccountType.SAVINGS){
                continue;
            }

            View rowView= inflater.inflate(R.layout.account_listview, null, true);

            final TextView details = (TextView) rowView.findViewById(R.id.accountMonth);
            if (account.getDisplayName().trim().isEmpty()) {
                details.setText(account.getNames().get(0).toString());
            }else{
                details.setText(account.getDisplayName());
            }

            ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
            TextView accountValue = (TextView)  rowView.findViewById(R.id.accountValue);

            accountValue.setText(""+account.getAccountValue());
            imageView.setImageResource(account.getExpenseCategory().getDrawable());

            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    savingsAccountId = account.getId();
                    savingsAccountTV.setText(details.getText());
                    alert.hide();
                }
            });

            accountsLayout.addView(rowView);
        }

        alert.show();

        Button cancel = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        LinearLayout.LayoutParams noButtonLL = (LinearLayout.LayoutParams) cancel.getLayoutParams();
        noButtonLL.gravity = Gravity.CENTER;
        noButtonLL.width = 500;
        cancel.setLayoutParams(noButtonLL);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        monthOfYear += 1;
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        try {
            if (setStartDate) {
                Date startDateD = formatter.parse(dayOfMonth + "-" + monthOfYear + "-" + year);

                if (endDate != null) {
                    Date endDateD = formatter.parse(endDate);
                    if (endDateD.before(startDateD)) {
                        Toast.makeText(CreateGoal.this, "End date must be after start date", Toast.LENGTH_LONG).show();
                        return;
                    }
                }

                String day = ""+dayOfMonth;
                String month = ""+monthOfYear;

                if ((""+dayOfMonth).length()==1) {
                    day = "0"+dayOfMonth;
                }

                if ((""+monthOfYear).length()==1) {
                    month = "0"+monthOfYear;
                }

                startDate = day + "-" + month + "-" + year;
                startDayTV.setText(dayOfMonth + "-" + monthOfYear + "-" + year);
            } else {
                Date endDateD = formatter.parse(dayOfMonth + "-" + monthOfYear + "-" + year);

                if (startDate != null) {
                    Date startDateD = formatter.parse(startDate);
                    if (startDateD.after(endDateD)) {
                        Toast.makeText(CreateGoal.this, "End date must be after start date", Toast.LENGTH_LONG).show();
                        return;
                    }
                }

                String day = ""+dayOfMonth;
                String month = ""+monthOfYear;

                //month of year seems to be a day behind not starting from 0
                monthOfYear++;

                if ((""+dayOfMonth).length()==1) {
                    day = "0"+dayOfMonth;
                }

                if ((""+monthOfYear).length()==1) {
                    month = "0"+monthOfYear;
                }

                endDate = day + "-" + month + "-" + year;
                endDayTV.setText(dayOfMonth + "-" + monthOfYear + "-" + year);
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
