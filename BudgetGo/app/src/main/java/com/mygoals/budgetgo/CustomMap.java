package com.mygoals.budgetgo;

import java.util.LinkedHashMap;

/**
 * Created by Takunda Chirema  on 2017-11-07.
 */

public class CustomMap<K,V> extends LinkedHashMap<K,V> {

    @Override
    public V put(K k, V v){

        BeforePut(v);

        super.put(k,v);

        AfterPut(v);

        return v;
    }

    public void BeforePut(Object v){

    }

    public void AfterPut(Object v){

    }

}
