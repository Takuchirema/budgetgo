package com.mygoals.budgetgo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Takunda Chirema  on 2017-10-21.
 */

public class Goals {

    public static SyncLinkedHashMap<Integer, Goal> goalsMap = new SyncLinkedHashMap<>();
    public static int leastPriority;

    public static void createTestGoals(){
        Goal g1 = new Goal(1);
        g1.setAim(50000);
        g1.setStartDay("01-07-2017");
        g1.setEndDay("30-07-2017");
        g1.setGoalType(EGoalType.Vacation);
        g1.setDescription("Go to vacation Italy!");
        g1.setDuration(new Duration(Period.MONTHS,5));
        g1.setRelativePriority(1);
        g1.setValue(5000);

        Goal g2 = new Goal(2);
        g2.setAim(20000);
        g2.setStartDay("01-07-2017");
        g2.setEndDay("30-07-2017");
        g2.setGoalType(EGoalType.Car);
        g2.setDescription("Buy an Apartment!");
        g2.setDuration(new Duration(Period.MONTHS,5));
        g2.setRelativePriority(1);
        g2.setValue(10000);

        Goal g3 = new Goal(3);
        g3.setAim(50000);
        g3.setStartDay("01-07-2017");
        g3.setEndDay("30-07-2017");
        g3.setGoalType(EGoalType.Car);
        g3.setDescription("Machine Learning Course");
        g3.setDuration(new Duration(Period.MONTHS,5));
        g3.setRelativePriority(1);
        g3.setValue(20000);

        Goal g4 = new Goal(4);
        g4.setAim(1000);
        g4.setStartDay("01-07-2017");
        g4.setEndDay("30-07-2017");
        g4.setGoalType(EGoalType.Car);
        g4.setDescription("Christmas!");
        g4.setDuration(new Duration(Period.MONTHS,5));
        g4.setRelativePriority(1);
        g4.setValue(500);

        goalsMap.put(g1.getId(),g1);
        goalsMap.put(g2.getId(),g2);
        goalsMap.put(g3.getId(),g3);
        goalsMap.put(g4.getId(),g4);
    }

    public static void save(){
        ArrayList<Goal> goals = new ArrayList(goalsMap.values());
        SQLite.storeGoals(goals);
    }

    public static void remove(int goalId){
        if (goalsMap.containsKey(goalId)){
            goalsMap.remove(goalId);
        }
    }

    public static void getDBGoals(){
        goalsMap = SQLite.getGoals();
        sortGoals();
    }

    public static void sortGoals(){
        List<Map.Entry<Integer, Goal>> entries = new ArrayList<>(goalsMap.entrySet());

        Collections.sort(entries, new Comparator<Map.Entry<Integer, Goal>>() {
            public int compare(Map.Entry<Integer, Goal> a, Map.Entry<Integer, Goal> b){
                return a.getValue().compareTo(b.getValue());
            }
        });

        if (!entries.isEmpty()) {
            leastPriority = entries.get(entries.size() - 1).getValue().getRelativePriority();
        }

        goalsMap = new SyncLinkedHashMap<>();
        for (Map.Entry<Integer, Goal> entry : entries) {
            goalsMap.put(entry.getKey(), entry.getValue());
        }
    }

    public static void storeDBGoal(Goal goal){
        ArrayList<Goal> goals = new ArrayList<>();
        goals.add(goal);
        SQLite.storeGoals(goals);
    }

    public static void storeDBGoals(ArrayList<Goal> goals){
        SQLite.storeGoals(goals);
    }

    public static EGoalType getGoalType(String goalType){

        if (goalType.equalsIgnoreCase("vacation")){
            return EGoalType.Vacation;
        }else if (goalType.equalsIgnoreCase("car")){
            return EGoalType.Car;
        }else if (goalType.equalsIgnoreCase("house/apartment")){
            return EGoalType.House;
        }else if (goalType.equalsIgnoreCase("study")){
            return EGoalType.Study;
        }else if (goalType.equalsIgnoreCase("family")){
            return EGoalType.Family;
        }
        return EGoalType.Vacation;
    }

    public static int getGoalIcon(EGoalType goalType) {
        switch (goalType) {
            case Vacation:
                return  R.drawable.vacation;
            case Study:
                return  R.drawable.school_fees;
            case House:
                return  R.drawable.agency;
            case Car:
                return  R.drawable.car;
            case Family:
                return  R.drawable.family;
        }

        return  R.drawable.miscellaneous;
    }
}
