package com.mygoals.budgetgo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * Created by Takunda Chirema  on 2017-11-07.
 */

public class AccountsMap<K,V> extends CustomMap<K,V> {

    public float getMonthValue(String month){
        float value = 0;

        Collection<Account> values = (Collection<Account>) values();

        for (Account account:values){
            if (Accounts.isExpense(account.getAccountType())) {
                value = value + account.getTransactions().getValueForMonth(month);
            }
        }

        return value;
    }
}
