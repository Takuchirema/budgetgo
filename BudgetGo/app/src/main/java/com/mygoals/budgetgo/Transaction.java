package com.mygoals.budgetgo;

import android.content.ContentValues;
import android.text.TextUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Simmetrics.StringMetric;
import Simmetrics.metrics.StringMetrics;

/**
 * Created by Takunda Chirema on 2017-08-24.
 */

public class Transaction implements Comparable<Transaction>, Cloneable{

    private Account account;
    private float value;

    //Just needed for finding accounts. Not used after that.
    private String valueNoSpace;
    private float balanceAfterTransaction;
    private Long date;
    private Date actualDate;
    private ETransactionType transactionType;
    private String transactionMessage;
    private Long id;
    private HashMap<ETransactionType,ArrayList<String>> transactionTypeHints = new HashMap<>();
    private HashMap<String,ArrayList<String>> keyAccountsHints = new HashMap<>();
    private ArrayList<String> commonWordsHints = new ArrayList<>();
    private ArrayList<String> cutOffPoints = new ArrayList<>();

    public static final ArrayList<String> staticMonths = new ArrayList<String>()
    {{add("Jan");add("Feb");add("Mar");add("Apr");add("May");add("Jun");add("Jul");add("Aug");add("Sep");add("Oct");add("Nov");add("Dec");}};

    //debit account is the account getting the money and credit account is the account money has been deducted from
    private Account debitAccount;
    private Account creditAccount;
    private boolean income = false;
    private int debitAccountId;
    private int creditAccountId;

    public Transaction(Long id,String transactionMessage){
        setTransactionMessage(transactionMessage);
        this.id=id;
        setDate(id);

        setupHints();
        searchForValue();
        searchForAccounts();
    }

    public Transaction(Long id){
        this.id=id;
        setDate(id);
    }

    public String getHour(){
        return new SimpleDateFormat("hh").format(actualDate);
    }

    public String getMin(){
        return new SimpleDateFormat("mm").format(actualDate);
    }

    public String getMonth(){
        return new SimpleDateFormat("MMM").format(actualDate);
    }

    public String getDay(){
        return new SimpleDateFormat("dd").format(actualDate);
    }

    public String getYear(){
        return new SimpleDateFormat("yyyy").format(actualDate);
    }

    public void setupHints(){
        ArrayList<String> withdrawalHints = new ArrayList<String>();
        withdrawalHints.add("withdrawal");
        withdrawalHints.add("transfer");
        withdrawalHints.add("withdrew");
        withdrawalHints.add("purchased");
        withdrawalHints.add("bought");
        withdrawalHints.add("paid");
        withdrawalHints.add("payment");
        withdrawalHints.add("topup");

        ArrayList<String> depositHints = new ArrayList<String>();
        depositHints.add("deposit");

        ArrayList<String> balanceHints = new ArrayList<String>();
        balanceHints.add("balance");
        balanceHints.add("available");
        balanceHints.add("bal");
        balanceHints.add("avail");

        transactionTypeHints.put(ETransactionType.WITHDRAWAL,withdrawalHints);
        transactionTypeHints.put(ETransactionType.BALANCE,balanceHints);
        transactionTypeHints.put(ETransactionType.DEPOSIT,depositHints);

        ArrayList<String> debitAccountHints = new ArrayList<String>();
        debitAccountHints.add("into");

        ArrayList<String> creditAccountHints = new ArrayList<String>();
        creditAccountHints.add("from");
        creditAccountHints.add("processed");

        keyAccountsHints.put("debit",debitAccountHints);
        keyAccountsHints.put("credit",creditAccountHints);

        commonWordsHints.addAll(withdrawalHints);
        commonWordsHints.addAll(depositHints);
        commonWordsHints.addAll(balanceHints);
        commonWordsHints.addAll(debitAccountHints);
        commonWordsHints.addAll(creditAccountHints);

        commonWordsHints.add("account");
        commonWordsHints.add("debit");
        commonWordsHints.add("credit");
        commonWordsHints.add("eft");
        commonWordsHints.add("bank");
        commonWordsHints.add("transfer");
        commonWordsHints.add("transaction");
        commonWordsHints.add("reference");
        commonWordsHints.add("card");
        commonWordsHints.add("card");
        commonWordsHints.add("queries");
        commonWordsHints.add("query");

        commonWordsHints.add("at");
        commonWordsHints.add("on");
        commonWordsHints.add("your");
        commonWordsHints.add("with");
        commonWordsHints.add("from");
        commonWordsHints.add("have");
        commonWordsHints.add("no");
        commonWordsHints.add("or");
        commonWordsHints.add("was");
    }

    public ArrayList<String> getCutoffPoints(){
        if (cutOffPoints.isEmpty()){
            cutOffPoints.add("queries");
        }
        return cutOffPoints;
    }

    /**
     * This method checks which account was debited and which was credited.
     * First looks to see which two or one account was affected by transaction
     * Then checks how they were affected. Does this by looking for 2 key words 'From' and 'Into'
     * If the account which was taken 'From' is found then the other was being put into and vice versa
     * @return
     */
    public void searchForAccounts(){
        creditAccount = null;
        debitAccount = null;
        creditAccountId = 0;
        debitAccountId = 0;

        if (value == 0 || balanceAfterTransaction ==0){
            return;
        }

        //System.out.println("search string 0: "+transactionMessage);
        //first prepare string for account search
        String searchForAccountsStr = accountsSearchString();
        //System.out.println("search string 1: "+searchForAccountsStr);

        //get account with the key words of 'Into' or 'From' and get remaining word subset for second account search
        searchForAccountsStr = getKeyAccount(searchForAccountsStr);

        //System.out.println("search string 2: "+searchForAccountsStr);

        //key account has to be there
        if (searchForAccountsStr == null){
            return;
        }

        //remove unnecessary characters
        searchForAccountsStr.replace("*","");

        //System.out.println("search string 3: "+searchForAccountsStr);

        ArrayList<String> hints = new ArrayList<>(Arrays.asList(searchForAccountsStr.split("\\s+")));
        Account account = searchForAccount(hints);

        //System.out.println("search string 4: "+searchForAccountsStr);
        if (account == null || (debitAccount == null && creditAccount == null)){
            //Find the most likely account name
            account = searchForPossibleAccount();
        }else{
            //System.out.println("not searching for possible account: ");
            if (account != null){
                //System.out.println(account.getName());
            }
        }

        if (account != null) {
            account.getTransactions().put(date,this);
        }

        if (account != null && debitAccount == null){
            debitAccount = account;
        }else if (account != null && creditAccount == null){
            creditAccount = account;

            //If this is an income transaction but coming from savings then its not really income
            if (income && creditAccount.getAccountType() == EAccountType.SAVINGS){
                income = false;
            }
        }
    }

    public ContentValues getContentValues(){
        ContentValues data = new ContentValues();

        data.put("Message",transactionMessage);
        data.put("Value",value);
        data.put("Balance",balanceAfterTransaction);
        data.put("DebitAccountId",getDebitAccountId());
        data.put("CreditAccountId",getCreditAccountId());
        data.put("IsIncome",income);

        return data;
    }

    //This class save the account to the db
    public void save(){
        Transaction newTransaction = SQLite.storeTransaction(this);

        Account creditAccount =  Accounts.get(newTransaction.getCreditAccountId());
        Account debitAccount =  Accounts.get(newTransaction.getDebitAccountId());

        if (creditAccount != null){
            creditAccount.getTransactions().put(newTransaction.getDate(),newTransaction);
        }

        if (debitAccount != null){
            debitAccount.getTransactions().put(newTransaction.getDate(),newTransaction);
        }
    }

    public boolean delete(){
        return SQLite.deleteTransaction(this.id);
    }

    private String getKeyAccount(String search){

        String[] searchArr = search.split("\\s+");

        for (int i=0;i<searchArr.length;i++){
            String word = searchArr[i];
            if (keyAccountsHints.get("debit").contains(word)){
                ArrayList<String> hints = new ArrayList<>();
                hints.add(searchArr[Math.min(searchArr.length-1,i+1)]);
                hints.add(searchArr[Math.min(searchArr.length-1,i+2)]);
                hints.add(searchArr[Math.min(searchArr.length-1,i+3)]);
                hints.add(searchArr[Math.min(searchArr.length-1,i+4)]);

                Account intoAccount = searchForAccount(hints);

                if (intoAccount != null) {
                    intoAccount.getTransactions().put(date,this);

                    // If the account being debited is current account then this is income
                    income = true;
                }

                debitAccount = intoAccount;

                searchArr[i] = "";

                String searchStr = TextUtils.join(" ",searchArr);
                return searchStr.replaceAll("\\s{2,}", " ").trim();
            }else if (keyAccountsHints.get("credit").contains(word)){
                ArrayList<String> hints = new ArrayList<>();
                hints.add(searchArr[Math.min(searchArr.length-1,i+1)]);
                hints.add(searchArr[Math.min(searchArr.length-1,i+2)]);
                hints.add(searchArr[Math.min(searchArr.length-1,i+3)]);
                hints.add(searchArr[Math.min(searchArr.length-1,i+4)]);

                Account fromAccount = searchForAccount(hints);
                if (fromAccount != null) {
                    fromAccount.getTransactions().put(date,this);
                }

                creditAccount = fromAccount;

                searchArr[i] = "";

                String searchStr = TextUtils.join(" ",searchArr);
                return searchStr.replaceAll("\\s{2,}", " ").trim();
            }
        }
        return null;
    }

    public void createIncomeTransaction(Account account){
        Account incomeAccount = Accounts.getTotalIncomeAccount();

        Transaction transaction = (Transaction) clone();
        transaction.setDebitAccount(incomeAccount);

        incomeAccount.getTransactions().put(transaction.getDate(),transaction);
    }

    private Account searchForAccount(ArrayList<String> hints){

        for (String word:hints){

            word = word.toLowerCase();

            if (isDate(word)){
                continue;
            }

            if (similarToValues(word)){
                continue;
            }

            Iterator<Account> iterator = Accounts.getIterator();
            while(iterator.hasNext()){
                Account account = iterator.next();
                ArrayList<String> accountNames = account.getNames();
                String accountNumber = account.getAccountNumber();

                if (accountNumber != null && !accountNumber.isEmpty()){
                    ////System.out.println("search for acc: "+account.getName()+" acc num: "+accountNumber+"---");
                    if (accountNumber.endsWith(word) || word.endsWith(accountNumber)){

                        if (hasAccount(account)) {
                            continue;
                        }

                        commonWordsHints.add(word);

                        //System.out.println("acc num search string: "+word+" num: "+accountNumber);
                        return account;
                    }
                }else {
                    StringMetric metric = StringMetrics.levenshtein();
                    for (String accountName:accountNames) {
                        ////System.out.println("1 search for acc: "+accountName+" word: "+word);
                        accountName = accountName.toLowerCase();

                        String[] accountNameArray = accountName.split("\\s+");

                        // If word is big enough and contained in account name then assume
                        if (accountNames.size() == 1 && accountName.length() > 5 && word.length() >= 5
                                && (accountName.contains(word) || word.contains(accountName)) ){
                            //System.out.println("contained acc name search string: " + word + " num: " + accountName);
                            return account;
                        }

                        float resultName;
                        if (accountNameArray.length == 1) {
                            resultName = metric.compare(accountName, word);
                        } else {
                            resultName = similarityCheck(accountNameArray, hints);
                        }

                        float cutOff = (float)0.9;

                        //if (word.length() <= 3 && accountName.length() <= 3){
                          //  cutOff = 1;
                        //}

                        ////System.out.println("search for acc: result "+resultName);
                        if (resultName >= cutOff) {

                            if (hasAccount(account)) {
                                continue;
                            }
                            commonWordsHints.add(word);
                            //System.out.println("acc name search string: " + word + " num: " + accountName);
                            return account;
                        }
                    }
                }
            }
        }
        return null;
    }

    private boolean hasAccount(Account account){
        if (creditAccount != null){
            if (account.getId() == creditAccount.getId()){
                return true;
            }
        }else if (debitAccount != null){
            if (account.getId() == debitAccount.getId()){
                return true;
            }
        }
        return false;
    }

    public boolean similarToValues(String check){
        StringMetric metric = StringMetrics.levenshtein();

        float valueCompare = metric.compare(""+value, check);
        float balanceCompare = metric.compare(""+balanceAfterTransaction, check);

        if (valueCompare > 0.5 || balanceCompare > 0.5){
            return true;
        }

        return false;
    }

    private Account searchForPossibleAccount(){

        //System.out.println("0.1 possible account: "+valueNoSpace+" "+value);
        if (valueNoSpace == null) {
            searchForValue();
        }

        if (valueNoSpace == null) {
            return null;
        }

        int valuePosition = transactionMessage.indexOf(valueNoSpace);
        String accountSearchString = transactionMessage.substring(valuePosition+valueNoSpace.length()+1);

        String[] accountSearchStringArray = accountSearchString.split(" ");

        ArrayList<String> possibleAccounts = new ArrayList<>();
        String possibleAccountNames = "";
        //System.out.println("0.1 possible account: "+accountSearchString);
        int lastPosition = 0;
        //Remove all words which are not account names.
        outerloop:
        for (int i=0;i<Math.min(20,accountSearchStringArray.length);i++){
            String possibleAccount = accountSearchStringArray[i].toLowerCase();
            //System.out.println("1 possible account: "+possibleAccount);

            String phoneNumberRegex = "^[0-9]{10}$";

            if (isDate(possibleAccount)){
                continue;
            }

            if (similarToValues(possibleAccount)){
                continue;
            }

            possibleAccount = possibleAccount.replaceAll("[^A-Za-z0-9]", "");

            //System.out.println("2 possible account: "+possibleAccount);
            if (possibleAccount.trim().equals("")){
                continue;
            }

            if (possibleAccount.matches(phoneNumberRegex)){
                continue;
            }

            //System.out.println("3 possible account: "+possibleAccount);
            for(String word:commonWordsHints) {
                if (word.length() <= 3){
                    if (word.equalsIgnoreCase(possibleAccount)){
                        continue outerloop;
                    }
                }else {
                    if (word.toLowerCase().contains(possibleAccount) || possibleAccount.contains(word.toLowerCase())) {
                        continue outerloop;
                    }
                }
            }

            //System.out.println("possible account: "+possibleAccount);
            possibleAccountNames = possibleAccountNames+","+possibleAccount;

            //System.out.println(possibleAccount+" - value: "+value+"balance: "+balanceAfterTransaction+" "+valueCompare+" "+balanceCompare);

            // So that one name e.g. Down South, will not be double counted
            if (lastPosition == 0 || i > lastPosition + 1){
                possibleAccounts.add(possibleAccount);
            }

            lastPosition = i;
        }

        if (possibleAccounts.isEmpty()){
            //System.out.println("possible accounts are empty");
            return null;
        }

        String possibleAccountName = possibleAccounts.get(0);

        //System.out.println("creating possible accounts: "+possibleAccounts.toString());

        Account newAccount = createPossibleAccount(possibleAccountName,possibleAccountNames);

        for (int i=1;i<possibleAccounts.size();i++){
            createPossibleAccount(possibleAccounts.get(i),possibleAccountNames);
        }

        return newAccount;
    }

    public boolean isDate(String name){

        String day = getDay();
        String month = getMonth();
        String year = getYear();

        String formatted = new SimpleDateFormat("ddMMyy").format(actualDate);

        if (name.contains(formatted)){
            return true;
        }

        // Check for transaction date
        if (name.contains(day) && name.toLowerCase().contains(month.toLowerCase())){
            return true;
        }

        if (name.equals(year)){
            return true;
        }

        // Check for transaction time
        if (name.contains(":")){
            if (name.matches("^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$")){
                return true;
            }
        }

        ////System.out.println("is date: name - "+name+" "+day+" "+month+" "+year);

        return false;
    }

    public Account createPossibleAccount(String accountName, String possibleAccountNames){
        Account account = new Account(0);
        account.setPhoneNumber("+27839206");
        account.setAccountType(EAccountType.EXPENSE);
        account.setName(accountName);
        account.setDisplayName(accountName);
        account.setBudget(1000);
        account.setExpenseCategoryId(ExpenseCategories.getDefaultCategory().getId());
        account.setExpensesValue(0);
        account.setPossibleAccountNames(possibleAccountNames);

        Account newAccount = SQLite.storeAccount(account);
        Accounts.put(newAccount.getId(),newAccount);

        return newAccount;
    }

    /**
     * Gets the words in the account name and checks them against the hints
     * */
    public float similarityCheck(String[] accountNameArray,ArrayList<String> hints){

        float similarity = 0;
        StringMetric metric = StringMetrics.levenshtein();

        String word1 = accountNameArray[0];
        //get position of most similar word1 in hints
        outerloop:
        for (int i=0;i<hints.size();i++){
            String hint = hints.get(i);
            float similarityCheck1 = metric.compare(word1.toLowerCase(),hint.toLowerCase());
            ////System.out.println("in similarity check sim1: "+word1+" "+hint+" similarity: "+similarityCheck1);
            //if first word is similar to something in hints
            //check the other words in hints just after that one to see if its the word
            if (similarityCheck1>0.7){
                for (int j=1;j<accountNameArray.length;j++){
                    if (i+j >= hints.size()){
                        return 0;
                    }else {
                        String word2 = accountNameArray[j];
                        String hint2 = hints.get(i+j);
                        ////System.out.println("in similarity check sim2: "+word1+" "+hint+" "+word2+" "+hint2);

                        float similarityCheck2 = metric.compare(word2.toLowerCase(), hint2.toLowerCase());
                        if (similarityCheck2 < 0.7){
                            continue outerloop;
                        }
                    }
                }
                return (float)1.0;
            }
        }
        return similarity;
    }

    /**
     * Removes the words before the first value and after the second value
     * Only exception is exception words
     * Also removes special characters
     * @return
     */
    public String accountsSearchString(){
        String[] transactionMessageArr = transactionMessage.split("\\s+");

        //Remove the first word up to the colon.
        //This is the one showing the Bank account on which transaction happens
        //e.g. Nedbank: or Standard Bank:
        //Remove because one bank might have many accounts e.g. current and savings account

        /*for (int i=0;i<transactionMessageArr.length;i++){
            String word = transactionMessageArr[i];
            if (!word.contains(""+value) && !word.contains("withdrawal")){
                transactionMessageArr[i]="";
            }else{
                break;
            }
        }*/

        //Remove useless info
        String searchStr = TextUtils.join(" ",transactionMessageArr);
        searchStr = searchStr.replaceAll("[^\\w\\s]", "");

        return searchStr.replaceAll("\\s{2,}", " ").trim();
    }

    public float searchForValue(){
        //String regex= "([R])\\s*([.\\d,]+)";
        //String regex= "([ ]+[R])\\s*([0-9]*[\\.,]?[0-9]+)";
        String regex= "([R])\\s*([0-9]*[\\.,][0-9]+)";
        Matcher matched = Pattern.compile(regex).matcher(transactionMessage);
        while (matched.find()){
            String value = matched.group();
            String valueNoSpace = value.replaceAll("\\s","");

            //System.out.println("space: "+value+" no space: "+valueNoSpace+" "+transactionMessage);

            //Pad the value no space so that it stands out
            transactionMessage = transactionMessage.replace(value," "+valueNoSpace+" ");

            ETransactionType transactionType = searchForTransactionType(valueNoSpace);

            if (transactionType != null && transactionType == ETransactionType.BALANCE){
                try {
                    balanceAfterTransaction = Float.parseFloat(valueNoSpace.substring(1));
                    //System.out.println("transaction balance found: "+valueNoSpace+" "+transactionMessage);
                }catch(Exception ex){
                    //System.out.println("transaction balance not found: "+balanceAfterTransaction+" "+transactionMessage);
                }
            }else {
                this.transactionType = transactionType;
                try {
                    this.valueNoSpace = valueNoSpace;
                    this.value = Float.parseFloat(valueNoSpace.substring(1));
                    //System.out.println("transaction value found: "+valueNoSpace+" "+transactionMessage);
                }catch(Exception ex){
                    //System.out.println("transaction value not found: "+valueNoSpace+" "+transactionMessage);
                }
            }
        }
        return 0;
    }

    /**
     * Checks the words before the value to determine whether it was withdrawal, deposit or an account balance
     * @param value
     * @return
     */
    public ETransactionType searchForTransactionType(String value){

        String[] splitMessage = transactionMessage.split("\\s+");

        for (int i=0;i<splitMessage.length;i++){
            String word = splitMessage[i];

            if (value.equalsIgnoreCase(word)){
                ArrayList<String> hints = new ArrayList<>();
                hints.add(splitMessage[Math.max(0,i-1)]);
                hints.add(splitMessage[Math.max(0,i-2)]);
                hints.add(splitMessage[Math.max(0,i-3)]);
                hints.add(splitMessage[Math.min(splitMessage.length-1,i+1)]);
                hints.add(splitMessage[Math.min(splitMessage.length-1,i+2)]);
                return getTransactionType(hints);
            }
        }

        return null;
    }

    private ETransactionType getTransactionType(ArrayList<String> hints){
        for (Map.Entry<ETransactionType,ArrayList<String>> e:transactionTypeHints.entrySet()){
            ETransactionType transactionType = e.getKey();
            ArrayList<String> transactionTypeHints = e.getValue();

            for (String typeHint:transactionTypeHints){

                for (String hint:hints){
                    if (hint.length() <= 3) {
                        //System.out.println("checking hints: " + hint + " " + typeHint + " " + transactionMessage);
                        if (typeHint.equalsIgnoreCase(hint)) {
                            return transactionType;
                        }
                    }else{
                        //System.out.println("checking hints: " + hint + " " + typeHint + " " + transactionMessage);
                        if (typeHint.contains(hint.toLowerCase())) {
                            return transactionType;
                        }
                    }
                }
            }
        }
        return null;
    }

    public Object clone()
    {
        try
        {
            Transaction newTransaction = new Transaction(this.id);
            newTransaction.setTransactionMessage(this.transactionMessage);
            newTransaction.setDate(this.date);
            newTransaction.setActualDate(this.actualDate);
            return newTransaction;
        }
        catch( Exception ex)
        {
            return null;
        }
    }

    public String getTransactionDate(){
        return null;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {

        if (this.actualDate != null){
            return;
        }

        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(date);
            this.actualDate = getRealTime(calendar.getTime());
            this.date = actualDate.getTime();
        } catch (Exception ex) {
            //System.out.println("db: error dating " + ex.getMessage());
        }
    }

    /**
     * Tries to find the actual date rather than sms date from the transaction message.
     * @param smsDate
     * @return
     */
    public Date getRealTime(Date smsDate){
        System.out.println("real date: find - "+transactionMessage);
        Calendar cal = Calendar.getInstance();
        cal.setTime(smsDate);

        String timeRegex= "([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]";
        String monthRegex = "(\\d{1,2}\\s*?(?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|(Nov|Dec)(?:ember)?)\\s*\\d{2,4})";

        Matcher timeMatched = Pattern.compile(timeRegex).matcher(transactionMessage);
        Matcher dateMatched = Pattern.compile(monthRegex).matcher(transactionMessage);

        while (timeMatched.find()) {
            String time = timeMatched.group();
            String hourStr = time.split(":")[0];
            String minStr = time.split(":")[1];

            int hour = Integer.parseInt(hourStr);
            int min = Integer.parseInt(minStr);

            cal.set(Calendar.HOUR_OF_DAY,hour);
            cal.set(Calendar.MINUTE,min);

            System.out.println("real date: hr - "+hour+" min - "+min);
        }

        while (dateMatched.find()) {
            String date = dateMatched.group();

            String[] dateSplit = date.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
            ////System.out.println("array date: "+ Arrays.toString(dateSplit));

            int dateInt = Integer.parseInt(dateSplit[0]);

            int dateYear = Integer.parseInt(dateSplit[2]);

            String month = dateSplit[1].substring(0,3);

            if (dateSplit[2].length() == 2) {
                dateYear = Integer.parseInt("20"+dateSplit[2]);
            }

            cal.set(Calendar.DATE,dateInt);
            cal.set(Calendar.YEAR,dateYear);
            cal.set(Calendar.MONTH,staticMonths.indexOf(month));

            ////System.out.println("real date: month - "+month+" date - "+dateInt+" year - "+dateYear);
        }

        System.out.println("real date: "+cal.getTime());
        return cal.getTime();
    }

    public ETransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(ETransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransactionMessage() {
        return transactionMessage;
    }

    public void setTransactionMessage(String transactionMessage) {
        this.transactionMessage = transactionMessage.replaceAll("[^A-Za-z0-9.:, ]", "");

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public float getBalanceAfterTransaction() {
        return balanceAfterTransaction;
    }

    public void setBalanceAfterTransaction(float balanceAfterTransaction) {
        this.balanceAfterTransaction = balanceAfterTransaction;
    }

    public HashMap<ETransactionType, ArrayList<String>> getTransactionTypeHints() {
        return transactionTypeHints;
    }

    public void setTransactionTypeHints(HashMap<ETransactionType, ArrayList<String>> transactionTypeHints) {
        this.transactionTypeHints = transactionTypeHints;
    }

    public Date getActualDate() {
        return actualDate;
    }

    public void setActualDate(Date actualDate) {
        this.actualDate = actualDate;
    }

    public void resetDate(){
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(id);
            this.actualDate = calendar.getTime();
        } catch (Exception ex) {
            //System.out.println("db: error dating " + ex.getMessage());
        }
    }

    @Override
    public int compareTo(Transaction another) {
        //if (actualDate.before(another.actualDate)){
        if (actualDate.getTime() < another.actualDate.getTime()){
            //System.out.println("1 compare: "+id+" "+actualDate+" to: "+another.getId()+" "+another.getActualDate());
            return 1;
        //}else if (actualDate.equals(another.actualDate)){
        }else if (actualDate.getTime() == another.actualDate.getTime()){
            //System.out.println("-1 compare: "+id+" "+actualDate+" to: "+another.getId()+" "+another.getActualDate());
            return 0;
        }else{
            //System.out.println("-1 compare: "+id+" "+actualDate+" to: "+another.getId()+" "+another.getActualDate());
            return -1;
        }
    }

    public Account getDebitAccount() {
        return debitAccount;
    }

    public void setDebitAccount(Account debitAccount) {
        this.debitAccount = debitAccount;
    }

    public Account getCreditAccount() {
        return creditAccount;
    }

    public void setCreditAccount(Account creditAccount) {
        this.creditAccount = creditAccount;
    }

    public int getDebitAccountId() {
        if (debitAccount!=null){
            return debitAccount.getId();
        }
        return debitAccountId;
    }

    public void setDebitAccountId(int debitAccountId) {
        this.debitAccountId = debitAccountId;
    }

    public int getCreditAccountId() {
        if (creditAccount!=null){
            return creditAccount.getId();
        }
        return creditAccountId;
    }

    public void setCreditAccountId(int creditAccountId) {
        this.creditAccountId = creditAccountId;
    }

    public boolean isIncome() {
        return income;
    }

    public void setIncome(boolean income) {
        this.income = income;
    }
}
