package com.mygoals.budgetgo;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CreateAccount extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{

    private Spinner accountTypeSP;
    private Spinner expenseCategorySP;
    private EditText accountNumberET;
    private EditText accountNameET;
    private EditText accountBudgetET;
    private EditText accountBalanceET;
    private TextView saveAccountTV;
    private TextView balanceAsAtTV;
    private DatePickerDialog datePickerDialog;
    private Date balanceDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        accountTypeSP = (Spinner)findViewById(R.id.selectAccountType);
        expenseCategorySP = (Spinner)findViewById(R.id.selectExpenseCategory);
        accountNumberET = (EditText)findViewById(R.id.accountNumber);
        accountNameET = (EditText)findViewById(R.id.displayName);
        saveAccountTV = (TextView)findViewById(R.id.saveAccount);
        accountBalanceET = (EditText)findViewById(R.id.accountBalance);
        accountBudgetET = (EditText)findViewById(R.id.accountBudget);
        balanceAsAtTV = (TextView)findViewById(R.id.balanceTxt);

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new DatePickerDialog(this, this, year, month, day);

        setSpinner();
        setListeners();
    }

    private void setSpinner(){
        List<StringWithTag> list = ExpenseCategories.getSpinnerItems();
        ArrayAdapter<StringWithTag> adapter = new ArrayAdapter<StringWithTag>(this, R.layout.spinner_text, list);
        expenseCategorySP.setAdapter(adapter);
    }

    public void setListeners(){
        saveAccountTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveAccount();
            }
        });

        balanceAsAtTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });

        accountTypeSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                EAccountType type = Accounts.getAccountType(accountTypeSP.getSelectedItem().toString());
                setActions(type);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    private void setActions(EAccountType type){
        if (!Accounts.isExpense(type)){
            findViewById(R.id.accountNumberContainer).setVisibility(View.VISIBLE);
            if (type == EAccountType.SAVINGS){
                findViewById(R.id.accountBalanceContainer).setVisibility(View.VISIBLE);
            }
        }else{
            findViewById(R.id.accountBudgetContainer).setVisibility(View.VISIBLE);
            findViewById(R.id.selectExpenseCategoryContainer).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        month += 1;

        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        try {
            balanceDate = formatter.parse(dayOfMonth + "-" + month + "-" + year);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String balanceDate = dayOfMonth + "-" + month + "-" + year;
        balanceAsAtTV.setText(balanceDate);
    }

    public void saveAccount(){
        String accountName = accountNameET.getText().toString();
        String accountBudgetStr = accountBudgetET.getText().toString();
        String accountBalanceStr = accountBalanceET.getText().toString();
        String accountNumberStr = accountNumberET.getText().toString();

        EAccountType accountType = Accounts.getAccountType(accountTypeSP.getSelectedItem().toString());

        StringWithTag selectedItem = (StringWithTag) expenseCategorySP.getSelectedItem();
        int expenseCategoryId = selectedItem.tag;

        Account a1 = new Account(0);

        if(TextUtils.isEmpty(accountName)) {
            accountNameET.setError("Please set the account name");
            return;
        }

        if (!Accounts.isExpense(accountType) && !Accounts.isIncome(accountType)) {
            if (TextUtils.isEmpty(accountNumberStr)) {
                accountNumberET.setError("Please set the account number");
                return;
            }
        }

        if (!TextUtils.isEmpty(accountBudgetStr)) {
            Float accountBudget = Float.parseFloat(accountBudgetStr);
            a1.setBudget(accountBudget);
        }

        if (!TextUtils.isEmpty(accountBalanceStr)) {
            Float accountBalance = Float.parseFloat(accountBalanceStr);
            a1.setBalanceAsAt(accountBalance);
            if (balanceDate != null){
                a1.setBalanceDate(balanceDate);
            }
        }

        if (!TextUtils.isEmpty(accountNumberStr)) {
            a1.setAccountNumber(accountNumberStr);
        }

        a1.setName(accountName);
        a1.setDisplayName(accountName);
        a1.setAccountType(Accounts.getAccountType(accountTypeSP.getSelectedItem().toString()));
        a1.setExpenseCategoryId(expenseCategoryId);
        a1.setExpensesValue(0);

        ExpenseCategory expenseCategory = ViewExpenses.getExpenseCategoriesMap().get(getExpenseAccountId());
        if (expenseCategory != null) {
            ViewExpenses.getExpenseCategoriesMap().get(getExpenseAccountId()).getAccounts().put(a1.getId(), a1);
        }

        //Store into database
        a1.save();

        GoalApp.reprocessTransactionAccounts();

        finish();
    }

    public EAccountType getAccountType(){
        String selected = accountTypeSP.getSelectedItem().toString();

        if (selected.equalsIgnoreCase("shop")){
            return EAccountType.SHOP;
        }else if (selected.equalsIgnoreCase("hospital")){
            return EAccountType.HOSPITAL;
        }else if (selected.equalsIgnoreCase("agency")){
            return EAccountType.AGENCY;
        }else if (selected.equalsIgnoreCase("savings")){
            return EAccountType.SAVINGS;
        }else if (selected.equalsIgnoreCase("current")){
            return EAccountType.CURRENT;
        }else if (selected.equalsIgnoreCase("cheque")){
            return EAccountType.CHEQUE;
        }else if (selected.equalsIgnoreCase("school")){
            return EAccountType.SCHOOL;
        }else if (selected.equalsIgnoreCase("Other")){
            return EAccountType.OTHER;
        }

        return EAccountType.SHOP;
    }

    public int getExpenseAccountId(){
        String selected = expenseCategorySP.getSelectedItem().toString();

        if (selected.equalsIgnoreCase("rent")){
            return 1;
        }else if (selected.equalsIgnoreCase("food")){
            return 2;
        }else if (selected.equalsIgnoreCase("School Fees")){
            return 3;
        }else if (selected.equalsIgnoreCase("Electricity")){
            return 4;
        }else if (selected.equalsIgnoreCase("Transport")){
            return 5;
        }else if (selected.equalsIgnoreCase("Medical")){
            return 6;
        }else if (selected.equalsIgnoreCase("Insurance")){
            return 7;
        }else if (selected.equalsIgnoreCase("Other")){
            return 8;
        }

        return 1;
    }
}
