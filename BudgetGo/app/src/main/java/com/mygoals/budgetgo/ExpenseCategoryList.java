package com.mygoals.budgetgo;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by Takunda Chirema on 2017-07-22.
 */

public class ExpenseCategoryList extends ArrayAdapter<Integer>{

    private final Activity context;
    private final ArrayList<Integer> web;
    private final LinkedHashMap<Integer, ExpenseCategory> expenseCategories;

    public ExpenseCategoryList(Activity context, ArrayList<Integer>  web, LinkedHashMap<Integer, ExpenseCategory> expenseCategories) {
        super(context, R.layout.expense_category_listview, web);

        this.context = context;
        this.web = web;
        this.expenseCategories = expenseCategories;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        final ExpenseCategory expenseCategory = expenseCategories.get(web.get(position));

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.expense_category_listview, null, true);
        TextView category = (TextView) rowView.findViewById(R.id.expenseMonth);
        category.setText(expenseCategory.getName()+" for "+expenseCategory.getPeriod());

        ImageView imageView = (ImageView) rowView.findViewById(R.id.img);

        TextView expenseCategoryValue = (TextView)  rowView.findViewById(R.id.expenseValue);
        expenseCategoryValue.setText(""+expenseCategory.getValue());

        int expenseProgress = expenseCategory.getExpensesProgress();

        if (expenseProgress < 1000 || !expenseCategory.hasProcessMonth()){
            expenseCategoryValue.setBackgroundResource(R.drawable.orange_rectangle);
        }else if (expenseProgress >= 1000){
            expenseCategoryValue.setBackgroundResource(R.drawable.red_rectangle);
        }

        SeekBar expensesBar = (SeekBar) rowView.findViewById(R.id.expenseProgress);

        expensesBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                int percent = progress/10;
                if (expenseCategory.getExpensesProgress() >= 1000){
                    seekBar.setThumb(getThumb(percent,R.drawable.red_circle));
                }else{
                    seekBar.setThumb(getThumb(percent,R.drawable.green_circle));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                seekBar.setThumb(getThumb(0,R.drawable.green_circle));
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        expensesBar.setProgress(expenseProgress);
        expensesBar.setProgress(expenseCategory.getExpensesProgress());
        expensesBar.setEnabled(false);

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context,ViewExpenseCategory.class);
                i.putExtra("id", expenseCategory.getId());
                context.startActivity(i);
            }
        });

        imageView.setImageResource(expenseCategory.getDrawable());

        return rowView;
    }

    public Drawable getThumb(int progress, int circle) {
        View thumbView = LayoutInflater.from(context).inflate(R.layout.layout_seekbar_thumb, null, false);
        thumbView.setBackgroundResource(circle);

        ((TextView) thumbView.findViewById(R.id.tvProgress)).setText(progress + "%");

        thumbView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        Bitmap bitmap = Bitmap.createBitmap(thumbView.getMeasuredWidth(), thumbView.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        thumbView.layout(0, 0, thumbView.getMeasuredWidth(), thumbView.getMeasuredHeight());
        thumbView.draw(canvas);

        return new BitmapDrawable(context.getResources(), bitmap);
    }
}
