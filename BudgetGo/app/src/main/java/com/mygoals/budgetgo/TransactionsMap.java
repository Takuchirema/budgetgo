package com.mygoals.budgetgo;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Takunda Chirema  on 2017-11-07.
 */

public class TransactionsMap<K,V> extends CustomMap<K,V> {

    private HashMap<String,ArrayList<Transaction>> transactionsByMonth = new HashMap<>();
    private HashMap<String,Float> transactionValuesByMonth = new HashMap<>();

    @Override
    public void BeforePut(Object v){
        Transaction transaction = (Transaction)v;
        String month = transaction.getMonth();

        if (containsKey(transaction.getDate())){
            return;
        }

        if (transactionsByMonth.containsKey(month)){
            transactionsByMonth.get(month).add(transaction);

            float monthValue = transactionValuesByMonth.get(month);
            monthValue = monthValue + transaction.getValue();

            transactionValuesByMonth.put(month,monthValue);
            //System.out.println("transactions value for month: mval "+monthValue+" tval: "+transaction.getValue()+" month: "+month);
        }else{
            ArrayList<Transaction> transactions = new ArrayList<>();
            transactions.add(transaction);
            transactionsByMonth.put(month,transactions);

            transactionValuesByMonth.put(month,transaction.getValue());
            //System.out.println("transactions value for month: tval "+transaction.getValue()+" "+month);
        }
    }

    @Override
    public void clear(){
        super.clear();
        transactionsByMonth.clear();
        transactionValuesByMonth.clear();
    }

    public float getValueForMonth(String month){
        //System.out.println("transactions value for month: "+month+" ");
        float value = 0;
        ArrayList<Transaction> transactions = transactionsByMonth.get(month);
        float monthValue =0;

        if (transactionValuesByMonth.containsKey(month)) {
            monthValue = transactionValuesByMonth.get(month);
        }

        if (transactions == null || transactions.isEmpty()){
            return value;
        }

        //System.out.println("transactions value for month: "+transactions.size());

        if (monthValue != 0){
            //return monthValue;
        }

        for (Transaction transaction:transactions){
            value = value + transaction.getValue();
        }

        transactionValuesByMonth.put(month,value);

        return value;
    }

    public ArrayList<Transaction> getTransactionsForMonth(String month){
        return transactionsByMonth.get(month);
    }
}
