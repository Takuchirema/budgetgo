package com.mygoals.budgetgo;

import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Color;
import android.icu.text.SimpleDateFormat;
import android.os.Build;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Takunda Chirema on 2017-08-21.
 */

public class Account implements Comparable<Account>{

    private String name;
    private String possibleAccountNames="";
    private String displayName="";
    private String shortName="";
    private int id;
    private int expenseCategoryId;
    private float expensesValue;
    private float accountValue;
    private EAccountType accountType;
    private String accountNumber;
    private ArrayList<Expense> expenses = new ArrayList<>();
    private TransactionsMap<Long,Transaction> transactions = new TransactionsMap<>();
    private ArrayList<String> names = new ArrayList<>();
    private String period;
    private String phoneNumber;
    private float budget;
    private float balanceAsAt;
    private Date balanceDate;

    private ExpenseCategory expenseCategory;
    public static int notificationPrefix = 1;
    public static String notificationType="ACCOUNT";

    // This is the month for which we want to process transactions. Default we process all.
    private String processMonth;

    public Account(int id){
        this.id=id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        name = name.replaceAll("^,","");
        this.name = name;
        setNames();
    }

    public void setNames(){
        names.clear();
        if (name != null){
            String[] namesArr = name.split("\\s*,[,\\s]*");
            for (String name: namesArr){
                names.add(name);
            }
        }
    }

    public ArrayList<String> getNames() {
        return names;
    }

    public String getStringNames() {
        String names = "";
        for (String name:this.names){
            names = names+","+name;
        }
        return names;
    }

    public String getPossibleAccountNames() {
        return possibleAccountNames;
    }

    public void setPossibleAccountNames(String possibleAccountNames) {
        this.possibleAccountNames = possibleAccountNames;
    }

    public void setNames(ArrayList<String> names) {
        this.names = names;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void calculateAccountValue() {
        if (!transactions.isEmpty()) {
            accountValue = 0;
            expensesValue = 0;
            //if current account then get balance of latest transaction
            if (Accounts.isCurrent(this)) {
                accountValue = ((Transaction)transactions.values().toArray()[0]).getBalanceAfterTransaction();
            } else if (!Accounts.isExpense(accountType)){

                if (balanceDate != null){
                    accountValue = balanceAsAt;
                }

                accountValue = getTransactionsValue(transactions,accountValue,balanceDate);
            }else{
                expensesValue = getTransactionsValue(transactions,expensesValue,null);
            }
        }
        if (accountType == EAccountType.SAVINGS){
            accountValue = Math.max(0,accountValue);
        }
    }

    /**
     * Assumes the values are already from the beginning
     * @return
     */
    public float calculateAverageValue(){
        if (Accounts.isCurrent(this)){
            return accountValue;
        }else{
            if (Accounts.isExpense(accountType)){
                return expensesValue/getPeriodInMonths();
            }
            return accountValue/getPeriodInMonths();
        }
    }

    //This class save the account to the db
    //Also updates the static Account map
    public void save(){
        Account newAccount = SQLite.storeAccount(this);
        Accounts.put(newAccount.getId(),newAccount);
    }

    public void delete(){
        boolean deleted = SQLite.deleteAccount(this.id);

        if (deleted){
            Accounts.remove(this.id);
        }
    }

    public ContentValues getContentValues(){
        ContentValues data = new ContentValues();

        data.put("Name",name);
        data.put("DisplayName",displayName);
        data.put("ExpensesValue",expensesValue);
        data.put("Value",accountValue);
        data.put("AccountType",accountType.toString());
        data.put("ExpenseCategoryId",expenseCategoryId);
        data.put("AccountNumber",accountNumber);
        data.put("Budget",budget);
        data.put("BalanceAsAt",balanceAsAt);

        if (balanceDate == null){
            data.put("BalanceDate", "");
        }else {
            //System.out.println("store balance date: "+balanceDate.getTime());
            data.put("BalanceDate", balanceDate.getTime());
        }

        return data;
    }

    private float getTransactionsValue(LinkedHashMap<Long,Transaction> transactions,float initialValue, Date balanceDate){
        float returnValue = initialValue;

        List<Long> keys = new ArrayList<>(transactions.keySet());
        for (Long key : keys) {

            Transaction transaction = transactions.get(key);

            if (balanceDate != null && transaction.getActualDate().before(balanceDate)){
                continue;
            }
            
            if (processMonth != null && !processMonth.isEmpty() && accountType != EAccountType.SAVINGS){
                if (transaction.getMonth().equalsIgnoreCase(processMonth)){
                    returnValue = returnValue + getValueSign(transaction) * transaction.getValue();
                }
            }else {
                returnValue = returnValue + getValueSign(transaction) * transaction.getValue();
            }
        }

        return returnValue;
    }

    public float getTransactionsValue(ArrayList<Transaction> transactions){
        float returnValue = 0;

        if (transactions == null){
            return 0;
        }

        for (Transaction transaction: transactions){
            returnValue = returnValue + getValueSign(transaction) * transaction.getValue();
        }

        return returnValue;
    }

    public int getValueSign(Transaction transaction){
        if (transaction.getCreditAccount()!=null && transaction.getCreditAccount().getId() == this.id){
            return -1;
        }else{
            return 1;
        }
    }

    public void reconcileTransactions(){
        if (!Accounts.isCurrent(this)){
            return;
        }

        TransactionsMap<Long,Transaction> newTransactions = new TransactionsMap<>();

        List<Long> keys = new ArrayList<>(transactions.keySet());
        //Collections.reverse(keys);
        for (int i=0;i<keys.size();i++){
            Transaction transaction = transactions.get(keys.get(i));
            newTransactions.put(transaction.getDate(),transaction);
            //If the next transaction balance is not the reconciling with this one then create a reconciliation one
            if (i != keys.size() - 1) {
                Transaction previousTransaction = transactions.get(keys.get(i+1));

                if (Math.abs(previousTransaction.getBalanceAfterTransaction() - transaction.getBalanceAfterTransaction()) < 5){
                    continue;
                }

                float currentBalance = previousTransaction.getBalanceAfterTransaction() +  getValueSign(transaction)*transaction.getValue();
                float difference = currentBalance - transaction.getBalanceAfterTransaction();
                if (Math.abs(difference) > 1){
                    System.out.println("reconcile: " + previousTransaction.getBalanceAfterTransaction()+" "+currentBalance+" "+transaction.getActualDate());

                    Calendar cal = Calendar.getInstance();
                    cal.setTime(transaction.getActualDate());

                    cal.set(Calendar.HOUR_OF_DAY,0);
                    cal.set(Calendar.MINUTE, transaction.getActualDate().getMinutes() + 1);

                    Transaction transactionRecon = new Transaction(cal.getTimeInMillis(),"Reconciliation Transaction");
                    transactionRecon.setDate(cal.getTimeInMillis());
                    transactionRecon.setActualDate(cal.getTime());
                    transactionRecon.setValue(difference);

                    if (difference > 0) {
                        transactionRecon.setCreditAccountId(id);
                        Account expenseAccount = Accounts.getUnknownExpenseAccount();

                        transactionRecon.setDebitAccountId(expenseAccount.getId());
                        expenseAccount.getTransactions().put(transactionRecon.getId(),transactionRecon);
                    }else{
                        transactionRecon.setDebitAccountId(id);
                    }

                    newTransactions.put(transactionRecon.getDate(),transactionRecon);
                }
            }
        }
        transactions = newTransactions;
    }

    /**
     * Get balance at end of a month.
     * For current accounts only
     * @param month
     * @return
     */
    public float getBalanceAt(String month){
        ArrayList<Transaction> monthTransactions = transactions.getTransactionsForMonth(month);

        if (monthTransactions == null){
            return 0;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            monthTransactions.sort(new Comparator<Transaction>() {
                @Override
                public int compare(Transaction transaction, Transaction t1) {
                    return transaction.compareTo(t1);
                }
            });
        }

        if (monthTransactions != null && !monthTransactions.isEmpty()){
            return monthTransactions.get(0).getBalanceAfterTransaction();
        }

        return 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getExpenseCategoryId() {
        return expenseCategoryId;
    }

    public void setExpenseCategoryId(int expenseCategoryId) {
        this.expenseCategoryId = expenseCategoryId;
        //Find the expense class and put the account in there
        for (Map.Entry<Integer,ExpenseCategory> e:ExpenseCategories.expenseCategoriesMap.entrySet()) {
            ExpenseCategory category = e.getValue();
            if (category.getId() == expenseCategoryId){
                this.expenseCategory = category;
                category.getAccounts().put(this.getId(),this);
            }
        }
    }

    public float getExpensesValue() {
        return expensesValue;
    }

    public void setExpensesValue(float expensesValue) {
        this.expensesValue = expensesValue;
    }

    public ArrayList<Expense> getExpenses() {
        return expenses;
    }

    public void setExpenses(ArrayList<Expense> expenses) {
        this.expenses = expenses;
    }

    public EAccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(EAccountType accountType) {
        this.accountType = accountType;
    }

    public String getPeriod() {
        DateTime dateTime = new DateTime(new Date());

        if (processMonth!=null && !processMonth.isEmpty()){
            period = fullMonthName(processMonth) + " "+dateTime.toString("yyyy");
        }else{
            if (!transactions.isEmpty()) {
                Transaction firstTransaction = getLastEntry();
                period = "since "+firstTransaction.getDay()+" "+firstTransaction.getMonth()+" "+firstTransaction.getYear();
            }else {
                period = dateTime.toString("MMMM") + " " + dateTime.toString("yyyy");
            }
        }
        return period;
    }

    public Transaction getLastEntry(){
        if (transactions.isEmpty()){
            return null;
        }
        return (Transaction) transactions.values().toArray()[transactions.size()-1];
    }

    /**
     * Gets the number of months from the earliest transaction of the Account.
     * @return
     */
    private int getPeriodInMonths(){
        Transaction firstTransaction = getLastEntry();
        if (firstTransaction == null){
            return 1;
        }
        Date beginningDate = firstTransaction.getActualDate();
        Date now = new Date();

        return monthsBetween(now,beginningDate);
    }

    public int monthsBetween(Date a, Date b) {
        Calendar cal = Calendar.getInstance();
        if (a.before(b)) {
            cal.setTime(a);
        } else {
            cal.setTime(b);
            b = a;
        }
        int c = 0;
        while (cal.getTime().before(b)) {
            cal.add(Calendar.MONTH, 1);
            c++;
        }
        return c - 1;
    }

    private String fullMonthName(String mmm){

        DateTime dateTime = new DateTime(new Date());
        String fullMonth = dateTime.toString("MMMM");

        if (mmm.equalsIgnoreCase("jan")){
            fullMonth = "January";
        }else if (mmm.equalsIgnoreCase("feb")){
            fullMonth = "February";
        }else if (mmm.equalsIgnoreCase("mar")){
            fullMonth = "March";
        }else if (mmm.equalsIgnoreCase("apr")){
            fullMonth = "April";
        }else if (mmm.equalsIgnoreCase("may")){
            fullMonth = "May";
        }else if (mmm.equalsIgnoreCase("jun")){
            fullMonth = "June";
        }else if (mmm.equalsIgnoreCase("jul")){
            fullMonth = "July";
        }else if (mmm.equalsIgnoreCase("aug")){
            fullMonth = "August";
        }else if (mmm.equalsIgnoreCase("sep")){
            fullMonth = "September";
        }else if (mmm.equalsIgnoreCase("oct")){
            fullMonth = "October";
        }else if (mmm.equalsIgnoreCase("nov")){
            fullMonth = "November";
        }else if (mmm.equalsIgnoreCase("dec")){
            fullMonth = "December";
        }
        return fullMonth;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getAccountNumber() {

        if (accountNumber == null){
            return "";
        }

        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public float getAccountValue() {
        if (Accounts.isIncome(accountType)){
            return Math.abs(accountValue);
        }
        return accountValue;
    }

    public void setAccountValue(float accountValue) {
        this.accountValue = accountValue;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public TransactionsMap<Long,Transaction> getTransactions() {
        return transactions;
    }

    public float getBalanceAsAt() {
        return balanceAsAt;
    }

    public void setBalanceAsAt(float balanceAsAt) {
        this.balanceAsAt = balanceAsAt;
    }

    public Date getBalanceDate() {
        return balanceDate;
    }

    public void setBalanceDate(Date balanceDate) {
        //System.out.println("save balance date: "+balanceDate.getTime());
        this.balanceDate = balanceDate;
    }

    public void setBalanceDate(Long balanceDate) {

        if (this.balanceDate != null || balanceDate == 0){
            return;
        }

        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(balanceDate);
            this.balanceDate = calendar.getTime();

        }catch (Exception ex){

        }
    }

    public String getMonth(){
        return new java.text.SimpleDateFormat("MMM").format(balanceDate);
    }

    public String getDay(){
        return new java.text.SimpleDateFormat("dd").format(balanceDate);
    }

    public String getYear(){
        return new java.text.SimpleDateFormat("yyyy").format(balanceDate);
    }

    public float getBudget() {
        return budget;
    }

    public void setBudget(float budget) {
        this.budget = budget;
    }

    public int getBudgetProgress() {
        float expensesSpentFraction = expensesValue / budget;

        int progress = (int)(expensesSpentFraction*1000);
        evaluateProgress(progress);
        return progress;
    }

    public void evaluateProgress(int progress){

        String currentMonth = new java.text.SimpleDateFormat("MMM").format(new Date());

        if (processMonth == null || processMonth.isEmpty()){
            return;
        }

        if (!processMonth.equals(currentMonth)){
            return;
        }

        if (!Accounts.isCurrent(this) && accountType != EAccountType.SAVINGS) {
            if (progress > 500 && progress <= 800) {
                //sendNotification("You have spent half of your budget for "+name);
            }else if (progress > 800 && progress < 1000){
                //sendNotification("Your budget for  "+name+" is almost up!");
            }else if (progress >= 1000){
                //sendNotification("Your have exhausted you budget for  "+name+"!");
            }
        }
    }

    public void sendNotification(String message){
        int notificationId = Integer.valueOf(String.valueOf(notificationPrefix) + String.valueOf(id));
        Intent i = new Intent(GoalApp.context,GoalApp.class);
        i.putExtra("menuFragment","Accounts");
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);


        Notifications notifications = new Notifications(GoalApp.context,notificationId,notificationType,i);
        notifications.setDrawableResource(Accounts.getAccountIcon(accountType));
        notifications.notify("Accounts",message);
    }

    public String getProcessMonth() {
        return processMonth;
    }

    public boolean hasProcessMonth(){

        if (processMonth == null){
            return false;
        }

        if (processMonth.isEmpty()){
            return false;
        }

        return true;
    }

    public void setProcessMonth(String processMonth) {
        this.processMonth = processMonth;
    }

    public int getColor(){
        int colorIndex = Math.abs(name.hashCode()%ColourCodes.colourCodes.length);
        return Color.parseColor(ColourCodes.colourCodes[colorIndex]);
    }

    public ExpenseCategory getExpenseCategory() {
        if (expenseCategory == null){
            if (ExpenseCategories.expenseCategoriesMap.containsKey(expenseCategoryId)) {
                expenseCategory = ExpenseCategories.expenseCategoriesMap.get(expenseCategoryId);
            }else{
                expenseCategory = ExpenseCategories.getDefaultCategory();
            }
        }
        return expenseCategory;
    }

    public void setExpenseCategory(ExpenseCategory expenseCategory) {
        this.expenseCategory = expenseCategory;
    }

    public boolean isEditable(){

        if (Accounts.isTotalIncomeAccount(this)){
            return false;
        }

        return true;
    }

    @Override
    public int compareTo(Account another) {
        float v1;
        float v2;

        boolean isv1Expense = Accounts.isExpense(accountType);
        boolean isv2Expense = Accounts.isExpense(another.getAccountType());

        if (!isv1Expense){
            v1=accountValue;
        }else{
            v1=expensesValue;
        }

        if (!isv2Expense){
            v2=another.getAccountValue();
        }else{
            v2=another.getExpensesValue();
        }

        if (!isv1Expense && isv2Expense){
            v1 = v2 + 1;
        }else if (isv1Expense && !isv2Expense){
            v2 = v1 + 1;
        }

        if (v2 > v1){
            return 1;
        }else if (v2 < v1){
            return -1;
        }else{
            return 0;
        }
    }
}
