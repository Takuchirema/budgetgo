package com.mygoals.budgetgo;

/**
 * Created by Takunda Chirema on 2017-08-21.
 */

public enum EAccountType {
    SHOP,
    RESTAURANT,
    AGENCY,
    HOSPITAL,
    SCHOOL,
    SAVINGS,
    CURRENT,
    CHEQUE,
    INCOME,
    CREDIT,
    EXPENSE,
    SALARY,
    OTHER
}
