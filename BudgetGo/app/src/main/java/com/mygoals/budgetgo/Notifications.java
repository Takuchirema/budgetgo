package com.mygoals.budgetgo;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.view.MenuItem;
import android.widget.ImageButton;

public class Notifications {
	
	private Context context;
	private Activity activity;
	private Intent notificationIntent;
	private int notificationCount = 1;
	private int notificationId;
	private String type;
	private int drawableResource;

	public static HashMap<String,ArrayList<Integer>> notificationIds = new HashMap<>();
	
	public Notifications(Context context,int notificationId,String type,Intent notificationIntent){
		this.context=context;
		this.notificationId = notificationId;
		this.notificationIntent = notificationIntent;
		this.type = type;
	}

	public void setActivity(Activity activity){
		this.activity=activity;
	}
	
	public void setIntent(Intent intent){
		this.notificationIntent=intent;
	}
	
	public int getNotificationCount() {
		return notificationCount;
	}


	public void notify(String notificationTitle, String notificationMessage){
	      NotificationManager notificationManager = (NotificationManager)context.getSystemService(context.NOTIFICATION_SERVICE);
	      
	      if (notificationIds.containsKey(type) && notificationIds.get(type).contains(notificationId)){
	    	  //System.out.println("notification there "+notificationId);
	    	  return;
	      }else if (!notificationIds.containsKey(type)){
			  ArrayList<Integer> ids = new ArrayList<>();
			  ids.add(notificationId);

	    	  //System.out.println("notification not there!! "+notificationId);
	    	  notificationIds.put(type,ids);
	      }else{
	      	notificationIds.get(type).add(notificationId);
		  }
	      
	      @SuppressWarnings("deprecation")
	      Notification notification = new Notification(getDrawableInt(),"New Message", System.currentTimeMillis());
	      notification.flags |= Notification.FLAG_AUTO_CANCEL;
	      
	      PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,notificationIntent,
	    		  PendingIntent.FLAG_UPDATE_CURRENT);
	      
	      NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
	      		  .setSmallIcon(getDrawableInt())
	              .setContentTitle(notificationTitle)
	              .setAutoCancel(true)
	              .setDefaults(Notification.DEFAULT_ALL)
	              .setStyle(new NotificationCompat.BigTextStyle().bigText(notificationMessage))
	              .setContentText(notificationMessage)
	              ;
	      mBuilder.setContentIntent(pendingIntent);
	      
	      mBuilder.setVibrate(new long[] { 1000, 1000});
	      Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
	      mBuilder.setSound(alarmSound);

		  mBuilder.setLights(Color.BLUE, 3000, 3000);
	      //System.out.println("notify!!! "+type+" id: "+notificationId);

	      notificationManager.notify(notificationId, mBuilder.build());
	}
	
	public int getDrawableInt(){
		if (drawableResource == 0) {
			return R.drawable.message;
		}
		return drawableResource;
	}

	public int getDrawableResource() {
		return drawableResource;
	}

	public void setDrawableResource(int drawableResource) {
		this.drawableResource = drawableResource;
	}

	public void remove(){
		cancelNotification(context,notificationId);
	}
	
	public void cancelNotification(Context ctx, int notifyId) {
	    String ns = Context.NOTIFICATION_SERVICE;
		try {
			NotificationManager nMgr = (NotificationManager) ctx.getSystemService(ns);
			nMgr.cancel(notifyId);
		}catch (Exception ex){}
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public Intent getNotificationIntent() {
		return notificationIntent;
	}

	public void setNotificationIntent(Intent notificationIntent) {
		this.notificationIntent = notificationIntent;
	}

	public String getType() {
		return type;
	}

	public Activity getActivity() {
		return activity;
	}

}
