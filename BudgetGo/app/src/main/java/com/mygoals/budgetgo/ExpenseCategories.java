package com.mygoals.budgetgo;

import android.graphics.Color;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Takunda Chirema  on 2017-10-21.
 */

public class ExpenseCategories {

    public static SyncLinkedHashMap<Integer, ExpenseCategory> expenseCategoriesMap = new SyncLinkedHashMap<>();
    public static ArrayList<StringWithTag> spinnerItems = new ArrayList<>();
    private static ExpenseCategory defaultCategory;

    public static void createCategories(){
        getDBExpenseCategories();

        if (expenseCategoriesMap.isEmpty()){
            createDefaultCategories();
        }
    }

    public static void createDefaultCategories(){
        ArrayList<ExpenseCategory> categories = new ArrayList<>();

        ExpenseCategory g1 = new ExpenseCategory(0,"Clothes");
        g1.createExpenses();
        g1.setAim(1000);
        g1.createValue();
        g1.setDrawable(R.drawable.clothes);

        ExpenseCategory g2 = new ExpenseCategory(0,"Food");
        g2.createExpenses();
        g2.setAim(5000);
        g2.createValue();
        g2.setDrawable(R.drawable.food);

        ExpenseCategory g3 = new ExpenseCategory(0,"Internet");
        g3.createExpenses();
        g3.setAim(1000);
        g3.createValue();
        g3.setDrawable(R.drawable.internet);

        ExpenseCategory g4 = new ExpenseCategory(0,"Transport");
        g4.createExpenses();
        g4.setAim(5000);
        g4.createValue();
        g4.setDrawable(R.drawable.transport);

        ExpenseCategory g5 = new ExpenseCategory(0,"Electricity");
        g5.createExpenses();
        g5.setAim(1000);
        g5.createValue();
        g5.setDrawable(R.drawable.electricity);

        ExpenseCategory g6 = new ExpenseCategory(0,"Rent");
        g6.createExpenses();
        g6.setAim(5000);
        g6.createValue();
        g6.setDrawable(R.drawable.agency);

        ExpenseCategory g7 = new ExpenseCategory(0,"School Fees");
        g7.createExpenses();
        g7.setAim(5000);
        g7.createValue();
        g7.setDrawable(R.drawable.school_fees);

        categories.add(g1);
        categories.add(g2);
        categories.add(g3);
        categories.add(g4);
        categories.add(g5);
        categories.add(g6);
        categories.add(g7);

        expenseCategoriesMap = SQLite.storeExpenseCategories(categories);
        //System.out.println("after store db: "+expenseCategoriesMap.size());
    }

    public static void getDBExpenseCategories(){
        expenseCategoriesMap = SQLite.getExpenseCategories();
        if (expenseCategoriesMap.isEmpty()){
            createDefaultCategories();
        }
    }

    /**
     * Returns a category called Other for uncategorized accounts
     * @return
     */
    public static ExpenseCategory getDefaultCategory(){
        if (defaultCategory != null){
            return defaultCategory;
        }

        if (expenseCategoriesMap.isEmpty()){
            getDBExpenseCategories();
        }

        for (Map.Entry<Integer,ExpenseCategory> e:expenseCategoriesMap.entrySet()){
            ExpenseCategory category = e.getValue();
            if (category.getName().equalsIgnoreCase("other")){
                defaultCategory = category;
                return category;
            }
        }

        ExpenseCategory g0 = new ExpenseCategory(0,"Other");
        g0.createExpenses();
        g0.setAim(5000);
        g0.createValue();
        g0.setDrawable(R.drawable.ibytes);

        ExpenseCategory newCategory = SQLite.storeExpenseCategory(g0);
        defaultCategory = newCategory;

        expenseCategoriesMap.put(newCategory.getId(),newCategory);

        return defaultCategory;
    }

    public static ExpenseCategory getCategoryByName(String name){
        if (expenseCategoriesMap.isEmpty()){
            getDBExpenseCategories();
        }

        for (Map.Entry<Integer,ExpenseCategory> e:expenseCategoriesMap.entrySet()){
            ExpenseCategory category = e.getValue();
            if (category.getName().equalsIgnoreCase(name)){
                return category;
            }
        }

        return ExpenseCategories.getDefaultCategory();
    }

    public static void storeDBExpenseCategory(ExpenseCategory category){
        ArrayList<ExpenseCategory> expenseCategories = new ArrayList<>();
        expenseCategories.add(category);
        SQLite.storeExpenseCategories(expenseCategories);
    }

    public static void storeDBExpenseCategories(ArrayList<ExpenseCategory> categories){
        SQLite.storeExpenseCategories(categories);
    }

    public static void remove(int categoryId){
        if (expenseCategoriesMap.containsKey(categoryId)){
            expenseCategoriesMap.remove(categoryId);
        }
    }

    public static ArrayList<StringWithTag> getSpinnerItems(){

        if (!spinnerItems.isEmpty()){
            return spinnerItems;
        }

        if (expenseCategoriesMap.isEmpty()){
            getDBExpenseCategories();
        }

        for (Map.Entry<Integer,ExpenseCategory> e:expenseCategoriesMap.entrySet()){
            ExpenseCategory category = e.getValue();

            spinnerItems.add(new StringWithTag(category.getName(),category.getId()));
        }

        return spinnerItems;
    }

    public static void refreshSpinnerItems(){
        spinnerItems.clear();

        for (Map.Entry<Integer,ExpenseCategory> e:expenseCategoriesMap.entrySet()){
            ExpenseCategory category = e.getValue();

            spinnerItems.add(new StringWithTag(category.getName(),category.getId()));
        }
    }
}
