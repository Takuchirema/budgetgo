package com.mygoals.budgetgo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Takunda Chirema on 2017-07-22.
 */

public class ViewAccounts extends Fragment implements IGetData{

    private static ArrayList<Integer> accountsList = new ArrayList<>();
    private static AccountsList adapter;
    private static ListView listView;

    private static Context context;

    private static View myFragmentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.activity_accounts, container, false);

        setRetainInstance(true);

        FloatingActionButton addAccount = (FloatingActionButton) myFragmentView.findViewById(R.id.addAccount);
        addAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addAccount();
            }
        });
        //GetAccountCategories getAccountCategories = new GetAccountCategories(this);
        //getAccountCategories.execute();

        createAccountCategories();
        refreshAccountBalances(null);
        return myFragmentView;
    }

    public static void addAccount(){
        Intent i = new Intent(context,CreateAccount.class);
        context.startActivity(i);
    }

    public void createAccountCategories(){
        accountsList.clear();

        if (Accounts.isEmpty()) {
            Accounts.createAccounts();
        }

        createListAdapter();
    }

    /**
     * This method takes all accounts in the map and calculates the balances
     * */
    public static void refreshAccountBalances(String processMonth){
        //System.out.println("refreshing account balances: for - "+processMonth+" "+Accounts.size());
        accountsList.clear();

        Iterator<Account> iterator = Accounts.getIterator();
        while(iterator.hasNext()){
            Account account = iterator.next();

            accountsList.add(account.getId());

            account.setProcessMonth(processMonth);
            account.calculateAccountValue();
            //System.out.println("Account: "+account.getName()+" balance: "+account.getAccountValue()+" trans: "+account.getTransactions().size());
        }

        prepareAdapter();
    }

    public static void filter(String searchString){

        accountsList.clear();

        Iterator<Account> iterator = Accounts.getIterator();
        while(iterator.hasNext()){
            Account account = iterator.next();

            if (searchString != null && !searchString.isEmpty()){
                String accountNames = (account.getName() + " "+ account.getDisplayName()).toLowerCase();
                if (accountNames.contains(searchString.toLowerCase())){
                    accountsList.add(account.getId());
                }
            }else{
                accountsList.add(account.getId());
            }
        }

        prepareAdapter();
    }

    /**
     * Creates the adapter to be put in the list view
     * */
    public void createListAdapter(){
        Account currentAccount = null;

        Iterator<Account> iterator = Accounts.getIterator();
        while(iterator.hasNext()){
            Account account = iterator.next();

            if (Accounts.isCurrent(account)){
                currentAccount = account;
            }
            accountsList.add(account.getId());
        }

        //If there is no current account ask user to set one
        if (currentAccount == null){
            Toast.makeText((Activity)context,"Please add a current Account before proceeding.", Toast.LENGTH_LONG).show();
            //addAccount();
        }

        listView = (ListView) myFragmentView.findViewById(R.id.accounts_list);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

            }
        });

        prepareAdapter();
    }

    private static void prepareAdapter(){
        adapter = new AccountsList((Activity)myFragmentView.getContext(), accountsList);

        adapter.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer lhs, Integer rhs) {
                return Accounts.get(lhs).compareTo(Accounts.get(rhs));
            }
        });

        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public static ArrayList<Integer> getAccountCategoryList() {
        return accountsList;
    }

    public static void setAccountCategoryList(ArrayList<Integer> accountCategoryList) {
        ViewAccounts.accountsList = accountCategoryList;
    }

    @Override
    public void postGetGoals(LinkedHashMap<String, Goal> goals) {

    }

    @Override
    public void postGetAccounts(HashMap<String, Account> account) {

    }

    @Override
    public void postGetShops(HashMap<String, Shop> shops) {

    }

    @Override
    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public void postGetExpenses(HashMap<String, Expense> expense) {

    }

    public static ArrayList<Integer> getAccountsList() {
        return accountsList;
    }

    public static void setAccountsList(ArrayList<Integer> accountsList) {
        ViewAccounts.accountsList = accountsList;
    }

    public static AccountsList getAdapter() {
        return adapter;
    }

    public static void setAdapter(AccountsList adapter) {
        ViewAccounts.adapter = adapter;
    }

    public static ListView getListView() {
        return listView;
    }

    public static void setListView(ListView listView) {
        ViewAccounts.listView = listView;
    }

    public static View getMyFragmentView() {
        return myFragmentView;
    }

    public static void setMyFragmentView(View myFragmentView) {
        ViewAccounts.myFragmentView = myFragmentView;
    }

}