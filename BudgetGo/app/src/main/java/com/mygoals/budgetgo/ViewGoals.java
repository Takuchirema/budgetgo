package com.mygoals.budgetgo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.woxthebox.draglistview.DragListView;
import com.woxthebox.draglistview.swipe.ListSwipeHelper;
import com.woxthebox.draglistview.swipe.ListSwipeItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Takunda Chirema on 2017-07-22.
 */

public class ViewGoals extends Fragment implements IGetData {
    
    private static ArrayList<Integer> goalList = new ArrayList<Integer>();
    private Context context;
    private static GoalList adapter;
    private static DragListView listView;
    private static View myFragmentView;
    private static HashMap<Integer,Float> savingsRunningBalance = new HashMap<>();

    private static boolean goalsCreated = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.activity_goals, container, false);

        FloatingActionButton addGoal = (FloatingActionButton) myFragmentView.findViewById(R.id.addGoal);
        addGoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addGoal();
            }
        });
        //GetGoals getGoals = new GetGoals(this);
        //getGoals.execute();

        createGoals();

        setRetainInstance(true);

        return myFragmentView;
    }

    public void addGoal(){
        Intent i = new Intent(myFragmentView.getContext(),CreateGoal.class);
        startActivity(i);
    }

    public static void refreshGoalsProgress(){
        savingsRunningBalance.clear();

        int priority = 1;

        for (Integer goalId:goalList){

            Goal goal = Goals.goalsMap.get(goalId);
            Account savingsAccount = Accounts.get(goal.getSavingsAccountId());
            float savings;

            if (savingsRunningBalance.containsKey(savingsAccount.getId())){
                savings = savingsRunningBalance.get(savingsAccount.getId());
            }else{
                savings = savingsAccount.getAccountValue();
                savingsRunningBalance.put(savingsAccount.getId(),savings);
            }

            goal.setRelativePriority(priority);
            priority = priority + 1;

            if (goal.isComplete()){
                continue;
            }

            goal.setValue(Math.min(savings,goal.getRequiredValue()));
            savings = Math.max(0,savings - goal.getRequiredValue());

            savingsRunningBalance.put(savingsAccount.getId(),savings);
        }

        //save goals
        Goals.save();

        adapter = new GoalList(R.layout.goal_listview,R.id.goalContainer,true,
                (Activity)myFragmentView.getContext(), goalList,Goals.goalsMap);
        listView.setAdapter(adapter,true);
        adapter.notifyDataSetChanged();
    }

    public static void reloadGoalsProgress(){
        int priority = 1;

        savingsRunningBalance.clear();
        goalList.clear();

        for (Map.Entry<Integer,Goal> e:Goals.goalsMap.entrySet()){
            Goal goal = e.getValue();
            Account savingsAccount = Accounts.get(goal.getSavingsAccountId());
            float savings;

            goalList.add(goal.getId());

            goal.setRelativePriority(priority);
            priority = priority + 1;

            if (goal.isComplete()){
                continue;
            }

            if (savingsRunningBalance.containsKey(savingsAccount.getId())){
                savings = savingsRunningBalance.get(savingsAccount.getId());
            }else{
                savings = savingsAccount.getAccountValue();
                savingsRunningBalance.put(savingsAccount.getId(),savings);
            }

            goal.setValue(Math.min(savings,goal.getRequiredValue()));
            savings = Math.max(0,savings - goal.getRequiredValue());
            //System.out.println("Goal: "+goal.getDescription()+" savings: "+savings);

            savingsRunningBalance.put(savingsAccount.getId(),savings);
        }

        //save goals
        Goals.save();

        adapter = new GoalList(R.layout.goal_listview,R.id.goalContainer,true,
                (Activity)myFragmentView.getContext(), goalList,Goals.goalsMap);
        listView.setAdapter(adapter,true);
        adapter.notifyDataSetChanged();
    }

    public static void filter(String searchString){

        goalList.clear();

        for (Map.Entry<Integer,Goal> e:Goals.goalsMap.entrySet()){
            Goal goal = e.getValue();

            if (searchString != null && !searchString.isEmpty()){
                if (goal.getDescription().toLowerCase().contains(searchString.toLowerCase())){
                    goalList.add(goal.getId());
                }
            }else{
                goalList.add(goal.getId());
            }
        }

        adapter = new GoalList(R.layout.goal_listview,R.id.goalContainer,true,
                (Activity)myFragmentView.getContext(), goalList,Goals.goalsMap);
        listView.setAdapter(adapter,true);
        adapter.notifyDataSetChanged();
    }

    public void createGoals(){
        goalList.clear();

        Goals.getDBGoals();

        createListAdapter();
    }

    /**
     * Creates the adapter to be put in the list view
     * */
    public void createListAdapter(){

        for (Map.Entry<Integer,Goal> e: Goals.goalsMap.entrySet()){
            Goal goal = e.getValue();
            goalList.add(goal.getId());
        }

        listView = (DragListView) myFragmentView.findViewById(R.id.goals_list);
        listView.getRecyclerView().setVerticalScrollBarEnabled(true);
        listView.setDragListListener(new DragListView.DragListListenerAdapter() {
            @Override
            public void onItemDragStarted(int position) {

            }

            @Override
            public void onItemDragEnded(int fromPosition, int toPosition) {
                if (fromPosition != toPosition) {
                    refreshGoalsProgress();
                }
            }
        });

        /*listView.setSwipeListener(new ListSwipeHelper.OnSwipeListenerAdapter() {
            @Override
            public void onItemSwipeStarted(ListSwipeItem item)
            {
            }

            @Override
            public void onItemSwipeEnded(ListSwipeItem item, ListSwipeItem.SwipeDirection swipedDirection) {

                // Swipe to delete on left
                if (swipedDirection == ListSwipeItem.SwipeDirection.LEFT) {
                    Pair<Long, String> adapterItem = (Pair<Long, String>) item.getTag();
                    int pos = listView.getAdapter().getPositionForItem(adapterItem);
                    listView.getAdapter().removeItem(pos);
                }
            }
        });*/

        listView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new GoalList(R.layout.goal_listview,R.id.goalContainer,true,
                (Activity)myFragmentView.getContext(), goalList,Goals.goalsMap);
        listView.setAdapter(adapter,true);
        listView.setCanDragHorizontally(false);
        listView.setCustomDragItem(null);
    }

    @Override
    public void postGetGoals(LinkedHashMap<String, Goal> goals) {

    }

    @Override
    public void postGetExpenses(HashMap<String, Expense> expense) {

    }

    @Override
    public void postGetShops(HashMap<String, Shop> shops) {

    }

    @Override
    public void postGetAccounts(HashMap<String, Account> accounts) {

    }

    @Override
    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public static ArrayList<Integer> getGoalList() {
        return goalList;
    }

    public void setGoalList(ArrayList<Integer> goalList) {
        this.goalList = goalList;
    }

    public static Goal getGoal(int id){
        return Goals.goalsMap.get(id);
    }
}
