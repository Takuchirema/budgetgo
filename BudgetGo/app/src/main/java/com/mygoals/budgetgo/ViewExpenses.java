package com.mygoals.budgetgo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Takunda Chirema on 2017-07-22.
 */

public class ViewExpenses extends Fragment implements IGetData{

    private static ArrayList<Integer> expenseCategoryList = new ArrayList<Integer>();
    private static ExpenseCategoryList adapter;
    private static ListView listView;

    private Context context;

    private static View myFragmentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.activity_expense_categories, container, false);

        FloatingActionButton addExpenseCategory = (FloatingActionButton) myFragmentView.findViewById(R.id.addExpenseCategory);
        addExpenseCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addExpenseCategory();
            }
        });
        //GetExpenseCategories getExpenseCategories = new GetExpenseCategories(this);
        //getExpenseCategories.execute();

        createExpenseCategories();

        setRetainInstance(true);
        refreshExpenseBalances(null);

        return myFragmentView;
    }

    public void addExpenseCategory(){
        Intent i = new Intent(context,CreateExpenseCategory.class);
        startActivity(i);
    }

    public void createExpenseCategories(){
        expenseCategoryList.clear();

        if (ExpenseCategories.expenseCategoriesMap.isEmpty()) {
            ExpenseCategories.createCategories();
        }

        createListAdapter();
    }

    /**
     * This method takes all accounts in the map and adds up to make expense
     * */
    public static void refreshExpenseBalances(String processMonth){
        //System.out.println("refreshing expense balances");
        expenseCategoryList.clear();

        for (Map.Entry<Integer,ExpenseCategory> e:ExpenseCategories.expenseCategoriesMap.entrySet()){
            ExpenseCategory expenseCategory = e.getValue();

            expenseCategoryList.add(expenseCategory.getId());

            expenseCategory.setProcessMonth(processMonth);
            expenseCategory.calculateExpensesValue();
            //System.out.println("Account: "+expenseCategory.getName()+" expense: "+expenseCategory.getValue());
        }

        prepareAdapter();
    }

    public static void filter(String searchString){

        expenseCategoryList.clear();

        for (Map.Entry<Integer,ExpenseCategory> e:ExpenseCategories.expenseCategoriesMap.entrySet()){
            ExpenseCategory category = e.getValue();

            if (searchString != null && !searchString.isEmpty()){
                if (category.getName().toLowerCase().contains(searchString.toLowerCase())){
                    expenseCategoryList.add(category.getId());
                }
            }else{
                expenseCategoryList.add(category.getId());
            }
        }

        prepareAdapter();
    }

    /**
     * Creates the adapter to be put in the list view
     * */
    public void createListAdapter(){

        for (Map.Entry<Integer,ExpenseCategory> e: ExpenseCategories.expenseCategoriesMap.entrySet()){
            ExpenseCategory expenseCategory = e.getValue();

            if (expenseCategory.getValue() == 0){
                expenseCategoryList.add(expenseCategory.getId());
                continue;
            }
            expenseCategoryList.add(0,expenseCategory.getId());
        }

        listView = (ListView) myFragmentView.findViewById(R.id.expense_categories_list);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

            }
        });

        prepareAdapter();
    }

    private static void prepareAdapter(){
        adapter = new ExpenseCategoryList((Activity)myFragmentView.getContext(), expenseCategoryList,ExpenseCategories.expenseCategoriesMap);

        adapter.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer lhs, Integer rhs) {
                float v1 = ExpenseCategories.expenseCategoriesMap.get(rhs).getValue();
                float v2 = ExpenseCategories.expenseCategoriesMap.get(lhs).getValue();
                if (v1 > v2)
                {
                    return 1;
                }else{
                    return -1;
                }
             }
        });

        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public static ArrayList<Integer> getExpenseCategoryList() {
        return expenseCategoryList;
    }

    public static void setExpenseCategoryList(ArrayList<Integer> expenseCategoryList) {
        ViewExpenses.expenseCategoryList = expenseCategoryList;
    }

    @Override
    public void postGetGoals(LinkedHashMap<String, Goal> goals) {

    }

    @Override
    public void postGetExpenses(HashMap<String, Expense> expense) {

    }

    @Override
    public void postGetShops(HashMap<String, Shop> shops) {

    }

    @Override
    public void postGetAccounts(HashMap<String, Account> accounts) {

    }

    public static LinkedHashMap<Integer, ExpenseCategory> getExpenseCategoriesMap() {
        return ExpenseCategories.expenseCategoriesMap;
    }

    @Override
    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public static void addExpense(ExpenseCategory expenseCategory){

        if (!ExpenseCategories.expenseCategoriesMap.containsKey(expenseCategory.getId())) {
            expenseCategoryList.add(expenseCategory.getId());
        }

        ExpenseCategories.expenseCategoriesMap.put(expenseCategory.getId(),expenseCategory);

        adapter = new ExpenseCategoryList((Activity)myFragmentView.getContext(), expenseCategoryList,ExpenseCategories.expenseCategoriesMap);
        listView.setAdapter(adapter);

        adapter.notifyDataSetChanged();
    }

    public static ExpenseCategory getExpenseCategory(int id){
        return ExpenseCategories.expenseCategoriesMap.get(id);
    }
}