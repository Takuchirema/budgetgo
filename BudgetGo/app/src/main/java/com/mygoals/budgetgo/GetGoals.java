package com.mygoals.budgetgo;

import android.os.AsyncTask;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Takunda Chirema on 2017-07-22.
 */

public class GetGoals extends AsyncTask<Void, Void, LinkedHashMap<String,Goal>> {

    private String url = "http://swapi.co/api/goals/";

    private IGetData gettingData;

    private LinkedHashMap<String,Goal> goalInformation = new LinkedHashMap<String,Goal>();

    private JSONParser allGoals = new JSONParser();

    private JSONArray goals = null;

    public GetGoals(IGetData gettingData){
        this.gettingData=gettingData;
    }

    @Override
    protected LinkedHashMap<String,Goal> doInBackground(Void... arg0) {

        try{
            List<NameValuePair> allGoalsParams = new ArrayList<NameValuePair>();

            JSONObject allGoalsJson = allGoals.makeHttpRequest(url, "GET",allGoalsParams);

            if (allGoalsJson == null){
                return null;
            }

            goals = allGoalsJson.getJSONArray("results");
            //System.out.println("size: "+goals.length());

            // looping through All FIlms
            for (int i = 0; i < goals.length(); i++) {
                JSONObject c = goals.getJSONObject(i);
                //System.out.println("goal: " + c.getString("title"));
                Goal goal = new Goal(1);

                goalInformation.put(c.getString("title"),goal);

            }

        }catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return goalInformation;
    }


    protected void onPostExecute(LinkedHashMap<String,Goal> goals) {

        if (goals == null){
            //System.out.println("goals are null");
        }else{
            gettingData.postGetGoals(goals);
        }
    }

}
