package com.mygoals.budgetgo;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import im.dacer.androidcharts.LineView;
import im.dacer.androidcharts.PieHelper;
import im.dacer.androidcharts.PieView;

public class IncomeExpenditure extends Fragment {

    private ArrayList<String> months = new ArrayList<>();
    private ArrayList<ArrayList<Integer>> dataset = new ArrayList<>();
    private ArrayList<PieHelper> pieDataSet = new ArrayList<>();
    private Account incomeAccount;
    private ArrayList<Account> savingsAccounts = new ArrayList<>();
    private static View myFragmentView;

    private TextView currentMonthTV;
    private String currentMonth;
    private String previousMonth;
    private float totalIncome;
    private float totalExpenditure;
    private float totalSavings;
    private float carriedFoward;

    private LinearLayout incomeRows;
    private LinearLayout expenditureRows;
    private LinearLayout savingsRows;
    private ImageView leftBtn;
    private ImageView rightBtn;
    private LayoutInflater inflater;

    public static final ArrayList<String> staticMonths = new ArrayList<String>()
    {{add("Jan");add("Feb");add("Mar");add("Apr");add("May");add("Jun");add("Jul");add("Aug");add("Sep");add("Oct");add("Nov");add("Dec");}};
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.income_expenditure, container, false);
        super.onCreate(savedInstanceState);

        this.inflater = inflater;

        currentMonthTV = (TextView) myFragmentView.findViewById(R.id.monthTxt);
        rightBtn = (ImageView)myFragmentView.findViewById(R.id.rightBtn);
        leftBtn = (ImageView)myFragmentView.findViewById(R.id.leftBtn);

        incomeRows = (LinearLayout) myFragmentView.findViewById(R.id.incomeLayout);
        expenditureRows = (LinearLayout) myFragmentView.findViewById(R.id.expenditureLayout);
        savingsRows = (LinearLayout) myFragmentView.findViewById(R.id.savingsLayout);

        setListeners();
        prepareData();
        
        return myFragmentView;
    }

    private void setListeners(){
        rightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int index = staticMonths.indexOf(currentMonth);
                index ++;

                if (index > staticMonths.size() - 1) {
                    currentMonth = staticMonths.get(0);
                }else{
                    currentMonth = staticMonths.get(index);
                }

                prepareData();
            }
        });

        leftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int index = staticMonths.indexOf(currentMonth);
                index --;

                if (index < 0) {
                    currentMonth = staticMonths.get(staticMonths.size() - 1);
                }else{
                    currentMonth = staticMonths.get(index);
                }

                prepareData();
            }
        });
    }

    public void createCharts(){
        createLineChart();
        createPieChart();
    }

    public void prepareData(){
        totalIncome = 0;
        totalExpenditure = 0;
        totalSavings = 0;
        carriedFoward = 0;

        incomeRows.removeAllViews();
        expenditureRows.removeAllViews();
        savingsRows.removeAllViews();

        String processMonth = GoalApp.processMonth;
        if (currentMonth == null){
            if (processMonth == null || processMonth.isEmpty()){
                currentMonth = new SimpleDateFormat("MMM").format(new Date());
            }else{
                currentMonth = processMonth;
            }
        }

        setPreviousMonth();

        currentMonthTV.setText(currentMonth);

        populateIncomeGrid();
        populateExpenditureGrid();
        populateSavingsGrid();
    }

    public void setPreviousMonth(){
        int index = staticMonths.indexOf(currentMonth);
        index --;

        if (index < 0) {
            previousMonth = staticMonths.get(staticMonths.size() - 1);
        }else{
            previousMonth = staticMonths.get(index);
        }
    }

    public void populateIncomeGrid(){
        incomeAccount = Accounts.getTotalIncomeAccount();

        carriedFoward = 0;

        ArrayList<Transaction> incomeTransactions = incomeAccount.getTransactions().getTransactionsForMonth(currentMonth);

        addRow("Account","Income","Proposed","#26A69A", "#FFFFFF",incomeRows);

        Iterator<Account> iterator = Accounts.getIterator();
        while(iterator.hasNext()){
            Account account = iterator.next();

            if (Accounts.isCurrent(account)){
                carriedFoward += account.getBalanceAt(previousMonth);
            }
        }

        totalIncome += carriedFoward;

        if (totalIncome <= 0){
            return;
        }

        addRow("Balance C/F",""+carriedFoward,"----",null,null,incomeRows);

        if (incomeTransactions != null) {
            for (Transaction transaction : incomeTransactions) {
                //System.out.println("income trans: " + transaction.getValue());

                totalIncome = totalIncome + transaction.getValue();

                //System.out.println("income total: " + totalIncome + " value: " + transaction.getValue());

                if (transaction.getCreditAccountId() != incomeAccount.getId()) {
                    //System.out.println("credit acc: " + transaction.getCreditAccountId());

                    if (transaction.getCreditAccountId() != 0) {
                        Account creditAccount = Accounts.get(transaction.getCreditAccountId());
                        addRow(creditAccount.getDisplayName(), "" + transaction.getValue(), "----", null, null,incomeRows);
                    }
                } else if (transaction.getDebitAccountId() != 0) {
                    //System.out.println("debit acc: " + transaction.getDebitAccountId());
                    Account debitAccount = Accounts.get(transaction.getDebitAccountId());
                    addRow(debitAccount.getDisplayName(), "" + transaction.getValue(), "----", null, null,incomeRows);
                }
            }
        }

        View row = addRow("Total",""+totalIncome,""+incomeAccount.getBudget(),"#EE7600", null,incomeRows);

        if (incomeAccount.getBudget() > 0 && totalIncome < incomeAccount.getBudget()){
            row.findViewById(R.id.text2).setBackgroundColor(Color.parseColor("#7F0000"));
            ((TextView)row.findViewById(R.id.text2)).setTextColor(Color.parseColor("#FFFFFF"));
        }
    }

    public View addRow(String text0, String text1, String text2, String color, String textColor,LinearLayout rowsLayout){

        View accountCell = inflater.inflate(R.layout.grid_row, null, true);

        TextView accountCellText = (TextView)accountCell.findViewById(R.id.text0);
        TextView currentCellText = (TextView)accountCell.findViewById(R.id.text1);
        TextView proposedCellText = (TextView)accountCell.findViewById(R.id.text2);

        if (color != null) {
            accountCellText.setBackgroundColor(Color.parseColor(color));
            currentCellText.setBackgroundColor(Color.parseColor(color));
            proposedCellText.setBackgroundColor(Color.parseColor(color));
        }

        if (textColor != null) {
            accountCellText.setTextColor(Color.parseColor(textColor));
            currentCellText.setTextColor(Color.parseColor(textColor));
            proposedCellText.setTextColor(Color.parseColor(textColor));
        }

        accountCellText.setText(text0);
        currentCellText.setText(text1);
        proposedCellText.setText(text2);

        rowsLayout.addView(accountCell);

        return accountCell;
    }

    public void populateExpenditureGrid(){

        float totalBudget = 0;

        addRow("Account","Expense","Proposed","#26A69A","#FFFFFF",expenditureRows);

        ArrayList<ExpenseCategory> categories= new ArrayList<>(ExpenseCategories.expenseCategoriesMap.values());

        Collections.sort(categories, new Comparator<ExpenseCategory>(){
            @Override
            public int compare(ExpenseCategory c1, ExpenseCategory c2) {
                return c1.compareTo(c2);
            }
        });

        for (ExpenseCategory category:categories){

            AccountsMap<Integer,Account> accounts = category.getAccounts();
            float value = accounts.getMonthValue(currentMonth);

            if (value <= 0){
                continue;
            }

            View row = addRow(category.getName(),""+value,""+category.getAim(),null,null,expenditureRows);

            if (category.getAim() > 0 && value > category.getAim()){
                row.findViewById(R.id.text2).setBackgroundColor(Color.parseColor("#7F0000"));
                ((TextView)row.findViewById(R.id.text2)).setTextColor(Color.parseColor("#FFFFFF"));
            }

            totalExpenditure = totalExpenditure + value;
            totalBudget = totalBudget + category.getAim();
            //System.out.println("expenses total: "+totalExpenditure+" value: "+value);
        }

        View row = addRow("Expenses",""+totalExpenditure,""+totalBudget,"#EE7600",null,expenditureRows);

        if (totalBudget > 0 && totalExpenditure > totalBudget){
            row.findViewById(R.id.text2).setBackgroundColor(Color.parseColor("#7F0000"));
            ((TextView)row.findViewById(R.id.text2)).setTextColor(Color.parseColor("#FFFFFF"));
        }

        /*
        addRow("Income Remaining",""+(totalIncome - totalExpenditure - carriedFoward),"----","#f39f4c",null,expenditureRows);
        addRow("Income Remaining",""+(totalIncome - totalExpenditure - carriedFoward),"----","#f39f4c",null,expenditureRows);
        */
    }

    public void populateSavingsGrid(){

        float totalBudget = 0;

        addRow("Account","Value","Proposed","#26A69A","#FFFFFF",savingsRows);

        ArrayList<Account> accounts= new ArrayList<>();

        Iterator<Account> iterator = Accounts.getIterator();
        while(iterator.hasNext()){
            Account account = iterator.next();

            if (account.getAccountType() != EAccountType.SAVINGS){
                continue;
            }

            float value = 0;
            ArrayList<Transaction> transactions = account.getTransactions().getTransactionsForMonth(currentMonth);

            if (transactions == null){
                continue;
            }
             
            for (Transaction transaction: transactions){
                value = value + account.getValueSign(transaction) * transaction.getValue();
            }

            View row = addRow(account.getName(),""+value,""+account.getBudget(),null,null,savingsRows);

            if (account.getBudget() > 0 && value < account.getBudget()){
                row.findViewById(R.id.text2).setBackgroundColor(Color.parseColor("#7F0000"));
                ((TextView)row.findViewById(R.id.text2)).setTextColor(Color.parseColor("#FFFFFF"));
            }

            totalSavings = totalSavings + value;
            totalBudget = totalBudget + account.getBudget();
            //System.out.println("expenses total: "+totalExpenditure+" value: "+value);

        }

        totalExpenditure = totalExpenditure + totalSavings;

        View row = addRow("Total",""+totalExpenditure,""+totalBudget,"#EE7600",null,savingsRows);

        if (totalBudget > 0 && totalExpenditure > totalBudget){
            row.findViewById(R.id.text2).setBackgroundColor(Color.parseColor("#7F0000"));
            ((TextView)row.findViewById(R.id.text2)).setTextColor(Color.parseColor("#FFFFFF"));
        }

        addRow("Balance",""+(totalIncome - totalExpenditure),"----","#f39f4c",null,savingsRows);
        addRow("Income Remaining",""+(totalIncome + totalSavings - totalExpenditure - carriedFoward),"----","#f39f4c",null,savingsRows);
    }



    public ArrayList<PieHelper> getPieData(){
        ArrayList<PieHelper> values = new ArrayList<>();

        LinkedHashMap<Integer,ExpenseCategory> categories = ExpenseCategories.expenseCategoriesMap;

        for (Map.Entry<Integer,ExpenseCategory> e:categories.entrySet()){
            ExpenseCategory category = e.getValue();
            float percent =  (category.getValue()/incomeAccount.getAccountValue())*100;
            PieHelper helper = new PieHelper(percent,Accounts.getAccountColor(category.getName()));
            //System.out.println("income pie data: "+percent+" "+category.getName()+" "+category.getValue());
            values.add(helper);
        }

        // Put in savings as well
        Iterator<Account> iterator = Accounts.getIterator();
        while(iterator.hasNext()){
            Account account = iterator.next();
            if (account.getAccountType() != EAccountType.SAVINGS){
                continue;
            }

            savingsAccounts.add(account);
            float percent =  (account.getAccountValue()/incomeAccount.getAccountValue())*100;
            PieHelper helper = new PieHelper(percent,Accounts.getAccountColor(account.getName()));
            //System.out.println("savings pie data: "+percent+" "+account.getName()+" "+account.getAccountValue());
            values.add(helper);
        }

        return values;
    }

    public ArrayList<Integer> getMonthValues(ArrayList<String> months){
        ArrayList<Integer> values = new ArrayList<>();
        TransactionsMap<Long,Transaction> transactions = incomeAccount.getTransactions();

        for (int i = 0;i<months.size();i++){
            String month = months.get(i);

            values.add(Math.round(transactions.getValueForMonth(month)));
        }

        return values;
    }

    public void createLineChart() {
        LineView lineView = (LineView)myFragmentView.findViewById(R.id.line_view);
        lineView.setDrawDotLine(true);
        lineView.setBackgroundColor(Color.WHITE);
        lineView.setShowPopup(LineView.SHOW_POPUPS_MAXMIN_ONLY);
        lineView.setBottomTextList(months);
        lineView.setColorArray(new int[]{Color.BLUE});
        lineView.setDataList(dataset);
    }

    public void createPieChart(){
        PieView pieView = (PieView)myFragmentView.findViewById(R.id.pie_view);
        pieView.setDate(pieDataSet);
        pieView.selectedPie(PieView.NO_SELECTED_INDEX);
        pieView.setOnPieClickListener(listener);
        pieView.showPercentLabel(true);

        //create the accounts info
        createPieInfo();
    }

    public void createPieInfo(){
        LayoutInflater inflater = getActivity().getLayoutInflater();

        for (Account account:savingsAccounts) {

            if (account.getAccountValue() <= 0){
                continue;
            }

            View pieInfoView = inflater.inflate(R.layout.pie_info, null, true);

            GradientDrawable background = (GradientDrawable) pieInfoView.findViewById(R.id.color).getBackground();
            background.setColor(account.getColor());

            TextView accountName = (TextView)pieInfoView.findViewById(R.id.accountName);
            accountName.setText(account.getName());

            TextView accountValue = (TextView)pieInfoView.findViewById(R.id.value);
            accountValue.setText("R"+account.getAccountValue());
            accountValue.setVisibility(View.VISIBLE);

            ((LinearLayout)myFragmentView.findViewById(R.id.pie_info_container)).addView(pieInfoView);
        }

        LinkedHashMap<Integer,ExpenseCategory> categories = ExpenseCategories.expenseCategoriesMap;
        for (Map.Entry<Integer,ExpenseCategory> e:categories.entrySet()){
            ExpenseCategory category = e.getValue();

            if (category.getValue() <= 0){
                continue;
            }

            View pieInfoView = inflater.inflate(R.layout.pie_info, null, true);

            GradientDrawable background = (GradientDrawable) pieInfoView.findViewById(R.id.color).getBackground();
            background.setColor(category.getColor());

            TextView accountName = (TextView)pieInfoView.findViewById(R.id.accountName);
            accountName.setText(category.getName());

            TextView accountValue = (TextView)pieInfoView.findViewById(R.id.value);
            accountValue.setText("R"+category.getValue());
            accountValue.setVisibility(View.VISIBLE);

            ((LinearLayout)myFragmentView.findViewById(R.id.pie_info_container)).addView(pieInfoView);
        }
    }

    PieView.OnPieClickListener listener = new PieView.OnPieClickListener() {
        @Override public void onPieClick(int index) {
            if (index != PieView.NO_SELECTED_INDEX) {
                GradientDrawable background = (GradientDrawable) myFragmentView.findViewById(R.id.color).getBackground();
                TextView accountName = (TextView) myFragmentView.findViewById(R.id.accountName);
                TextView accountValue = (TextView)myFragmentView.findViewById(R.id.value);

                ExpenseCategory category = getCategoryAt(index);

                if (category == null){
                    Account account = getAccountAt(index - ExpenseCategories.expenseCategoriesMap.size());
                    background.setColor(account.getColor());
                    accountName.setText(account.getName());

                    accountValue.setText("R"+account.getAccountValue());
                }else {
                    background.setColor(category.getColor());
                    accountName.setText(category.getName());

                    accountValue.setText("R"+category.getValue());
                }
            }
        }
    };

    public ExpenseCategory getCategoryAt(int index){
        List<ExpenseCategory> l = new ArrayList<>(ExpenseCategories.expenseCategoriesMap.values());
        if (index >= l.size()){
            return null;
        }
        return l.get(index);
    }

    public Account getAccountAt(int index){
        return savingsAccounts.get(index);
    }

    public ArrayList<String> getPastMonths(String startingMonth, int range){
        ArrayList<String> months = new ArrayList<>();

        // Get the position of starting month
        int index = staticMonths.indexOf(startingMonth);

        if (range >= 12){
            months = staticMonths;
        }else{

            for (int i = range - 1;i>=0;i--){
                int position = index - i;
                if (position < 0){
                    position = 12 - Math.abs(position);
                }

                months.add(staticMonths.get(position));
            }
        }

        return months;
    }
}
