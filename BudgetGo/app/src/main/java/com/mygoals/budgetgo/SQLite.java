package com.mygoals.budgetgo;

import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Takunda Chirema  on 2017-10-28.
 */

public class SQLite {

    public static SQLiteDatabase budgetDB;

    public static SQLiteDatabase openOrCreateDB(){
        /*File file = new File (GoalApp.context.getFilesDir().getPath()+"/Budget.db");
        budgetDB = SQLiteDatabase.openOrCreateDatabase(file.getPath(), null, new DatabaseErrorHandler() {
            @Override
            public void onCorruption(SQLiteDatabase dbObj) {
                //System.out.println("db: oncurruption");
            }
        });*/
        budgetDB = DatabaseHelper.getInstance(GoalApp.context).getReadableDatabase();
        return budgetDB;
    }

    public static void createTransactionsTable(){
        openOrCreateDB().execSQL("CREATE TABLE IF NOT EXISTS Transactions(" +
                "Date INT PRIMARY KEY," +
                "Message VARCHAR," +
                "Value VARCHAR," +
                "Balance VARCHAR," +
                "DebitAccountId INT," +
                "CreditAccountId INT," +
                "IsIncome VARCHAR" +
                ");");
    }


    public static SyncLinkedHashMap<Long,Transaction> storeTransactions(ArrayList<Transaction> transactions){
        SyncLinkedHashMap<Long,Transaction> newTransactions = new SyncLinkedHashMap<>();

        createTransactionsTable();

        for (Transaction transaction:transactions){
            Transaction newTransaction = storeTransaction(transaction);
            newTransactions.put(newTransaction.getDate(),newTransaction);
        }

        return newTransactions;
    }

    public static Transaction storeTransaction(Transaction transaction){

        createTransactionsTable();

        if (recordExists("Transactions","Date",""+transaction.getDate())){
            budgetDB.update("Transactions",transaction.getContentValues(),"Date = '"+transaction.getDate()+"'",null);

            Cursor resultSet = budgetDB.rawQuery("Select * from Transactions where Date='"+transaction.getDate()+"'",null);
            resultSet.moveToFirst();

            Transaction newTransaction =  getTransaction(resultSet);
            resultSet.close();

            return newTransaction;
        }

        //System.out.println("store transaction: dr "+transaction.getDebitAccountId()+" cr "+transaction.getCreditAccountId());
        String insertStatement = "INSERT INTO Transactions " +
                "(Date,Message,Value,Balance,DebitAccountId,CreditAccountId,IsIncome) "+
                " VALUES(" +
                "'"+transaction.getDate()+"'," +
                "'"+transaction.getTransactionMessage()+"'," +
                "'"+transaction.getValue()+"'," +
                "'"+transaction.getBalanceAfterTransaction()+"',"+
                "'"+transaction.getDebitAccountId()+"'," +
                "'"+transaction.getCreditAccountId()+"'," +
                "'"+transaction.isIncome()+"'" ;

        budgetDB.execSQL(insertStatement+ ");");

        Cursor resultSet = budgetDB.rawQuery("Select * from Transactions",null);
        resultSet.moveToLast();

        Transaction newTransaction =  getTransaction(resultSet);
        resultSet.close();

        return newTransaction;
    }

    public static boolean deleteTransaction(Long transactionId)
    {
        return budgetDB.delete("Transactions", "Date = '"+transactionId+"'", null) > 0;
    }

    public static SyncLinkedHashMap<Long,Transaction> getTransactions(){
        createTransactionsTable();

        SyncLinkedHashMap<Long,Transaction> transactions = new SyncLinkedHashMap<>();
        Cursor resultSet = budgetDB.rawQuery("Select * from Transactions",null);

        while (resultSet.moveToNext()) {
            Transaction transaction = getTransaction(resultSet);
            ////System.out.println("db: get transaction - "+transaction.getCreditAccountId()+" "+transaction.getDebitAccountId());
            transactions.put(transaction.getDate(),transaction);
        }

        resultSet.close();
        return transactions;
    }

    /**
     * Will get the transactions which happened after the specified date
     * @param from
     * @return
     */
    public static SyncLinkedHashMap<Long,Transaction> getTransactions(Date from){
        createTransactionsTable();

        Long fromLong = from.getTime();

        SyncLinkedHashMap<Long,Transaction> transactions = new SyncLinkedHashMap<>();
        Cursor resultSet = budgetDB.rawQuery("Select * from Transactions where Date > "+fromLong,null);

        while (resultSet.moveToNext()) {
            Transaction transaction = getTransaction(resultSet);
            ////System.out.println("db: get transaction - "+transaction.getCreditAccountId()+" "+transaction.getDebitAccountId());
            transactions.put(transaction.getDate(),transaction);
        }

        resultSet.close();
        return transactions;
    }

    public static Transaction getTransaction(Cursor resultSet){
        ////System.out.println("column index: "+resultSet.getColumnIndex("Date")+" count: "+resultSet.getCount());
        Long date = resultSet.getLong(resultSet.getColumnIndex("Date"));
        String message = resultSet.getString(resultSet.getColumnIndex("Message"));

        String valueStr = resultSet.getString(resultSet.getColumnIndex("Value"));
        float value = 0;
        try{
            value = Float.parseFloat(valueStr);
        }catch(Exception ex){

        }

        String balanceStr = resultSet.getString(resultSet.getColumnIndex("Balance"));
        float balance = 0;
        try{
            balance = Float.parseFloat(balanceStr);
        }catch(Exception ex){

        }

        int debitAccountId = resultSet.getInt(resultSet.getColumnIndex("DebitAccountId"));
        int creditAccountId = resultSet.getInt(resultSet.getColumnIndex("CreditAccountId"));

        String isIncomeStr = resultSet.getString(resultSet.getColumnIndex("IsIncome"));
        boolean isIncome = false;
        if (isIncomeStr.equalsIgnoreCase("1")){
            isIncome = true;
        }

        Transaction transaction = new Transaction(date);
        transaction.setTransactionMessage(message);
        transaction.setDate(date);
        transaction.setBalanceAfterTransaction(balance);
        transaction.setValue(value);
        transaction.setDebitAccountId(debitAccountId);
        transaction.setCreditAccountId(creditAccountId);
        transaction.setIncome(isIncome);
        //System.out.println("is income "+isIncome+" "+transaction.getTransactionMessage());
        return transaction;
    }

    public static void createGoalsTable(){
        openOrCreateDB().execSQL("CREATE TABLE IF NOT EXISTS Goals(" +
                "Id INTEGER PRIMARY KEY AUTOINCREMENT,"+
                "Name VARCHAR," +
                "Description VARCHAR," +
                "Aim VARCHAR," +
                "Value VARCHAR," +
                "Priority INT," +
                "GoalType VARCHAR," +
                "Complete VARCHAR,"+
                "StartDay VARCHAR,"+
                "EndDay VARCHAR,"+
                "SavingsAccountId INT,"+
                "Drawable INT"+
                ");");
    }

    public static SyncLinkedHashMap<Integer,Goal> storeGoals(ArrayList<Goal> goals){
        SyncLinkedHashMap<Integer,Goal> newGoals = new SyncLinkedHashMap<>();

        createGoalsTable();

        for (Goal goal:goals){
            Goal newGoal = storeGoal(goal);
            newGoals.put(newGoal.getId(),newGoal);
        }

        return newGoals;
    }

    public static Goal storeGoal(Goal goal){
        createGoalsTable();

        if (recordExists("Goals","Id",""+goal.getId())){
            //System.out.println("store get goal: "+goal.getId()+" "+goal.getDescription()+" priority: "+goal.getRelativePriority()+" "+goal.isComplete());
            budgetDB.update("Goals",goal.getContentValues(),"Id = '"+goal.getId()+"'",null);

            Cursor resultSet = budgetDB.rawQuery("Select * from Goals where Id='"+goal.getId()+"'",null);
            resultSet.moveToFirst();

            Goal newGoal =  getGoal(resultSet);
            resultSet.close();

            return newGoal;
        }

        String insertStatement = "INSERT INTO Goals (" +
                "Name,Description,Aim,Value,Priority,GoalType,Complete,StartDay,EndDay,SavingsAccountId,Drawable) "+
                " VALUES (" +
                "'"+goal.getName()+"'," +
                "'"+goal.getDescription()+"'," +
                "'"+goal.getAim()+"'," +
                "'"+goal.getValue()+"',"+
                "'"+goal.getRelativePriority()+"'," +
                "'"+goal.getGoalType()+"'," +
                "'"+goal.isComplete()+"'," +
                "'"+goal.getStartDay()+"',"+
                "'"+goal.getEndDay()+"',"+
                "'"+goal.getSavingsAccountId()+"',"+
                "'"+goal.getDrawable()+"'";

        budgetDB.execSQL(insertStatement+ ");");

        Cursor resultSet = budgetDB.rawQuery("Select * from Goals",null);
        resultSet.moveToLast();

        Goal newGoal =  getGoal(resultSet);
        resultSet.close();

        return newGoal;
    }

    public static boolean deleteGoal(int goalId)
    {
        //System.out.println("deleting "+goalId);
        return budgetDB.delete("Goals", "Id = '"+goalId+"'", null) > 0;
    }

    public static SyncLinkedHashMap<Integer,Goal> getGoals(){
        createGoalsTable();

        SyncLinkedHashMap<Integer,Goal> goals= new SyncLinkedHashMap<>();
        Cursor resultSet = budgetDB.rawQuery("Select * from Goals ORDER BY Priority DESC",null);

        while (resultSet.moveToNext()) {
            Goal goal = getGoal(resultSet);
            goals.put(goal.getId(),goal);
        }

        resultSet.close();
        return goals;
    }

    public static Goal getGoal(Cursor resultSet){
        int id = resultSet.getInt(resultSet.getColumnIndex("Id"));

        String name = resultSet.getString(resultSet.getColumnIndex("Name"));
        String description = resultSet.getString(resultSet.getColumnIndex("Description"));

        String aimStr = resultSet.getString(resultSet.getColumnIndex("Aim"));
        float aim = 0;
        try{
            aim = Float.parseFloat(aimStr);
        }catch (Exception ex){

        }

        String valueStr = resultSet.getString(resultSet.getColumnIndex("Value"));
        float value = 0;
        try{
            value = Float.parseFloat(valueStr);
        }catch (Exception ex){

        }

        int priority = resultSet.getInt(resultSet.getColumnIndex("Priority"));

        String goalType = resultSet.getString(resultSet.getColumnIndex("GoalType"));
        String isComplete = resultSet.getString(resultSet.getColumnIndex("Complete"));
        String startDay = resultSet.getString(resultSet.getColumnIndex("StartDay"));
        String endDay = resultSet.getString(resultSet.getColumnIndex("EndDay"));
        int savingsAccountId = resultSet.getInt(resultSet.getColumnIndex("SavingsAccountId"));
        int drawable = resultSet.getInt(resultSet.getColumnIndex("Drawable"));

        //System.out.println("Get Goal: "+description+" priority: "+priority+" complete: "+isComplete);

        Goal goal = new Goal(id);
        goal.setName(name);
        goal.setDescription(description);
        goal.setAim(aim);
        goal.setValue(value);
        goal.setRelativePriority(priority);
        goal.setGoalType(Goals.getGoalType(goalType));
        goal.setSavingsAccountId(savingsAccountId);
        goal.setDrawable(drawable);

        if (isComplete.equalsIgnoreCase("1")){
            goal.setComplete(true);
        }else{
            goal.setComplete(false);
        }

        goal.setStartDay(startDay);
        goal.setEndDay(endDay);

        return goal;
    }

    public static void createAccountsTable(){
        openOrCreateDB().execSQL("CREATE TABLE IF NOT EXISTS Accounts(" +
                "Id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "Name VARCHAR," +
                "PossibleAccountNames VARCHAR," +
                "DisplayName VARCHAR," +
                "Value VARCHAR," +
                "ExpensesValue VARCHAR," +
                "AccountType VARCHAR," +
                "ExpenseCategoryId INTEGER," +
                "AccountNumber VARCHAR," +
                "Budget VARCHAR," +
                "BalanceAsAt VARCHAR," +
                "BalanceDate INT" +
                ");");
    }

    public static Account storeAccount(Account account){

        createAccountsTable();

        if (recordExists("Accounts","Id",""+account.getId())){
            budgetDB.update("Accounts",account.getContentValues(),"Id = '"+account.getId()+"'",null);

            Cursor resultSet = budgetDB.rawQuery("Select * from Accounts where Id='"+account.getId()+"'",null);
            resultSet.moveToFirst();

            Account newAccount =  getAccount(resultSet);
            resultSet.close();

            return newAccount;
        }

        String insertStatement = "INSERT INTO Accounts (" +
                "Name,PossibleAccountNames,DisplayName,Value,ExpensesValue,AccountType,ExpenseCategoryId,AccountNumber,Budget,BalanceAsAt,BalanceDate) " +
                "VALUES(" +
                "'"+account.getName()+"'," +
                "'"+account.getPossibleAccountNames()+"'," +
                "'"+account.getDisplayName()+"'," +
                "'"+account.getAccountValue()+"'," +
                "'"+account.getExpensesValue()+"'," +
                "'"+account.getAccountType()+"'," +
                "'"+account.getExpenseCategoryId()+"'," +
                "'"+account.getAccountNumber()+"'," +
                "'"+account.getBudget()+"'," +
                "'"+account.getBalanceAsAt()+"'";

        if (account.getBalanceDate() != null){
            insertStatement = insertStatement + ",'"+account.getBalanceDate().getTime()+"'";
        }else{
            insertStatement = insertStatement + ",''";
        }

        budgetDB.execSQL(insertStatement+ ");");

        Cursor resultSet = budgetDB.rawQuery("Select * from Accounts",null);
        resultSet.moveToLast();

        Account newAccount =  getAccount(resultSet);
        resultSet.close();

        return newAccount;
    }

    public static boolean deleteAccount(int accountId)
    {
        return budgetDB.delete("Accounts", "Id = '"+accountId+"'", null) > 0;
    }

    public static SyncLinkedHashMap<Integer,Account> storeAccounts(ArrayList<Account> accounts){
        SyncLinkedHashMap<Integer,Account> newAccounts = new SyncLinkedHashMap<>();

        createAccountsTable();

        for (Account account:accounts){
            Account newAccount = storeAccount(account);
            newAccounts.put(newAccount.getId(),newAccount);
            //System.out.println("inserted: "+newAccount.getName()+" new id: "+newAccount.getId()+" num: "+newAccount.getAccountNumber());
        }

        return newAccounts;
    }

    public static void deleteRow(String table,String field,String fieldValue){
        budgetDB.delete(table,field+"=?",new String[]{fieldValue});
    }

    public static SyncLinkedHashMap<Integer,Account> getAccounts(){
        createAccountsTable();

        SyncLinkedHashMap<Integer,Account> accounts = new SyncLinkedHashMap<>();
        Cursor resultSet = budgetDB.rawQuery("Select * from Accounts",null);

        while (resultSet.moveToNext()) {
            Account account = getAccount(resultSet);
            accounts.put(account.getId(),account);
            //System.out.println("db: get account - "+account.getName()+" id: "+account.getId()+" ");
        }

        resultSet.close();
        return accounts;
    }

    public static SyncLinkedHashMap<Integer,Account> sortAccounts(SyncLinkedHashMap<Integer,Account> accounts){
        List<Account> accountsList = new ArrayList<>(accounts.values());
        Collections.sort(accountsList, new Comparator<Account>() {
            public int compare(Account a, Account b){
                return a.compareTo(b);
            }
        });
        accounts = new SyncLinkedHashMap<>();
        for (Account account:accountsList){
            accounts.put(account.getId(),account);
        }
        return accounts;
    }

    public static Account getAccount(Cursor resultSet){

        int id = resultSet.getInt(resultSet.getColumnIndex("Id"));
        String name = resultSet.getString(resultSet.getColumnIndex("Name"));
        String possibleAccountNames = resultSet.getString(resultSet.getColumnIndex("PossibleAccountNames"));
        String displayName = resultSet.getString(resultSet.getColumnIndex("DisplayName"));

        String valueStr = resultSet.getString(resultSet.getColumnIndex("Value"));
        float value = 0;
        try{
            value = Float.parseFloat(valueStr);
        }catch (Exception ex){

        }

        String expenseValueStr = resultSet.getString(resultSet.getColumnIndex("ExpensesValue"));
        float expenseValue = 0;
        try{
            expenseValue = Float.parseFloat(expenseValueStr);
        }catch(Exception ex){

        }

        String accountType = resultSet.getString(resultSet.getColumnIndex("AccountType"));
        int expenseCategoryId = resultSet.getInt(resultSet.getColumnIndex("ExpenseCategoryId"));
        String accountNumber = resultSet.getString(resultSet.getColumnIndex("AccountNumber"));

        String budgetStr = resultSet.getString(resultSet.getColumnIndex("Budget"));

        float budget = 0;
        try {
            budget = Float.parseFloat(budgetStr);
        }catch(Exception ex){
            //System.out.println("error get acc budget: "+ex.getMessage());
        }

        String balanceAsAtStr = resultSet.getString(resultSet.getColumnIndex("BalanceAsAt"));
        float balanceAsAt = 0;
        try {
            balanceAsAt = Float.parseFloat(balanceAsAtStr);
        }catch(Exception ex){

        }

        Long balanceDate = resultSet.getLong(resultSet.getColumnIndex("BalanceDate"));
        //System.out.println("get account: "+name);
        Account account = new Account(id);
        account.setName(name);
        account.setPossibleAccountNames(possibleAccountNames);
        account.setDisplayName(displayName);
        account.setAccountNumber(accountNumber);
        account.setAccountValue(value);
        account.setExpensesValue(expenseValue);
        account.setAccountType(Accounts.getAccountType(accountType));
        account.setExpenseCategoryId(expenseCategoryId);
        account.setBudget(budget);
        account.setBalanceAsAt(balanceAsAt);
        account.setBalanceDate(balanceDate);

        return account;
    }

    public static SyncLinkedHashMap<Integer,ExpenseCategory> storeExpenseCategories(ArrayList<ExpenseCategory> categories){
        SyncLinkedHashMap<Integer,ExpenseCategory> newCategories = new SyncLinkedHashMap<>();

        createExpenseCategoriesTable();

        for (ExpenseCategory category:categories){
            ExpenseCategory newCategory = storeExpenseCategory(category);
            newCategories.put(newCategory.getId(),newCategory);
        }

        return newCategories;
    }
    
    public static ExpenseCategory storeExpenseCategory(ExpenseCategory category){
        
        createExpenseCategoriesTable();

        if (recordExists("ExpenseCategories","Id",""+category.getId())){
            budgetDB.update("ExpenseCategories",category.getContentValues(),"Id = '"+category.getId()+"'",null);

            Cursor resultSet = budgetDB.rawQuery("Select * from ExpenseCategories where Id='"+category.getId()+"'",null);
            resultSet.moveToFirst();

            ExpenseCategory newCategory =  getExpenseCategory(resultSet);
            resultSet.close();

            return newCategory;
        }

        String insertStatement = "INSERT INTO ExpenseCategories " +
                "(Name,Aim,Drawable) "+
                "VALUES(" +
                "'"+category.getName()+"'," +
                "'"+category.getAim()+"'," +
                "'"+category.getDrawable()+"'";

        budgetDB.execSQL(insertStatement+ ");");

        Cursor resultSet = budgetDB.rawQuery("Select * from ExpenseCategories",null);
        resultSet.moveToLast();

        ExpenseCategory newCategory =  getExpenseCategory(resultSet);
        resultSet.close();

        return newCategory;
    }

    public static boolean deleteExpenseCategory(int categoryId)
    {
        return budgetDB.delete("ExpenseCategories", "Id = '"+categoryId+"'", null) > 0;
    }

    public static void createExpenseCategoriesTable(){
        openOrCreateDB().execSQL("CREATE TABLE IF NOT EXISTS ExpenseCategories(" +
                "Id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "Name VARCHAR," +
                "Aim VARCHAR," +
                "Drawable INTEGER" +
                ");");
    }

    public static SyncLinkedHashMap<Integer,ExpenseCategory> getExpenseCategories(){
        createExpenseCategoriesTable();

        SyncLinkedHashMap<Integer,ExpenseCategory> categories = new SyncLinkedHashMap<>();
        Cursor resultSet = budgetDB.rawQuery("Select * from ExpenseCategories",null);

        while (resultSet.moveToNext()) {
            ExpenseCategory category = getExpenseCategory(resultSet);
            categories.put(category.getId(),category);
        }

        resultSet.close();
        return categories;
    }

    public static ExpenseCategory getExpenseCategory(Cursor resultSet){
        int id = resultSet.getInt(resultSet.getColumnIndex("Id"));

        String name = resultSet.getString(resultSet.getColumnIndex("Name"));

        String aimStr = resultSet.getString(resultSet.getColumnIndex("Aim"));
        int drawable = resultSet.getInt(resultSet.getColumnIndex("Drawable"));

        float aim = 0;
        try{
            aim = Float.parseFloat(aimStr);
        }catch (Exception ex){

        }

        ExpenseCategory category = new ExpenseCategory(id,name);
        category.setAim(aim);
        category.setDrawable(drawable);

        return category;
    }

    public static boolean recordExists(String TableName, String dbfield, String fieldValue) {
        String Query = "Select * from " + TableName + " where " + dbfield + " = '" + fieldValue+"'";
        Cursor cursor = budgetDB.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            return false;
        }

        cursor.close();
        return true;
    }
}
