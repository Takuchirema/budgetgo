package com.mygoals.budgetgo;

import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.lang.System.out;

/**
 * Created by Takunda Chirema on 2017-07-22.
 */

public class Goal implements Parcelable, Comparable<Goal>{

    private String description;
    private Duration duration;
    private float aim;
    private float value;
    private int relativePriority;
    private int id;
    private String name;
    private EGoalType goalType;
    private int progress;
    private int requiredProgress;
    private boolean complete = false;
    private int savingsAccountId;
    private int drawable;

    private int mData;
    //format is dd-mm-yyyy
    private String startDay;
    private String endDay;

    //percentage of value against aim
    private double status;

    public Goal(int id){
        this.id = id;
    }

    private Goal(Parcel in) {
        mData = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        out.write(mData);
    }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<Goal> CREATOR = new Parcelable.Creator<Goal>() {
        public Goal createFromParcel(Parcel in) {
            return new Goal(in);
        }

        public Goal[] newArray(int size) {
            return new Goal[size];
        }
    };


    public ContentValues getContentValues(){
        ContentValues data = new ContentValues();

        data.put("Name",name);
        data.put("Description",description);
        data.put("Aim",aim);
        data.put("Value",value);
        data.put("Priority",relativePriority);
        data.put("GoalType",goalType.toString());
        data.put("Complete",complete);
        data.put("StartDay",startDay);
        data.put("EndDay",endDay);
        data.put("SavingsAccountId",savingsAccountId);
        data.put("Drawable",drawable);

        return data;
    }

    //This class save the account to the db
    //Also updates the static Account map
    public void save(){
        Goal newGoal = SQLite.storeGoal(this);
        Goals.goalsMap.put(newGoal.getId(),newGoal);
    }

    public void delete(){
        boolean deleted = SQLite.deleteGoal(this.id);
        if (deleted){
            Goals.remove(this.id);
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public float getAim() {
        return aim;
    }

    public void setAim(float aim) {
        this.aim = aim;
    }

    public float getValue() {
        return roundOff(value);
    }

    public void setValue(float value) {
        this.value = value;
    }

    public int getRelativePriority() {
        return relativePriority;
    }

    public void setRelativePriority(int relativePriority) {
        this.relativePriority = relativePriority;
    }

    public double getStatus() {
        return status;
    }

    public void setStatus(double status) {
        this.status = status;
    }

    public EGoalType getGoalType() {
        return goalType;
    }

    public void setGoalType(EGoalType EGoalType) {
        this.goalType = EGoalType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSavingsAccountId() {
        return savingsAccountId;
    }

    public void setSavingsAccountId(int savingsAccountId) {
        this.savingsAccountId = savingsAccountId;
    }

    public int getDrawable() {

        if (drawable == 0){
            return R.drawable.vacation;
        }

        return drawable;
    }

    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }

    /**
     * This shows the amount still left to be covered for goal to be reached
     * **/
    public int getProgress() {

        float fraction = (float)value/aim;
        progress = (int)((fraction)*1000);
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public int getRequiredProgress() {
        int daysSpent = daysBetween() - daysLeft();

        out.println("days left: "+daysLeft()+" days spent: "+daysSpent+" goal value: "+aim);
        float daysSpentFraction = 0;
        if (daysSpent > 0) {
            daysSpentFraction = (float) daysSpent / daysBetween();
        }

        requiredProgress = (int)(daysSpentFraction*1000);
        return requiredProgress;
    }

    public float getRequiredValue(){
        int daysSpent = daysBetween() - daysLeft();

        out.println("days left: "+daysLeft()+" days spent: "+daysSpent+" goal value: "+aim);
        float daysSpentFraction = 0;
        if (daysSpent > 0) {
            daysSpentFraction = (float) daysSpent / daysBetween();
        }

        float requiredValue= daysSpentFraction*aim;
        return roundOff(requiredValue);
    }

    public void setRequiredProgress(int requiredProgress) {
        this.requiredProgress = requiredProgress;
    }

    public String getStartDay() {
        return startDay;
    }

    public void setStartDay(String startDay) {
        this.startDay = startDay;
    }

    public String getEndDay() {
        return endDay;
    }

    public void setEndDay(String endDay) {
        this.endDay = endDay;
    }

    public int daysBetween(){

        try {
            DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Date startDate = formatter.parse(startDay);
            Date endDate = formatter.parse(endDay);

            int days = daysBetween(startDate, endDate);
            return days;
        }catch (Exception ex){
            ex.printStackTrace();
            return 0;
        }
    }

    public int daysLeft(){
        DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        String currentDay = sdf.format(date);

        try {
            DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Date endDate = formatter.parse(endDay);
            Date currentDate = formatter.parse(currentDay);

            int days = 0;
            if (currentDate.before(endDate)) {
                days = daysBetween(currentDate, endDate);
            }

            return days;
        }catch (Exception ex){
            ex.printStackTrace();
            return 0;
        }

    }

    public int daysBetween(Date d1, Date d2){
        return (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
    }

    public float roundOff(float value){
        float roundOff = (float) (Math.round((double)value * 100.0) / 100.0);
        return roundOff;
    }

    public boolean isComplete() {
        return complete;
    }

    public void complete(boolean isComplete){
        if (isComplete){
            complete = true;
        }
        else{
            complete = false;
        }
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    @Override
    public int compareTo(Goal goal) {
        if (goal.relativePriority > this.relativePriority){
            return -1;
        }else{
            return 1;
        }
    }
}
