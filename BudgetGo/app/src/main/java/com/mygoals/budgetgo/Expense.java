package com.mygoals.budgetgo;

/**
 * Created by Takunda Chirema on 2017-07-22.
 */

public class Expense {

    private float value;
    private int expenseCategory;
    private String expenseDay;
    private String expenseDescription;
    private int expenseAccountId;
    private String expenseAccountName;

    public Expense(float value,int expenseCategory){
        this.value = value;
        this.expenseCategory=expenseCategory;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public int getExpenseCategory() {
        return expenseCategory;
    }

    public void setExpenseCategory(int expenseCategory) {
        this.expenseCategory = expenseCategory;
    }

    public String getExpenseDay() {
        return expenseDay;
    }

    public void setExpenseDay(String expenseDay) {
        this.expenseDay = expenseDay;
    }

    public String getExpenseDescription() {
        return expenseDescription;
    }

    public void setExpenseDescription(String expenseDescription) {
        this.expenseDescription = expenseDescription;
    }

    public int getExpenseAccountId() {
        return expenseAccountId;
    }

    public void setExpenseAccountId(int expenseAccountId) {
        this.expenseAccountId = expenseAccountId;
    }

    public String getExpenseAccountName() {
        return expenseAccountName;
    }

    public void setExpenseAccountName(String expenseAccountName) {
        this.expenseAccountName = expenseAccountName;
    }
}
