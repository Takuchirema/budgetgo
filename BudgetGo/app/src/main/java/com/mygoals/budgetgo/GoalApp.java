package com.mygoals.budgetgo;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.stetho.Stetho;
import com.google.android.gms.vision.text.Line;

import org.joda.time.DateTime;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.lang.reflect.Field;

public class GoalApp extends AppCompatActivity implements TabLayout.OnTabSelectedListener,
        NavigationView.OnNavigationItemSelectedListener, DatePickerDialog.OnDateSetListener, IProcess {

    ViewPager viewPager;
    public DrawerLayout drawer;
    private Menu menu;
    public static String processMonth;
    public static Context context;

    //This is our tablayout
    private TabLayout tabLayout;
    private static IProcess process;
    private static TextView savingsTV;
    private static TextView currentsTV;
    private static EditText searchItemsET;
    private static ImageView searchItemsIM;
    private static int currentTabPosition;
    public static ArrayList<Integer> imageResources = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goal_app);

        context = GoalApp.this;
        process = GoalApp.this;

        Stetho.initializeWithDefaults(this);
        //Ask for permissions for external access
        if (canWrite() && canReadSMS()) {
            prepareApp();
        }else {
            getPermissions();
        }
    }

    public void prepareApp(){

        // Get the data if available before anything else
        Goals.getDBGoals();
        ExpenseCategories.getDBExpenseCategories();
        Accounts.getDBAccounts();

        String menuFragment = getIntent().getStringExtra("menuFragment");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        setSupportActionBar(toolbar);
        savingsTV = (TextView) findViewById(R.id.savingsBalance);
        currentsTV = (TextView) findViewById(R.id.currentsBalance);
        searchItemsET = (EditText) findViewById(R.id.searchItems);
        searchItemsIM = (ImageView) findViewById(R.id.searchItemsIm);

        currentsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context,ViewIncomeExpenditure.class);
                context.startActivity(i);
            }
        });

        savingsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context,ViewSavings.class);
                context.startActivity(i);
            }
        });

        searchItemsET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= searchItemsET.getRight() - searchItemsET.getTotalPaddingRight()){
                        searchItemsET.setText("");
                        searchItemsET.setVisibility(View.GONE);
                        searchItemsIM.setVisibility(View.VISIBLE);
                        filterView(null);
                        return true;
                    }
                }
                return false;
            }
        });

        searchItemsET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filterView(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        searchItemsIM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchItemsET.setVisibility(View.VISIBLE);
                searchItemsIM.setVisibility(View.GONE);
            }
        });

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);

        //Initializing the tablayout
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        //Adding the tabs using addTab() method
        tabLayout.addTab(tabLayout.newTab().setText("Accounts"));
        tabLayout.addTab(tabLayout.newTab().setText("Expenses"));
        tabLayout.addTab(tabLayout.newTab().setText("Goals"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                currentTabPosition = tab.getPosition();
                //System.out.println("current tab: "+currentTabPosition);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        //Initializing viewPager
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setOffscreenPageLimit(2);

        //Creating our pager adapter
        Pager adapter = new Pager(this,getSupportFragmentManager(), tabLayout.getTabCount());

        //Adding adapter to pager
        viewPager.setAdapter(adapter);

        //Adding onTabSelectedListener to swipe views
        tabLayout.setOnTabSelectedListener(this);
        tabLayout.setupWithViewPager(viewPager);

        NavigationView navigationView = (NavigationView)findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Process for the current month
        processMonth = new SimpleDateFormat("MMM").format(new Date());

        if (menuFragment != null){
            if (menuFragment.equals("Goals")){
                viewPager.setCurrentItem(0);
            }
            else if (menuFragment.equals("Expenses")){
                viewPager.setCurrentItem(1);
            }
            else if (menuFragment.equals("Accounts")){
                viewPager.setCurrentItem(2);
            }
        }else {
            getSMSTransactions();
        }

        prepareImageSelection();
    }

    public void prepareImageSelection(){
        Field[] ID_Fields = R.drawable.class.getFields();
        for(int i = 0; i < ID_Fields.length; i++) {
            try {
                if (checkExtension(ID_Fields[i].getInt(null))) {
                    imageResources.add(ID_Fields[i].getInt(null));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public boolean checkExtension(int resourceId){
        TypedValue value = new TypedValue();

        getResources().getValue(resourceId, value, true);
        String name = value.string.toString();
        String extension = name.substring(name.lastIndexOf(".") + 1);

        ////System.out.println("name: "+name+"extension: "+extension);

        if (name.contains("ic_")){
            return false;
        }

        if (extension.equals("png") || extension.equals("jpg") || extension.equals("jpeg")){
            return true;
        }

        return false;
    }

    public void filterView(String searchString){
        if (currentTabPosition == 0){
            ViewAccounts.filter(searchString);
        }
        else if (currentTabPosition == 1){
            ViewExpenses.filter(searchString);
        }
        else if (currentTabPosition == 2){
            ViewGoals.filter(searchString);
        }
    }

    public void getPermissions(){
        String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_SMS};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, 1);
        }
    }

    public boolean canWrite() {
        return(hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    public void getSMSTransactions(){
        processingBalances();

        Transactions transactions = new Transactions(this);
        transactions.setProcessCaller(GoalApp.this);
        transactions.execute();
    }

    public static void processNewMessages(){
        if (process!=null) {
            processingBalances();

            Transactions transactions = new Transactions(context);
            transactions.setProcessCaller(process);
            transactions.execute();
        }
    }

    public static void reprocessTransactionAccounts(){
        processingBalances();

        Transactions transactions = new Transactions(context);
        transactions.setProcessCaller(process);
        transactions.setReprocessAccounts(true);
        transactions.execute();
    }

    public static void processingBalances(){
        setMonthIcon();
        currentsTV.setText("Processing...");
        savingsTV.setText("Processing...");
    }

    public static void setMonthIcon(){
        ImageView monthIcon = (ImageView) ((Activity)context).findViewById(R.id.monthIcon);
        TextView monthText = ((TextView)((Activity)context).findViewById(R.id.monthText));

        if (monthIcon == null){
            return;
        }

        monthIcon.setBackgroundResource(getMonthIcon(processMonth));
        if (processMonth == null) {
            monthText.setText("All");
        }else{
            monthText.setText(processMonth);
        }
    }

    public static int getMonthIcon(String mmm){

        if (mmm == null){
            return R.drawable.mar;
        }

        if (mmm.equalsIgnoreCase("jan")){
            return R.drawable.jan;
        }else if (mmm.equalsIgnoreCase("feb")){
            return R.drawable.feb;
        }else if (mmm.equalsIgnoreCase("mar")){
            return R.drawable.mar;
        }else if (mmm.equalsIgnoreCase("apr")){
            return R.drawable.apr;
        }else if (mmm.equalsIgnoreCase("may")){
            return R.drawable.may;
        }else if (mmm.equalsIgnoreCase("jun")){
            return R.drawable.jun;
        }else if (mmm.equalsIgnoreCase("jul")){
            return R.drawable.jul;
        }else if (mmm.equalsIgnoreCase("aug")){
            return R.drawable.aug;
        }else if (mmm.equalsIgnoreCase("sep")){
            return R.drawable.sep;
        }else if (mmm.equalsIgnoreCase("oct")){
            return R.drawable.oct;
        }else if (mmm.equalsIgnoreCase("nov")){
            return R.drawable.nov;
        }else if (mmm.equalsIgnoreCase("dec")){
            return R.drawable.dec;
        }
        return R.drawable.mar;
    }

    public static void reprocessGoalProgress(){
        ViewGoals.refreshGoalsProgress();
    }

    public static void reloadGoalProgress(){
        ViewGoals.reloadGoalsProgress();
    }

    public static void reprocessExpenses(){
        ViewExpenses.refreshExpenseBalances(processMonth);
    }

    private void checkPermissions(){
        int getPermission = 2;
        if (ContextCompat.checkSelfPermission(GoalApp.this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(GoalApp.this,new String[]{Manifest.permission.READ_SMS},getPermission);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode,permissions,grantResults);
        switch (requestCode){
            case 1:
                //Ask for permissions for external access
                if (canWrite() && canReadSMS()) {
                    prepareApp();
                }else {
                    getPermissions();
                }
                break;
        }

    }

    public boolean canReadSMS(){
        return(hasPermission(Manifest.permission.READ_SMS));
    }

    public boolean hasPermission(String perm) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return(PackageManager.PERMISSION_GRANTED== this.checkSelfPermission(perm));
        }else{
            return true;
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.goal_app, menu);
        this.menu=menu;
        editCurrentMenuView("Processing...");
        editSavingsMenuView("Processing...");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id=item.getItemId();
        switch (id) {
            case R.id.nav_process:
                createDialogWithoutDateField();
                break;
            case R.id.nav_settings:
                Intent i = new Intent(context,Settings.class);
                context.startActivity(i);
                break;
            /*case R.id.nav_gallery:
                Toast.makeText(GoalApp.this, "Gallery clicked", Toast.LENGTH_LONG).show();
                break;
            case R.id.nav_manage:
                Toast.makeText(GoalApp.this, "Manage clicked", Toast.LENGTH_LONG).show();
                break;
            case R.id.nav_share:
                Toast.makeText(GoalApp.this, "Share clicked", Toast.LENGTH_LONG).show();
                break;
            case R.id.nav_slideshow:
                Toast.makeText(GoalApp.this, "Slideshow clicked", Toast.LENGTH_LONG).show();
                break;
            case R.id.nav_view:
                Toast.makeText(GoalApp.this, "View clicked", Toast.LENGTH_LONG).show();
                break;*/
        }
        return false;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

    }

    private void createDialogWithoutDateField() {
        LayoutInflater inflater = this.getLayoutInflater();
        View layout = inflater.inflate(R.layout.date_picker, null, true);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final DatePicker picker = (DatePicker)layout.findViewById(R.id.datePicker);
        picker.findViewById(Resources.getSystem().getIdentifier("day","id","android")).setVisibility(View.GONE);

        builder.setTitle("Select Transaction Month");
        builder.setView(layout);

        builder.setNegativeButton("All",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                processMonth = null;
                getSMSTransactions();
                dialog.dismiss();
            }
        });

        builder.setPositiveButton("Set", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                processMonth = new DateFormatSymbols().getMonths()[picker.getMonth()];
                processMonth = processMonth.substring(0,3);

                //System.out.println("process month: "+processMonth);
                getSMSTransactions();
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();

        Button ok = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        LinearLayout.LayoutParams okButtonLL = (LinearLayout.LayoutParams) ok.getLayoutParams();
        okButtonLL.gravity = Gravity.CENTER;
        okButtonLL.width = 500;
        ok.setLayoutParams(okButtonLL);

        Button cancel = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        LinearLayout.LayoutParams noButtonLL = (LinearLayout.LayoutParams) cancel.getLayoutParams();
        noButtonLL.gravity = Gravity.CENTER;
        noButtonLL.width = 500;
        cancel.setLayoutParams(noButtonLL);
    }

    public static void editSavingsMenuView(String value){
        /*
        LayoutInflater inflater = GoalApp.this.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.account_balance, null, true);

        TextView accountValueView = (TextView) rowView.findViewById(R.id.accountValue);

        accountValueView.setText("Savings: "+value);
        accountValueView.setTextColor(Color.WHITE);

        MenuItem currentItem = menu.findItem(R.id.action_savings);
        currentItem.setActionView(rowView);
        */
        savingsTV.setText("Savings: "+value);
    }

    public static void editCurrentMenuView(String value){
        /*
        LayoutInflater inflater = GoalApp.this.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.account_balance, null, true);

        TextView accountValueView = (TextView) rowView.findViewById(R.id.accountValue);

        accountValueView.setText("Current: "+value);
        accountValueView.setTextColor(Color.WHITE);

        MenuItem currentItem = menu.findItem(R.id.action_current);
        currentItem.setActionView(rowView);
        */
        currentsTV.setText("Current: "+value);
    }

    public static void refreshBalances(){
        //calculate balances in the accounts
        ViewAccounts.refreshAccountBalances(processMonth);
        ViewExpenses.refreshExpenseBalances(processMonth);
        ViewGoals.refreshGoalsProgress();
    }

    @Override
    public void postGetTransactionSMS(ArrayList<Transaction> transactions) {
        setMonthIcon();

        refreshBalances();

        if (!Transactions.sortedTransactionList.isEmpty()) {
            //System.out.println("%%%%%%%%% Latest Transaction %%%%%%%%%");
            Transaction transaction = Transactions.sortedTransactionList.get(0);
            Transactions.printTransaction(transaction);
            //System.out.println("%%%%%%%%% "+transaction.getDate()+" %%%%%%%%%");
            //String latestBalance = ""+transaction.getBalanceAfterTransaction();

            String currentAccountBalance = ""+getCurrentAccountBalances();
            editCurrentMenuView(currentAccountBalance);

            String savingsAccountBalance = ""+getSavingsAccountBalances();
            editSavingsMenuView(savingsAccountBalance);
        }else{
            editCurrentMenuView("0.0");
            editSavingsMenuView("0.0");
        }
    }

    public static float getCurrentAccountBalances(){
        float balance=0;
        Iterator<Account> iterator = Accounts.getIterator();
        while(iterator.hasNext()){
            Account account = iterator.next();
            if (Accounts.isCurrent(account)) {
                balance = balance+ account.getAccountValue();
            }
        }
        return balance;
    }

    public static float getSavingsAccountBalances(){
        float balance=0;

        Iterator<Account> iterator = Accounts.getIterator();
        while(iterator.hasNext()){
            Account account = iterator.next();
            if (account.getAccountType()==EAccountType.SAVINGS) {
                balance = balance+ account.getAccountValue();
            }
        }

        return balance;
    }
}
