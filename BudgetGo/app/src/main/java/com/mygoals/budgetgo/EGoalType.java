package com.mygoals.budgetgo;

/**
 * Created by Takunda Chirema on 2017-07-22.
 */

public enum EGoalType {
    Vacation,
    Car,
    Education,
    House,
    Study,
    Family
}
