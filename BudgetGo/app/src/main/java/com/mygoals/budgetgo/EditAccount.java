package com.mygoals.budgetgo;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Takunda Chirema on 2017-07-22.
 */

public class EditAccount extends Fragment implements IGetData,DatePickerDialog.OnDateSetListener{

    private Spinner accountTypeSP;
    private Spinner expenseCategorySP;
    private EditText accountNumberET;
    private GridLayout accountNamesLayout;
    private EditText accountDisplayNameET;
    private EditText accountBudgetET;
    private EditText accountBalanceET;
    private TextView saveAccountTV;
    private TextView deleteAccountTV;

    private TextView balanceAsAtTV;
    private int accountId;
    private Account account;
    private DatePickerDialog datePickerDialog;
    private Date balanceDate;

    boolean reprocessTransactions = false;

    private static View myFragmentView;

    private String accountName;
    private String accountType;

    private int expenseCategoryId;
    private String accountNumber;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.activity_create_account, container, false);

        ((TextView)myFragmentView.findViewById(R.id.title)).setText("Edit Account");

        accountTypeSP = (Spinner)myFragmentView.findViewById(R.id.selectAccountType);
        expenseCategorySP = (Spinner)myFragmentView.findViewById(R.id.selectExpenseCategory);
        accountNumberET = (EditText)myFragmentView.findViewById(R.id.accountNumber);
        accountNamesLayout = (GridLayout) myFragmentView.findViewById(R.id.accountNames);
        accountDisplayNameET = (EditText) myFragmentView.findViewById(R.id.displayName);
        accountBudgetET = (EditText)myFragmentView.findViewById(R.id.accountBudget);
        accountBalanceET = (EditText)myFragmentView.findViewById(R.id.accountBalance);
        saveAccountTV = (TextView)myFragmentView.findViewById(R.id.saveAccount);
        balanceAsAtTV = (TextView)myFragmentView.findViewById(R.id.balanceTxt);

        deleteAccountTV = (TextView)myFragmentView.findViewById(R.id.deleteAccount);
        deleteAccountTV.setVisibility(View.VISIBLE);

        if (account.getBalanceDate() != null) {
            String formatted = account.getDay()+"-"+account.getMonth()+"-"+account.getYear();
            ((TextView) myFragmentView.findViewById(R.id.balanceTxt)).setText("Balance As At " + formatted);
        }

        String accountTypeUpperString = accountType.substring(0,1).toUpperCase() + accountType.substring(1).toLowerCase();
        accountTypeSP.setSelection(((ArrayAdapter<String>)accountTypeSP.getAdapter()).getPosition(accountTypeUpperString));

        accountNumberET.setText(accountNumber);
        accountBalanceET.setText(""+account.getBalanceAsAt());
        accountBudgetET.setText(""+account.getBudget());
        accountDisplayNameET.setText(account.getDisplayName());

        setAccountNames();
        setSpinner();

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);

        if (Accounts.isExpense(account.getAccountType())){
            accountBalanceET.setKeyListener(null);
        }else{
            expenseCategorySP.setOnKeyListener(null);
        }

        setActions(account.getAccountType());
        setListeners();
        return myFragmentView;
    }

    private void setSpinner(){
        List<StringWithTag> list = ExpenseCategories.getSpinnerItems();
        ArrayAdapter<StringWithTag> adapter = new ArrayAdapter<StringWithTag>(getContext(), R.layout.spinner_text, list);
        expenseCategorySP.setAdapter(adapter);

        for (int i=0;i<list.size();i++){
            if (expenseCategoryId == list.get(i).tag){
                expenseCategorySP.setSelection(i);
                break;
            }
        }
    }

    public void setAccountNames(){
        String[] accountNamesArr = account.getPossibleAccountNames().split("\\s*,[,\\s]*");
        LayoutInflater inflater = getActivity().getLayoutInflater();

        for (String name:accountNamesArr){
            View view = inflater.inflate(R.layout.account_name, null, true);
            final TextView accountNameTV = (TextView) view.findViewById(R.id.accountName);
            accountNameTV.setText(name);

            if (account.getNames().contains(name)) {
                accountNameTV.setBackgroundResource(R.drawable.green_rounded_rectangle);
            }else{
                accountNameTV.setBackgroundResource(R.drawable.orange_rounded_rectangle);
            }

            accountNameTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String name = accountNameTV.getText().toString();
                    String displayName = "";

                    if (account.getNames().contains(name)) {
                        int index = account.getNames().indexOf(name);
                        //System.out.println("contains name");
                        accountNameTV.setBackgroundResource(R.drawable.orange_rounded_rectangle);
                        account.getNames().remove(index);
                    }else{
                        //System.out.println("does not contains name");
                        accountNameTV.setBackgroundResource(R.drawable.green_rounded_rectangle);
                        account.getNames().add(name);
                    }

                    reprocessTransactions = true;

                    for (String possibleName:account.getNames()){
                        displayName = displayName+" "+possibleName;
                    }

                    accountDisplayNameET.setText(displayName);

                    account.setName(account.getStringNames());
                    //System.out.println("acc name: "+account.getName());
                }
            });

            accountNamesLayout.addView(view);
        }
    }

    public void setListeners(){

        if (!account.isEditable()){
            saveAccountTV.setBackgroundColor(Color.LTGRAY);
            deleteAccountTV.setBackgroundColor(Color.LTGRAY);
            return;
        }

        saveAccountTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveAccount();
            }
        });

        if (!Accounts.isExpense(account.getAccountType())) {
            balanceAsAtTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    datePickerDialog.show();
                }
            });
        }

        deleteAccountTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAccount();
            }
        });

        accountTypeSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                EAccountType type = Accounts.getAccountType(accountTypeSP.getSelectedItem().toString());
                setActions(type);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    private void setActions(EAccountType type){

        if (account.getPossibleAccountNames().length() > 1){
            myFragmentView.findViewById(R.id.accountIdContainer).setVisibility(View.VISIBLE);
        }

        if (!Accounts.isExpense(type)){
            myFragmentView.findViewById(R.id.accountNumberContainer).setVisibility(View.VISIBLE);
            if (type == EAccountType.SAVINGS){
                myFragmentView.findViewById(R.id.accountBalanceContainer).setVisibility(View.VISIBLE);
            }
        }else{
            myFragmentView.findViewById(R.id.accountBudgetContainer).setVisibility(View.VISIBLE);
            myFragmentView.findViewById(R.id.selectExpenseCategoryContainer).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        month += 1;

        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        try {
            balanceDate = formatter.parse(dayOfMonth + "-" + month + "-" + year);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //System.out.println("set balance date: "+balanceDate.getTime());

        String balanceDateStr = dayOfMonth + "-" + month + "-" + year;
        balanceAsAtTV.setText(balanceDateStr);

    }

    public void viewTransactions(){
        account = Accounts.get(accountId);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        LinearLayout transactionList = (LinearLayout)myFragmentView.findViewById(R.id.transactionList);

        if (account!=null){
            HashMap<Long,Transaction> transactions = account.getTransactions();

            for (Map.Entry<Long,Transaction> e: transactions.entrySet()){
                Transaction transaction = e.getValue();

                //System.out.println("Transaction: "+transaction.getTransactionMessage()+" account: "+account.getName());
                View transactionView = inflater.inflate(R.layout.transaction, null, true);
                TextView month = (TextView) transactionView.findViewById(R.id.transactionMonth);
                TextView day = (TextView) transactionView.findViewById(R.id.transactionDay);
                TextView year = (TextView) transactionView.findViewById(R.id.transactionYear);

                TextView message = (TextView) transactionView.findViewById(R.id.transactionMessage);
                TextView value = (TextView) transactionView.findViewById(R.id.transactionValue);

                month.setText(transaction.getMonth());
                day.setText(transaction.getDay());
                year.setText(transaction.getYear());

                message.setText(transaction.getTransactionMessage());
                value.setText(""+transaction.getValue());

                transactionList.addView(transactionView);
            }
        }else{
            //System.out.println("Account is null");
        }
    }

    public void saveAccount(){
        String accountBudgetStr = accountBudgetET.getText().toString();
        String accountBalanceStr = accountBalanceET.getText().toString();
        String accountNumberStr = accountNumberET.getText().toString();
        String displayName = accountDisplayNameET.getText().toString();

        EAccountType accountType = Accounts.getAccountType(accountTypeSP.getSelectedItem().toString());

        StringWithTag selectedItem = (StringWithTag) expenseCategorySP.getSelectedItem();

        int expenseCategoryId = selectedItem.tag;

        if (!TextUtils.isEmpty(accountBudgetStr)) {
            Float accountBudget = Float.parseFloat(accountBudgetStr);
            account.setBudget(accountBudget);
        }

        if (!Accounts.isExpense(accountType) && !Accounts.isIncome(accountType)) {
            if (TextUtils.isEmpty(accountNumberStr)) {
                accountNumberET.setError("Please set the account number");
                return;
            }
        }

        if (!TextUtils.isEmpty(accountBalanceStr)) {
            Float accountBalance = Float.parseFloat(accountBalanceStr);
            account.setBalanceAsAt(accountBalance);
            if (balanceDate != null){
                account.setBalanceDate(balanceDate);

                Transaction transaction = new Transaction(balanceDate.getTime(),"Balance as at: "+balanceDate);
                transaction.setDate(balanceDate.getTime());
                transaction.setActualDate(balanceDate);
                transaction.setValue(accountBalance);
                transaction.setDebitAccountId(account.getId());
                transaction.save();
            }else{
                //System.out.println("save balance date: null");
            }
        }

        if (!TextUtils.isEmpty(accountNumberStr)) {
            if (!accountNumberStr.equalsIgnoreCase(account.getAccountNumber())){
                reprocessTransactions = true;
            }
            account.setAccountNumber(accountNumberStr);
        }

        if (account.getPossibleAccountNames().isEmpty()) {
            if (!displayName.equalsIgnoreCase(account.getDisplayName())) {
                reprocessTransactions = true;
            }
        }

        account.setDisplayName(displayName);
        account.setAccountType(accountType);
        account.setExpenseCategoryId(expenseCategoryId);

        ExpenseCategory expenseCategory = ViewExpenses.getExpenseCategoriesMap().get(getExpenseAccountId());
        if (expenseCategory != null) {
            ViewExpenses.getExpenseCategoriesMap().get(getExpenseAccountId()).getAccounts().put(account.getId(), account);
        }

        //Store into database
        account.save();

        if (reprocessTransactions) {
            GoalApp.reprocessTransactionAccounts();
        }

        GoalApp.refreshBalances();

        getActivity().finish();
    }

    private void deleteAccount(){
        ArrayList<Transaction> transactions = new ArrayList<>(account.getTransactions().values());
        account.delete();

        GoalApp.refreshBalances();

        Transactions.reprocessTransactions(transactions);
        //GoalApp.reprocessTransactionAccounts();
        getActivity().finish();
    }

    public int getExpenseAccountId(){
        String selected = expenseCategorySP.getSelectedItem().toString();

        if (selected.equalsIgnoreCase("rent")){
            return 1;
        }else if (selected.equalsIgnoreCase("food")){
            return 2;
        }else if (selected.equalsIgnoreCase("School Fees")){
            return 3;
        }else if (selected.equalsIgnoreCase("Electricity")){
            return 4;
        }else if (selected.equalsIgnoreCase("Transport")){
            return 5;
        }else if (selected.equalsIgnoreCase("Medical")){
            return 6;
        }else if (selected.equalsIgnoreCase("Insurance")){
            return 7;
        }else if (selected.equalsIgnoreCase("Other")){
            return 8;
        }

        return 1;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public int getExpenseCategoryId() {
        return expenseCategoryId;
    }

    public void setExpenseCategoryId(int expenseCategoryId) {
        this.expenseCategoryId = expenseCategoryId;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public void postGetGoals(LinkedHashMap<String, Goal> goals) {

    }

    @Override
    public void postGetExpenses(HashMap<String, Expense> expense) {

    }

    @Override
    public void postGetShops(HashMap<String, Shop> shops) {

    }

    @Override
    public void postGetAccounts(HashMap<String, Account> accounts) {

    }
}
