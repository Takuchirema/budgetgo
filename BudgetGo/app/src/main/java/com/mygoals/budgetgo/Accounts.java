package com.mygoals.budgetgo;

import android.graphics.Color;

import java.sql.SQLWarning;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Takunda Chirema on 2017-10-21.
 */

public class Accounts {

    private static SyncLinkedHashMap<Integer, Account> accountsMap = new SyncLinkedHashMap<>();

    // There is only one instance of the income account
    private static Account incomeAccount;
    private static Account expenseAccount;

    public static void setAccountsMap(SyncLinkedHashMap<Integer, Account> accountsMap) {
        Accounts.accountsMap = accountsMap;
    }

    public static Iterator<Account> getIterator(){
        return accountsMap.iterator();
    }

    public static void put(int id,Account account){
        accountsMap.put(id,account);
    }

    public static Account get(int id){
       return accountsMap.get(id);
    }

    public static boolean isEmpty(){
        return accountsMap.isEmpty();
    }

    public static int size(){
        return  accountsMap.size();
    }
    public static Collection<Account> values(){
        return accountsMap.values();
    }

    public static void createAccounts(){
        getDBAccounts();

        if (accountsMap.isEmpty()){
            createDefaultAccounts();
        }
    }

    public static void createDefaultAccounts(){
        ArrayList<Account> accounts = new ArrayList<>();

        Account a1 = new Account(0);
        a1.setAccountType(EAccountType.SHOP);
        a1.setName("Checkers");
        a1.setDisplayName("Checkers");
        a1.setBudget(5000);
        a1.setExpenseCategoryId(ExpenseCategories.getCategoryByName("Food").getId());
        a1.setExpensesValue(0);

        Account a2 = new Account(0);
        a2.setAccountType(EAccountType.SHOP);
        a2.setName("Pick n Pay,PnP");
        a2.setDisplayName("Pick n Pay");
        a2.setBudget(5000);
        a2.setExpenseCategoryId(ExpenseCategories.getCategoryByName("Food").getId());
        a2.setExpensesValue(0);

        Account a3 = new Account(0);
        a3.setPhoneNumber("+27839206");
        a3.setAccountType(EAccountType.SHOP);
        a3.setName("Shoprite");
        a3.setDisplayName("Shoprite");
        a3.setBudget(5000);
        a3.setExpenseCategoryId(ExpenseCategories.getCategoryByName("Food").getId());
        a3.setExpensesValue(0);

        Account a4 = new Account(0);
        a4.setPhoneNumber("+27839206");
        a4.setAccountType(EAccountType.SHOP);
        a4.setName("Woolworths");
        a4.setDisplayName("Woolworths");
        a4.setBudget(5000);
        a4.setExpenseCategoryId(ExpenseCategories.getCategoryByName("Food").getId());
        a4.setExpensesValue(0);

        Account a5 = new Account(0);
        a5.setPhoneNumber("+27839206");
        a5.setAccountType(EAccountType.RESTAURANT);
        a5.setName("Nandos");
        a5.setDisplayName("Nandos");
        a5.setBudget(5000);
        a5.setExpenseCategoryId(ExpenseCategories.getCategoryByName("Food").getId());
        a5.setExpensesValue(0);

        Account a6 = new Account(0);
        a6.setPhoneNumber("+27839206");
        a6.setAccountType(EAccountType.RESTAURANT);
        a6.setName("KFC");
        a6.setDisplayName("KFC");
        a6.setBudget(5000);
        a6.setExpenseCategoryId(ExpenseCategories.getCategoryByName("Food").getId());
        a6.setExpensesValue(0);

        Account a7 = new Account(0);
        a7.setPhoneNumber("+27839206");
        a7.setAccountType(EAccountType.RESTAURANT);
        a7.setName("Burger King");
        a7.setDisplayName("Burger King");
        a7.setBudget(5000);
        a7.setExpenseCategoryId(ExpenseCategories.getCategoryByName("Food").getId());
        a7.setExpensesValue(0);

        Account a8 = new Account(0);
        a8.setPhoneNumber("+27839206");
        a8.setAccountType(EAccountType.RESTAURANT);
        a8.setName("Steers");
        a8.setDisplayName("Steers");
        a8.setBudget(5000);
        a8.setExpenseCategoryId(ExpenseCategories.getCategoryByName("Food").getId());
        a8.setExpensesValue(0);

        Account a9 = new Account(0);
        a9.setAccountType(EAccountType.EXPENSE);
        a9.setName("Withdrawals");
        a9.setDisplayName("Withdrawals");
        a9.setBudget(5000);
        a9.setExpenseCategoryId(ExpenseCategories.getDefaultCategory().getId());
        a9.setAccountValue(0);

        Account a10 = new Account(0);
        a10.setPhoneNumber("+27839206");
        a10.setAccountType(EAccountType.SHOP);
        a10.setName("airtime");
        a10.setDisplayName("airtime");
        a10.setBudget(5000);
        a10.setExpenseCategoryId(ExpenseCategories.getCategoryByName("Internet").getId());
        a10.setExpensesValue(0);

        Account a11 = new Account(0);
        a11.setAccountType(EAccountType.INCOME);
        a11.setName("Income");
        a11.setDisplayName("Income");
        a11.setExpenseCategoryId(ExpenseCategories.getDefaultCategory().getId());
        a11.setExpensesValue(0);

        Account a12 = new Account(0);
        a12.setPhoneNumber("+27839206");
        a12.setAccountType(EAccountType.SHOP);
        a12.setName("Edgars");
        a12.setDisplayName("Edgars");
        a12.setBudget(5000);
        a12.setExpenseCategoryId(ExpenseCategories.getCategoryByName("Clothes").getId());
        a12.setExpensesValue(0);

        Account a13 = new Account(0);
        a13.setAccountType(EAccountType.SHOP);
        a13.setName("MrPrice");
        a13.setDisplayName("Mr Price");
        a13.setBudget(5000);
        a13.setExpenseCategoryId(ExpenseCategories.getCategoryByName("Clothes").getId());
        a13.setExpensesValue(0);

        accounts.add(a1);
        accounts.add(a2);
        accounts.add(a3);
        accounts.add(a4);
        accounts.add(a5);
        accounts.add(a6);
        accounts.add(a7);
        accounts.add(a8);
        accounts.add(a9);
        accounts.add(a10);
        //accounts.add(a11);
        accounts.add(a12);
        accounts.add(a13);

        accountsMap = SQLite.storeAccounts(accounts);
        //System.out.println("after store db: "+accountsMap.size());
    }

    public static void getDBAccounts(){
        if (accountsMap.isEmpty()) {
            accountsMap = SQLite.getAccounts();
            //System.out.println("after get db: " + accountsMap.size());

            if (accountsMap.isEmpty()){
                createDefaultAccounts();
            }
        }
    }

    public static Account getTotalIncomeAccount(){

        if (incomeAccount != null) {
            return incomeAccount;
        }

        for (Map.Entry<Integer,Account> e:accountsMap.entrySet()){
            Account account = e.getValue();
            if (isTotalIncomeAccount(account)){
                incomeAccount = account;
                return incomeAccount;
            }
        }

        createIncomeAccount();

        return incomeAccount;
    }

    public static Account getUnknownExpenseAccount(){

        if (expenseAccount != null) {
            return expenseAccount;
        }

        for (Map.Entry<Integer,Account> e:accountsMap.entrySet()){
            Account account = e.getValue();
            if (isUnknownExpenseAccount(account)){
                expenseAccount = account;
                return expenseAccount;
            }
        }

        createExpenseAccount();

        return expenseAccount;
    }

    public static boolean isTotalIncomeAccount(Account account){

        if (account.getAccountType() == EAccountType.INCOME
                && account.getName().equalsIgnoreCase("Total Income")){
            return true;
        }

        return false;
    }

    public static boolean isUnknownExpenseAccount(Account account){

        if (account.getAccountType() == EAccountType.EXPENSE
                && account.getName().equalsIgnoreCase("Unknown Expense")){
            return true;
        }

        return false;
    }

    private static Account createIncomeAccount(){
        Account a11 = new Account(0);
        a11.setAccountType(EAccountType.INCOME);
        a11.setName("Total Income");
        a11.setExpenseCategoryId(ExpenseCategories.getDefaultCategory().getId());
        a11.setExpensesValue(0);

        incomeAccount = SQLite.storeAccount(a11);
        accountsMap.put(incomeAccount.getId(),incomeAccount);

        return incomeAccount;
    }

    private static Account createExpenseAccount(){
        Account a11 = new Account(0);
        a11.setAccountType(EAccountType.EXPENSE);
        a11.setName("Unknown Expense");
        a11.setExpenseCategoryId(ExpenseCategories.getDefaultCategory().getId());
        a11.setExpensesValue(1000);

        expenseAccount = SQLite.storeAccount(a11);
        accountsMap.put(expenseAccount.getId(),expenseAccount);

        return expenseAccount;
    }

    public static ArrayList<Account> getSalarayAccounts(){

        ArrayList<Account> accounts = new ArrayList<>();

        for (Map.Entry<Integer,Account> e:accountsMap.entrySet()){
            Account account = e.getValue();
            if (account.getAccountType() == EAccountType.SALARY){
                accounts.add(account);
            }
        }

        return accounts;
    }

    public static void storeDBAccounts(ArrayList<Account> accounts){
        accountsMap = SQLite.storeAccounts(accounts);
    }

    public static void remove(int accountId){
        if (accountsMap.containsKey(accountId)){
            accountsMap.remove(accountId);
        }
    }

    public static EAccountType getAccountType(String accountType){

        if (accountType.equalsIgnoreCase("savings")){
            return EAccountType.SAVINGS;
        }else if (accountType.equalsIgnoreCase("current")){
            return EAccountType.CURRENT;
        }else if (accountType.equalsIgnoreCase("income")){
            return EAccountType.INCOME;
        }else if (accountType.equalsIgnoreCase("cheque")){
            return EAccountType.CHEQUE;
        }else if (accountType.equalsIgnoreCase("credit")){
            return EAccountType.CREDIT;
        } else if (accountType.equalsIgnoreCase("expense")){
            return EAccountType.EXPENSE;
        }else if (accountType.equalsIgnoreCase("salary")){
            return EAccountType.SALARY;
        }else if (accountType.equalsIgnoreCase("Other")){
            return EAccountType.OTHER;
        }

        return EAccountType.EXPENSE;
    }

    public static int getAccountIcon(EAccountType accountType) {
        switch (accountType) {
            case SAVINGS:
                return  R.drawable.savings;
            case HOSPITAL:
                return  R.drawable.hospital;
            case SCHOOL:
                return  R.drawable.school;
            case SHOP:
                return  R.drawable.shop;
            case RESTAURANT:
                return  R.drawable.restaurant;
            case CREDIT:
                return  R.drawable.savings;
            case CURRENT:
                return  R.drawable.savings;
            case AGENCY:
                return  R.drawable.agency;
            case INCOME:
                return  R.drawable.income;
            case SALARY:
                return  R.drawable.income;
        }

        return  R.drawable.miscellaneous;
    }

    public static int getAccountColor(String name){
        int colorIndex = Math.abs(name.toString().hashCode()%ColourCodes.colourCodes.length);
        return Color.parseColor(ColourCodes.colourCodes[colorIndex]);
    }

    public static boolean isIncome(EAccountType accountType){
        if (accountType == EAccountType.INCOME || accountType == EAccountType.SALARY){
                return true;
        }
        return false;
    }

    public static boolean isExpense(EAccountType accountType){

        if (accountType == EAccountType.EXPENSE || accountType == EAccountType.OTHER){
            return true;
        }
        return false;
    }

    public static boolean isCurrent(Account account){
        EAccountType accountType = account.getAccountType();
        if (accountType == EAccountType.CURRENT || accountType == EAccountType.CHEQUE ||
                accountType == EAccountType.CREDIT){
            return true;
        }
        return false;
    }
}
